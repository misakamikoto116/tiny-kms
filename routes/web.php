<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/register-orangtua', 'HomeController@RegisterOrangTua')->name('orangtua.register');

Auth::routes();

Route::get('/images/{folder}/{filename}', function ($folder, $filename) {
    $path = public_path('/storage/' . $folder . '/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::group([
    'middleware' => ['auth', 'role:Admin'],
    'prefix' => 'admin',
    'as' => 'admin.'
], function () {
    Route::redirect('', '/admin/home');
    Route::get('/home',  'HomeController@AdminDashboard')->name('home');

    Route::resource('posyandu', 'PosyanduController');
    Route::resource('petugas', 'PetugasController');
    Route::resource('orangtua', 'OrangTuaController');

    Route::get('posyandu-rekap', 'PosyanduController@rekap_penimbangan_desa')->name('posyandu.rekap');
    Route::get('posyandu-rekap-print', 'PosyanduController@rekap_penimbangan_desa_print')->name('posyandu.rekap.print');
});

Route::group([
    'middleware' => ['auth', 'role:Petugas'],
    'prefix' => 'petugas',
    'as' => 'petugas.'
], function () {
    Route::redirect('', '/petugas/home');
    Route::get('/home',  'HomeController@PetugasDashboard')->name('home');

    Route::get('/cari-anak', 'AnakController@index')->name('anak.cari-anak');

    Route::get('/anak', function () {
        return view('petugas.anak.index');
    })->name('anak.index');

    Route::get('/anak/detail', function () {
        return view('petugas.anak.detail');
    })->name('anak.detail');

    Route::post('/confirm-anak/{id}', 'AnakController@ConfirmAnak')->name('confirm.anak');

    Route::resource('jadwal-kegiatan', 'JadwalKegiatanController');

    Route::get('posyandu-report', 'PenimbanganController@PosyanduReport')->name('posyandu.report');
    Route::get('posyandu-print', 'PenimbanganController@PosyanduPrint')->name('posyandu.print');

    Route::group([
        'prefix' => 'anak',
        'as' => 'anak.'
    ], function () {
        // Reports
        Route::get('imunisasi/report', 'ImunisasiController@report')->name('imunisasi.report');
        Route::get('vitamin/report', 'VitaminAController@report')->name('vitamin.report');
        Route::get('penimbangan/report', 'PenimbanganController@report')->name('penimbangan.report');

        // Prints
        Route::get('imunisasi/print', 'ImunisasiController@print')->name('imunisasi.print');
        Route::get('vitamin/print', 'VitaminAController@print')->name('vitamin.print');
        Route::get('penimbangan/print', 'PenimbanganController@print')->name('penimbangan.print');
        Route::group(['middleware' => 'NotInAnakSession'], function () {
            Route::resource('imunisasi', 'ImunisasiController')->except('show');
            Route::resource('vitamin', 'VitaminAController')->except('show');
            Route::resource('penimbangan', 'PenimbanganController')->except('show');
            Route::get('grafik', 'PenimbanganController@grafik')->name('penimbangan.grafik');
        });
    });
});

Route::group([
    'middleware' => ['auth', 'role:Orang_Tua'],
    'prefix' => 'orangtua',
    'as' => 'orangtua.'
], function () {
    Route::redirect('', '/orangtua/home');
    Route::get('/home',  'HomeController@OrangTuaDashboard')->name('home');

    Route::get('/pilih-anak', 'OrangTuaController@PilihAnak')->name('pilih_anak');
    Route::get('/tambah-anak', 'OrangTuaController@TambahAnak')->name('tambah_anak');
    Route::post('/confirm-anak/{id}', 'AnakController@ConfirmAnak')->name('confirm.anak');

    Route::resource('jadwal-kegiatan', 'JadwalKegiatanController')->only('index');

    Route::group([
        'prefix' => 'anak',
        'as' => 'anak.'
    ], function () {
        Route::redirect('', '/orangtua/anak/data');
        Route::resource('data', 'AnakController');
        Route::group(['middleware' => 'NotInAnakSession'], function () {
            Route::resource('imunisasi', 'ImunisasiController')->only('index');
            Route::resource('vitamin', 'VitaminAController')->only('index');
            Route::resource('penimbangan', 'PenimbanganController')->only('index');
            Route::get('grafik', 'PenimbanganController@grafik')->name('penimbangan.grafik');
        });
    });
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('jenis-imunisasi/{jenis}', function ($jenis) {
        return view('shared.jenis-imunisasi.' . $jenis);
    });
    Route::get('password', 'ProfileController@password')->name('profiles.get.password');
    Route::put('password', 'ProfileController@passwordPost')->name('profiles.post.password');
    Route::get('profile', 'ProfileController@profile')->name('profiles.get.profile');
    Route::put('profile', 'ProfileController@profilePost')->name('profiles.post.profile');
});
