<?php

namespace App\Http\Middleware;

use Closure;

class NotInAnakSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->get('anak')) {
            $role = auth()->user()->getRoleNames()->first();

            switch ($role) {
                case 'Petugas':
                    $redirect = redirect()->route('petugas.anak.cari-anak');
                    break;
                
                default:
                    $redirect = redirect()->route('orangtua.pilih_anak');
                    break;
            }
            return $redirect;
        }

        return $next($request);
    }
}
