<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\KBM;
use App\Models\VitaminA;
use App\Models\Anak;
use App\Models\Zscore;
use App\Models\Penimbangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PenimbanganRequest;

class PenimbanganController extends Controller
{
    public function __construct(
        VitaminA $vitamin,
        Penimbangan $penimbangan,
        Anak $anak
    ) {
        $this->vitamin     = $vitamin;
        $this->penimbangan = $penimbangan;
        $this->anak        = $anak;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $anak = $request->session()->get('anak');
        $data = $this->penimbangan->filter($request)->with('anak', 'posyandu', 'petugas')->where('anak_id', $anak['id'])->orderBy('tanggal', 'DESC')->paginate(10);

        $views = [
            'datas' => $data
        ];

        return view('shared.anak.penimbangan.index')->with($views);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $anak        = $request->session()->get('anak');
        $last_month_penimbangan    = $this->penimbangan->where('anak_id', $anak['id'])->orderBy('tanggal', 'DESC')->first();
        $views = [
            'last_month_penimbangan'   => $last_month_penimbangan,
            'anak'                     => $anak,
        ];

        return view('shared.anak.penimbangan.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenimbanganRequest $request)
    {
        DB::beginTransaction();
        try {
            $anak_id = $request->session()->get('anak.id');
            $request->merge([
                'anak_id'     => $anak_id,
                'petugas_id'  => auth()->user()->petugas->id,
                'posyandu_id' => auth()->user()->petugas->posyandu_id,
            ]);

            $pnm = $this->penimbangan->where('anak_id', $anak_id)->count();
            $penimbangan = $this->penimbangan->create($request->all());

            $keterangan = $this->keterangan($request->berat_badan, $anak_id, $request->tanggal, $penimbangan->umur_bulan, $pnm);
            $bb = $penimbangan->berat_badan;
            $color = 'undefined';
            $position = 'undefined';

            $zscores = Zscore::where('bulan', $penimbangan->umur_bulan)->get();
            foreach ($zscores as $zscore) {
                if ($zscore->color == 'red') {
                    if ($bb < $zscore->min) {
                        $color = $zscore->color;
                        $position = $zscore->position;
                    }
                } elseif ($zscore->color == 'yellow' && $zscore->position == 'top') {
                    if ($bb > $zscore->min) {
                        $color = $zscore->color;
                        $position = $zscore->position;
                    }
                } else {
                    if ($this->nBetween($bb, $zscore->max, $zscore->min)) {
                        $color = $zscore->color;
                        $position = $zscore->position;
                    }
                }
            }

            $penimbangan->update([
                'jml_umur' => $penimbangan->umur_bulan,
                'color'    => $color,
                'position' => $position,
                'keterangan'  => $keterangan
            ]);

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Penimbangan Anak');
            $request->session()->flash('type', 'success');

            return redirect()->route('petugas.anak.penimbangan.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    private function keterangan($nowBerat, $anak_id, $date, $umur, $penimbangan)
    {
        $dates = Carbon::parse($date)->subMonths(1);
        if ($penimbangan !== 0) {
            $last_month_penimbangan  = $this->penimbangan->where('anak_id', $anak_id)->whereMonth('tanggal', $dates->format('m'))->whereYear('tanggal', $dates->format('Y'))->first();
            if ($last_month_penimbangan) {
                $oldBerat = $last_month_penimbangan->berat_badan;
                $kbm = Kbm::where('umur', $umur)->first();
                if ($kbm) {
                    $calculate = ($nowBerat - $oldBerat) * 1000;
                    if ($calculate > $kbm->kbm) {
                        $keterangan = '1';
                    } else {
                        $keterangan = '2';
                    }
                } else {
                    if ($nowBerat > $oldBerat) {
                        $keterangan = '1';
                    } else {
                        $keterangan = '2';
                    }
                }
            } else {
                $keterangan = '3';
            }
        } else {
            $keterangan = '0';
        }

        return $keterangan;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data        = $this->penimbangan->with('anak', 'posyandu', 'petugas')->findOrFail($id);
        $anak        = $request->session()->get('anak');
        $dates = $data->tanggal->subMonths(1);
        $last_month_penimbangan  = $this->penimbangan->where('anak_id', $anak['id'])->whereMonth('tanggal', $dates->format('m'))->whereYear('tanggal', $dates->format('Y'))->first();

        $views = [
            'last_month_penimbangan' => $last_month_penimbangan,
            'data'                   => $data,
            'anak'                   => $anak,
        ];

        return view('shared.anak.penimbangan.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenimbanganRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $anak_id = $request->session()->get('anak.id');
            $data = $this->penimbangan->findOrFail($id);
            $penimbangan = $this->penimbangan->where('anak_id', $anak_id)->where('id', '!=', $id)->count();

            $data->update($request->all());

            $keterangan = $this->keterangan($request->berat_badan, $anak_id, $request->tanggal, $data->umur_bulan, $penimbangan);

            $bb = $data->berat_badan;
            $color = 'undefined';
            $position = 'undefined';

            $zscores = Zscore::where('bulan', $data->umur_bulan)->get();
            foreach ($zscores as $zscore) {
                if ($zscore->color == 'red') {
                    if ($bb < $zscore->min) {
                        $color = $zscore->color;
                        $position = $zscore->position;
                    }
                } elseif ($zscore->color == 'yellow' && $zscore->position == 'top') {
                    if ($bb > $zscore->min) {
                        $color = $zscore->color;
                        $position = $zscore->position;
                    }
                } else {
                    if ($this->nBetween($bb, $zscore->max, $zscore->min)) {
                        $color = $zscore->color;
                        $position = $zscore->position;
                    }
                }
            }

            $data->update([
                'jml_umur' => $data->umur_bulan,
                'color'    => $color,
                'position' => $position,
                'keterangan' => $keterangan
            ]);

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data Penimbangan Anak');
            $request->session()->flash('type', 'success');

            return redirect()->route('petugas.anak.penimbangan.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $this->penimbangan->findOrFail($id);
            $data->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Penimbangan Anak');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    public function report()
    {
        return view('petugas.anak.reports.penimbangan');
    }

    public function print(Request $request)
    {
        $tanggal = $request->date;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];
        $tanggal_formatted = Carbon::createFromDate($tahun, $bulan, 1)->format('F Y');

        $datas = $this->penimbangan->filter($request)
            ->with('anak', 'posyandu', 'petugas')
            ->whereMonth('tanggal', $bulan)
            ->whereYear('tanggal', $tahun)
            ->orderBy('tanggal', 'DESC')->get();

        if ($datas->count() === 0) {
            $request->session()->flash('messages', 'Tidak Ada Data Penimbangan di ' . auth()->user()->petugas->posyandu->name);
            $request->session()->flash('type', 'danger');

            return redirect()->route('petugas.anak.penimbangan.report')->withInput();
        }

        $views = [
            'datas' => $datas,
            'tanggal' => $tanggal_formatted
        ];

        return view('petugas.anak.reports.print.penimbangan')->with($views);
    }

    public function grafik(Request $request)
    {
        // Anak
        $anak = $request->session()->get('anak');

        // Penimbangan
        $datas = $this->penimbangan->filter($request)->with('anak', 'posyandu', 'petugas')->where('anak_id', $anak['id'])->orderBy('tanggal', 'ASC')->get()->filter(function ($item) {
            return $item->umur_bulan <= 60;
        });

        // Cards
        $penimbangan = $datas->map(function ($item, $Key) {
            // KBM
            $kbm = KBM::where('umur', $item->umur_bulan)->first()->kbm;

            return [
                'anak'              => $item->anak->name,
                'umur_bulan'        => $item->umur_bulan,
                'petugas'           => $item->petugas->user->name,
                'posyandu'          => $item->posyandu->name,
                'berat_badan'       => $item->berat_badan,
                'kbm'               => $kbm,
                'tanggal'           => $item->tanggal_formatted,
                'tinggi_badan'      => $item->tinggi_badan,
                'lingkar_kepala'    => $item->lingkar_kepala,
                'asi_eksklusif'     => $item->asi_eksklusif,
                'keterangan'        => $item->keterangan_formatted,
                'persentage'        => $item->persentage,
                'color'             => $item->color,
                'position'          => $item->position
            ];
        });

        $weights = $datas->pluck('berat_badan')->toArray();
        $months = $datas->pluck('umur_bulan')->toArray();
        $colors = $datas->pluck('color')->toArray();

        $views = [
            'cards'   => $penimbangan,
            'weights' => $weights,
            'months'  => $months,
            'colors'  => $colors
        ];

        return view('shared.anak.grafik.index')->with($views);
    }


    public function PosyanduReport()
    {
        return view('petugas.anak.reports.posyandu');
    }

    // Yang Berfungsi Cuma ID = 1, Perlu dicek lagi nnti

    public function PosyanduPrint(Request $request)
    {
        $col = collect();
        $tanggal = $request->tanggal;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];
        $tanggal_formatted = Carbon::createFromDate($tahun, $bulan, 1)->format('F Y');

        $report_1 = $this->report_vitamins($request, 'biru');
        $report_2 = $this->report_vitamins($request, 'merah');
        $report_3 = $this->report_penimbangan($request, [0, 12], true);
        $report_4 = $this->report_penimbangan($request, [13, 60], true);
        $report_5 = $this->report_penimbangan($request, [0, 60]);

        $report_8 = $this->report_penimbangan($request, [0, 23]);
        $report_9 = $this->report_penimbangan($request, [24, 60]);
        $report_10 = $this->report_penimbangan($request, [0, 23], false, '1', true);
        $report_11 = $this->report_penimbangan($request, [24, 60], false, '1', true);
        $report_12 = $this->report_penimbangan($request, false, false, '2', true);
        $report_13 = $this->report_penimbangan($request, false, false, '3', true);
        $report_14 = $this->report_penimbangan($request, [0, 23], false, '0');
        $report_15 = $this->report_penimbangan($request, [24, 60], false, '0');
        $report_16 = $this->report_penimbangan_red($request, [6, 12]);
        $report_17 = $this->report_penimbangan_red($request, [13, 60]);
        $report_18 = $this->report_penimbangan($request, false, false, 'no', false, false, 2);
        $report_19 = $this->report_penimbangan($request, [36, 36], false, 'no', true);
        $report_20 = $this->report_penimbangan($request, [36, 36], false, 'no', true, 1160);

        $results = $col->put(1, $report_1)
            ->put(2, $report_2)
            ->put(3, $report_3)
            ->put(4, $report_4)
            ->put(5, $report_5)
            ->put(8, $report_8)
            ->put(9, $report_9)
            ->put(10, $report_10)
            ->put(11, $report_11)
            ->put(12, $report_12)
            ->put(13, $report_13)
            ->put(14, $report_14)
            ->put(15, $report_15)
            ->put(16, $report_16)
            ->put(17, $report_17)
            ->put(18, $report_18)
            ->put(19, $report_19)
            ->put(20, $report_20);

        $views = [
            'datas'     => $results,
            'tanggal'   => $tanggal_formatted
        ];

        return view('petugas.anak.reports.print.posyandu')->with($views);
    }

    private function report_vitamins($request, $jenis)
    {
        $cowo = $this->anak->Laki();
        $cewe = $this->anak->Perempuan();
        $jumlah = $this->anak;

        $tanggal = $request->tanggal;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];

        $col = collect();
        $report_L = $cowo->whereHas('vitamin_a', function ($q) use ($request, $jenis, $bulan, $tahun) {
            $q->Jenis($jenis)
                ->whereMonth('tanggal', $bulan)
                ->whereYear('tanggal', $tahun);
        })->count();

        $report_P = $cewe->whereHas('vitamin_a', function ($q) use ($request, $jenis, $bulan, $tahun) {
            $q->Jenis($jenis)
                ->whereMonth('tanggal', $bulan)
                ->whereYear('tanggal', $tahun);
        })->count();

        $report_J = $jumlah->whereHas('vitamin_a', function ($q) use ($request, $jenis, $bulan, $tahun) {
            $q->Jenis($jenis)
                ->whereMonth('tanggal', $bulan)
                ->whereYear('tanggal', $tahun);
        })->count();

        $results = $col->put('L', $report_L)->put('P', $report_P)->put('J', $report_J);

        return $results;
    }

    private function report_penimbangan($request, $between = false, $posyandu = false, $keterangan = 'no', $month = false, $more = false, $has = false)
    {
        $cowo = $this->anak->Laki();
        $cewe = $this->anak->Perempuan();
        $jumlah = $this->anak;

        $tanggal = $request->tanggal;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];

        $col = collect();
        $report_L = $cowo->when($has === false, function ($qq) use ($request, $between, $posyandu, $keterangan, $month, $more, $bulan, $tahun) {
            $qq->whereHas('penimbangan', function ($q) use ($request, $between, $posyandu, $keterangan, $month, $more, $bulan, $tahun) {
                $q->when($more !== false, function ($qw) use ($more) {
                    $qw->where('berat_badan', '=', $more);
                })->when($between !== false, function ($qw) use ($between) {
                    $qw->whereBetween('jml_umur', $between);
                })->when($posyandu === true, function ($qw) {
                    $qw->where('posyandu_id', auth()->user()->petugas->posyandu_id);
                })->when($keterangan !== 'no', function ($qw) use ($keterangan) {
                    $qw->where('keterangan', $keterangan);
                })
                    ->whereMonth('tanggal', $bulan)
                    ->whereYear('tanggal', $tahun);
            });
        })->when($has !== false, function ($qq) use ($has) {
            $qq->has('penimbangan_turun', '>=', $has);
        })->count();

        $report_P = $cewe->when($has === false, function ($qq) use ($request, $between, $posyandu, $keterangan, $month, $more, $bulan, $tahun) {
            $qq->whereHas('penimbangan', function ($q) use ($request, $between, $posyandu, $keterangan, $month, $more, $bulan, $tahun) {
                $q->when($more !== false, function ($qw) use ($more) {
                    $qw->where('berat_badan', '>', $more);
                })->when($between !== false, function ($qw) use ($between) {
                    $qw->whereBetween('jml_umur', $between);
                })->when($posyandu === true, function ($qw) {
                    $qw->where('posyandu_id', auth()->user()->petugas->posyandu_id);
                })->when($keterangan !== 'no', function ($qw) use ($keterangan) {
                    $qw->where('keterangan', $keterangan);
                })
                    ->whereMonth('tanggal', $bulan)
                    ->whereYear('tanggal', $tahun);
            });
        })->when($has !== false, function ($qq) use ($has) {
            $qq->has('penimbangan_turun', '>=', $has);
        })->count();


        $report_J = $jumlah->when($has === false, function ($qq) use ($request, $between, $posyandu, $keterangan, $month, $more, $bulan, $tahun) {
            $qq->whereHas('penimbangan', function ($q) use ($request, $between, $posyandu, $keterangan, $month, $more, $bulan, $tahun) {
                $q->when($more !== false, function ($qw) use ($more) {
                    $qw->where('berat_badan', '>', $more);
                })->when($between !== false, function ($qw) use ($between) {
                    $qw->whereBetween('jml_umur', $between);
                })->when($posyandu === true, function ($qw) {
                    $qw->where('posyandu_id', auth()->user()->petugas->posyandu_id);
                })->when($keterangan !== 'no', function ($qw) use ($keterangan) {
                    $qw->where('keterangan', $keterangan);
                })
                    ->whereMonth('tanggal', $bulan)
                    ->whereYear('tanggal', $tahun);
            });
        })->when($has !== false, function ($qq) use ($has) {
            $qq->has('penimbangan_turun', '>=', $has);
        })->count();

        $results = $col->put('L', $report_L)->put('P', $report_P)->put('J', $report_J);

        return $results;
    }

    private function report_penimbangan_red($request, $between = false)
    {
        $col = collect();

        $tanggal = $request->tanggal;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];

        // Penimbangan
        $datas = $this->penimbangan->with('anak', 'posyandu', 'petugas')
            ->whereBetween('jml_umur', $between)
            ->whereMonth('tanggal', $bulan)
            ->whereYear('tanggal', $tahun)
            ->orderBy('tanggal', 'ASC')->get();

        // Cards
        $penimbangan = $datas->map(function ($item, $Key) {
            if ($item->color === 'red') {
                return [
                    'anak_id'    => $item->anak_id,
                    'color'      => $item->color,
                    'position'   => $item->position,
                    'jenis_kelamin' => $item->anak->jenis_kelamin
                ];
            }
        });

        $report_L = $penimbangan->where('color', 'red')->where('position', 'bottom')->where('jenis_kelamin', 'LK')->groupBy('anak_id')->count();
        $report_P = $penimbangan->where('color', 'red')->where('position', 'bottom')->where('jenis_kelamin', 'PR')->groupBy('anak_id')->count();
        $report_J = $penimbangan->where('color', 'red')->where('position', 'bottom')->groupBy('anak_id')->count();

        $results = $col->put('L', $report_L)->put('P', $report_P)->put('J', $report_J);

        return $results;
    }

    private function nBetween($varToCheck, $high, $low)
    {
        if ($varToCheck < $low) return false;
        if ($varToCheck > $high) return false;
        return true;
    }
}
