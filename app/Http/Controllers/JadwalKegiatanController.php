<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use App\Models\Posyandu;
use Illuminate\Http\Request;
use App\Http\Requests\JadwalRequest;
use Illuminate\Support\Facades\DB;

class JadwalKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Jadwal::filter($request)->with('posyandu')->orderBy('created_at', 'ASC')->paginate(10);

        $views = [
            'datas' => $data
        ];

        return view('shared.jadwal-kegiatan.index')->with($views);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tempat_kegiatan = Posyandu::orderBy('code', 'ASC')->get();
        $views = [
            'tempat_kegiatan'   => $tempat_kegiatan
        ];

        return view('shared.jadwal-kegiatan.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JadwalRequest $request)
    {
        DB::beginTransaction();
        try {
            $wew = Jadwal::create($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Jadwal Kegiatan');
            $request->session()->flash('type', 'success');

            return redirect()->route('petugas.jadwal-kegiatan.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Jadwal::findOrFail($id);
        $views = [
            'data'              => $data,
        ];

        return view('shared.jadwal-kegiatan.show')->with($views);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Jadwal::findOrFail($id);
        $tempat_kegiatan = Posyandu::orderBy('code', 'ASC')->get();
        $views = [
            'data'              => $data,
            'tempat_kegiatan'   => $tempat_kegiatan
        ];

        return view('shared.jadwal-kegiatan.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JadwalRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Jadwal::findOrFail($id);
            $data->update($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data Jadwal Kegiatan');
            $request->session()->flash('type', 'success');

            return redirect()->route('petugas.jadwal-kegiatan.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Jadwal::findOrFail($id);

            $data->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Jadwal Kegiatan');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }
}
