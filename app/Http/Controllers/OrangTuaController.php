<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Models\Petugas;
use App\Models\OrangTua;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\OrangTuaRequest;

class OrangTuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::filter($request)->with('orang_tua.anak')->role('Orang_Tua')->orderBy('name', 'ASC')->paginate(10);
        $view = [
            'datas'  => $user
        ];

        return view('admin.orangtua.index')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.orangtua.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * Request: name, no_hp, jenis_kelamin, tgl_lahir, alamat, foto, nik, no_kk, type
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrangTuaRequest $request)
    {
        DB::beginTransaction();
        try {

            $in_anggana = false;
            $file = $request->foto;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

            if (Str::substr($request->nik ?? $request->no_ktp, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $request->merge([
                'password' => bcrypt($request->password),
            ]);

            $user = User::create($request->except('no_kk', 'nik'));
            $user->update(['foto' => $photo]);
            $user->assignRole('Orang_Tua');

            OrangTua::create([
                'no_kk'   => $request->no_kk,
                'nik'     => $request->nik,
                'user_id' => $user->id,
                'is_in_anggana' => $in_anggana
            ]);

            $this->SavePhotoNR($file, $photo, public_path('storage/orang_tua/'));

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Orang Tua');
            $request->session()->flash('type', 'success');

            return redirect()->route('admin.orangtua.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::with('orang_tua.anak')->role('Orang_Tua')->findOrFail($id);

        $views = [
            'data' => $data
        ];

        return view('admin.orangtua.show')->with($views);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::with('orang_tua.anak')->role('Orang_Tua')->findOrFail($id);

        $views = [
            'data' => $data,
        ];

        return view('admin.orangtua.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * Request: name, no_hp, jenis_kelamin, tgl_lahir, alamat, foto, jabatan
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrangTuaRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $in_anggana = false;
            $user = User::findOrFail($id);
            $user->update($request->except('no_kk', 'nik'));

            if (Str::substr($request->no_ktp ?? $request->nik, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $user->orang_tua->update([
                'no_kk'         => $request->no_kk,
                'nik'           => $request->nik,
                'is_in_anggana' => $in_anggana

            ]);

            if (isset($request->foto)) {
                if (!empty($user->foto)) {
                    \File::delete(public_path('storage/orang_tua/' . $user->foto));
                }

                $file = $request->foto;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $user->update(['foto' => $photo]);

                $this->SavePhotoNR($file, $photo, public_path('storage/orang_tua/'));
            }

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data Orang Tua');
            $request->session()->flash('type', 'success');

            return redirect()->route('admin.orangtua.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->orang_tua->delete();

            if (!empty($user->foto)) {
                \File::delete(public_path('storage/orang_tua/' . $user->foto));
            }

            $user->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Orang Tua');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    public function PilihAnak(Request $request)
    {
        $orang_tua = auth()->user()->orang_tua;
        $anak = $orang_tua->anak->pluck('name', 'id');
        $request->session()->forget('anak');

        $views = [
            'anak'  => $anak
        ];

        return view('orangtua.pilih-anak')->with($views);
    }

    public function TambahAnak()
    {
        $ibu = OrangTua::whereHas('user', function ($q) {
            $q->where('jenis_kelamin', 'PR');
        })->with('user')->get()->pluck('user.name', 'id');

        $bapak = OrangTua::whereHas('user', function ($q) {
            $q->where('jenis_kelamin', 'LK');
        })->with('user')->get()->pluck('user.name', 'id');

        $views = [
            'bapak' => $bapak,
            'ibu'   => $ibu
        ];

        return view('orangtua.tambah-anak')->with($views);
    }
}
