<?php

namespace App\Http\Controllers;

use App\Models\Imunisasi;
use App\Models\Posyandu;
use Illuminate\Http\Request;
use App\Http\Requests\ImunisasiRequest;
use Illuminate\Support\Facades\DB;

class ImunisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $anak = $request->session()->get('anak');
        $datas = Imunisasi::filter($request)->with('anak', 'posyandu', 'petugas')->where('anak_id', $anak['id'])->orderBy('tanggal', 'DESC')->paginate(10);

        $prefix = auth()->user()->getRoleNames()->first();

        $views = [
            'anak'  => $anak,
            'datas' => $datas
        ];

        return view('shared.anak.imunisasi.index')->with($views);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $posyandu    = Posyandu::orderBy('code', 'ASC')->get();
        $anak        = $request->session()->get('anak');
        $petugas     = auth()->user()->with('petugas');
        
        $views = [
            'posyandu'   => $posyandu,
            'anak'       => $anak,
            'petugas'    => $petugas
        ];

        return view('shared.anak.imunisasi.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImunisasiRequest $request)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'anak_id'     => $request->session()->get('anak.id'),
                'petugas_id'  => auth()->user()->petugas->id,
                'posyandu_id' => auth()->user()->petugas->posyandu_id
            ]);

            $imunisasi = Imunisasi::create($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Imunisasi Anak');
            $request->session()->flash('type', 'success');
            
            return redirect()->route('petugas.anak.imunisasi.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data        = Imunisasi::findOrFail($id);
        $posyandu    = Posyandu::orderBy('code', 'ASC')->get();
        $anak        = $request->session()->get('anak');
        $petugas     = auth()->user()->with('petugas');
        
        $views = [
            'data'       => $data,
            'posyandu'   => $posyandu,
            'anak'       => $anak,
            'petugas'    => $petugas
        ];

        return view('shared.anak.imunisasi.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImunisasiRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Imunisasi::findOrFail($id);
            $data->update($request->all());

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Mengubah Data Imunisasi Anak');
            $request->session()->flash('type', 'success');
    
            return redirect()->route('petugas.anak.imunisasi.index');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Imunisasi::findOrFail($id);
            $data->delete();

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Menghapus Data Imunisasi Anak');
            $request->session()->flash('type', 'success');
    
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    public function report() {
        return view('petugas.anak.reports.imunisasi');
    }

    public function print(Request $request) {
        $request->start = $request->start.' 00:00:00';
        $request->end = $request->end.' 23:59:59';
        
        $datas = Imunisasi::filter($request)
                          ->with('anak', 'posyandu', 'petugas')
                          ->whereBetween('tanggal', [$request->start_date, $request->end_date])
                          ->orderBy('tanggal', 'DESC')->get();

        if ($datas->count() === 0) {
            $request->session()->flash('messages', 'Tidak Ada Data Imunisasi di ' . auth()->user()->petugas->posyandu->name);
            $request->session()->flash('type', 'danger');
    
            return redirect()->route('petugas.anak.imunisasi.report')->withInput();
        }

        $views = [
            'datas' => $datas
        ];

        return view('petugas.anak.reports.print.imunisasi')->with($views);
    }
}
