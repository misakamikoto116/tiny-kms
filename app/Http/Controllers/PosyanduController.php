<?php

namespace App\Http\Controllers;

use App\Models\Posyandu;
use Illuminate\Http\Request;
use App\Http\Requests\PosyanduRequest;
use App\Models\Petugas;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PosyanduController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Posyandu::filter($request)->orderBy('code', 'ASC')->paginate(10);

        $views = [
            'datas' => $data
        ];

        return view('admin.posyandu.index')->with($views);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = $this->getLastCodePosyandu();
        $kelurahans = Petugas::kelurahan_choices();

        $views = [
            'kode_posyandu' => $id,
            'kelurahan_choices' => $kelurahans
        ];

        return view('admin.posyandu.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * NOTE: Request => name, jumlah_kader, desa, ketua
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PosyanduRequest $request)
    {
        DB::beginTransaction();
        try {

            $request->merge(['code' => $this->getLastCodePosyandu()]);
            $wew = Posyandu::create($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Posyandu');
            $request->session()->flash('type', 'success');

            return redirect()->route('admin.posyandu.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    public function getLastCodePosyandu()
    {
        $last_data = Posyandu::orderBy('id', 'desc')->first();
        if ($last_data !== null) {
            $last_code = $this->splitCode($last_data->code);
        } else {
            $last_code = '000';
        }

        return 'P' . str_pad($last_code + 1, 3, '0', STR_PAD_LEFT);
    }

    private function splitCode($last_no_order)
    {
        return (int) substr($last_no_order, -3);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Posyandu::with('ketua')->findOrFail($id);

        $views = [
            'data'    => $data,
        ];

        return view('admin.posyandu.show')->with($views);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * Appends Status Used => False / True
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Posyandu::findOrFail($id);
        $kelurahans = Petugas::kelurahan_choices();

        $views = [
            'data'    => $data,
            'kelurahan_choices' => $kelurahans
        ];
        return view('admin.posyandu.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * Request => name, jumlah_kader, desa, ketua
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PosyanduRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Posyandu::findOrFail($id);
            $data->update($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data Posyandu');
            $request->session()->flash('type', 'success');

            return redirect()->route('admin.posyandu.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Posyandu::findOrFail($id);
            if ($data->status_used) {
                $request->session()->flash('messages', 'Posyandu tidak dapat dihapus karena sudah terdapat data');
                $request->session()->flash('type', 'danger');

                return redirect()->back();
            }

            $data->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Posyandu');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }


    public function rekap_penimbangan_desa()
    {
        $kelurahans = Petugas::kelurahan_choices();
        $views = [
            'kelurahan_choices' => $kelurahans
        ];
        return view('admin.reports.posyandu-rekap')->with($views);
    }

    public function rekap_penimbangan_desa_print(Request $request)
    {
        // Anak Laki Laki = data->jumlah_anak_l
        // Anak Perempuan = data->jumlah_anak_p
        // Anak All = data->jumlah_anak

        $tanggal = $request->tanggal;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];
        $tanggal_formatted = Carbon::createFromDate($tahun, $bulan, 1)->format('F Y');

        $posyandu = Posyandu::where('desa', $request->kelurahan)->whereHas('penimbangan', function ($q) use ($bulan, $tahun) {
            $q->whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun);
        })->get();

        $jumlah = $posyandu->sum('jumlah_anak');

        $views = [
            'datas' => $posyandu,
            'tanggal' => $tanggal_formatted,
            'kelurahan' => $request->kelurahan,
            'jumlah'    => $jumlah
        ];

        return view('admin.reports.print.posyandu-rekap')->with($views);
    }
}
