<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Support\Str;
use App\Models\OrangTua;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'          => 'required|string|max:120',
            'username'      => 'required|unique:users,username',
            'email'         => 'required|unique:users,email',
            'password'      => 'required|min:6|max:20',
            'no_kk'         => 'required|string',
            'nik'           => 'required|string',
            'no_hp'         => 'required|numeric',
            'jenis_kelamin' => 'required|string',
            'tgl_lahir'     => 'required',
            'alamat'        => 'required',
            'foto'          => 'required|image|mimes:jpeg,jpg,png,gif,JPG,JPEG'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $file = $request->foto;
        $slug = Str::slug($file->getClientOriginalName(), '_');
        $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

        $request->merge([
            'password' => bcrypt($request->password),
        ]);

        $user = User::create($request->except('no_kk', 'nik'));
        $user->update(['foto' => $photo]);
        $user->assignRole('Orang_Tua');

        OrangTua::create([
            'no_kk'   => $request->no_kk,
            'nik'     => $request->nik, 
            'user_id' => $user->id
        ]);
        
        $this->SavePhotoNR($file, $photo, public_path('storage/orang_tua/'));

        return true;
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }
}
