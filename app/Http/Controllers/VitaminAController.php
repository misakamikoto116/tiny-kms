<?php

namespace App\Http\Controllers;

use App\Models\VitaminA;
use App\Models\Posyandu;
use Illuminate\Http\Request;
use App\Http\Requests\VitaminARequest;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VitaminAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $anak = $request->session()->get('anak');
        $datas = VitaminA::filter($request)->with('anak', 'posyandu', 'petugas')->where('anak_id', $anak['id'])->orderBy('tanggal', 'DESC')->paginate(10);

        $prefix = auth()->user()->getRoleNames()->first();

        $views = [
            'anak'  => $anak,
            'datas' => $datas
        ];

        return view('shared.anak.vitamin.index')->with($views);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $posyandu    = Posyandu::orderBy('code', 'ASC')->get();
        $anak        = $request->session()->get('anak');
        $petugas     = auth()->user()->with('petugas');

        $views = [
            'posyandu'   => $posyandu,
            'anak'       => $anak,
            'petugas'    => $petugas
        ];

        return view('shared.anak.vitamin.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VitaminARequest $request)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'anak_id'     => $request->session()->get('anak.id'),
                'petugas_id'  => auth()->user()->petugas->id,
                'posyandu_id' => auth()->user()->petugas->posyandu_id
            ]);

            $vitamin = VitaminA::create($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Vitamin A Anak');
            $request->session()->flash('type', 'success');

            return redirect()->route('petugas.anak.vitamin.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data        = VitaminA::findOrFail($id);
        $posyandu    = Posyandu::orderBy('code', 'ASC')->get();
        $anak        = $request->session()->get('anak');
        $petugas     = auth()->user()->with('petugas');

        $views = [
            'data'       => $data,
            'posyandu'   => $posyandu,
            'anak'       => $anak,
            'petugas'    => $petugas
        ];

        return view('shared.anak.vitamin.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VitaminARequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = VitaminA::findOrFail($id);
            $data->update($request->all());

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data Vitamin A Anak');
            $request->session()->flash('type', 'success');

            return redirect()->route('petugas.anak.vitamin.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = VitaminA::findOrFail($id);
            $data->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Vitamin A Anak');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    public function report()
    {
        return view('petugas.anak.reports.vitamin');
    }

    public function print(Request $request)
    {
        $tanggal = $request->date;
        $tanggal_new = explode("/", $tanggal);
        $bulan = $tanggal_new[1];
        $tahun = $tanggal_new[0];
        $tanggal_formatted = Carbon::createFromDate($tahun, $bulan, 1)->format('F Y');

        $datas = VitaminA::filter($request)
            ->with('anak', 'posyandu', 'petugas')
            ->whereMonth('tanggal', $bulan)
            ->whereYear('tanggal', $tahun)
            ->orderBy('tanggal', 'DESC')->get();

        if ($datas->count() === 0) {
            $request->session()->flash('messages', 'Tidak Ada Data Vitamin A di ' . auth()->user()->petugas->posyandu->name);
            $request->session()->flash('type', 'danger');

            return redirect()->route('petugas.anak.vitamin.report')->withInput();
        }

        $views = [
            'datas' => $datas,
            'tanggal' => $tanggal_formatted
        ];

        return view('petugas.anak.reports.print.vitamin')->with($views);
    }
}
