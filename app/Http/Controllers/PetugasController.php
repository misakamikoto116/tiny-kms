<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Models\Petugas;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\PetugasRequest;
use App\Models\Posyandu;


class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::filter($request)->with('petugas')->role('Petugas')->orderBy('name', 'ASC')->paginate(10);
        $view = [
            'datas'  => $user
        ];

        return view('admin.petugas.index')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posyandu = Posyandu::get();

        $views = [
            'posyandu' => $posyandu,
        ];

        return view('admin.petugas.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * Request: name, email, username, password, jabatan, confirmation_password, no_hp, jenis_kelamin, tgl_lahir, alamat, foto
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PetugasRequest $request)
    {
        DB::beginTransaction();
        try {

            $file = $request->foto;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

            $request->merge([
                'password' => bcrypt($request->password),
            ]);

            $user = User::create($request->except('jabatan', 'no_ktp'));
            $user->update(['foto' => $photo]);
            $user->assignRole('Petugas');
            $in_anggana = false;

            if (Str::substr($request->no_ktp ?? $request->nik, 0, 6) == 640204) {
                $in_anggana = true;
            }

            Petugas::create([
                'no_ktp'    => $request->no_ktp,
                'jabatan'   => $request->jabatan,
                'user_id'   => $user->id,
                'posyandu_id' => $request->posyandu_id,
                'is_in_anggana' => $in_anggana
            ]);

            $this->SavePhotoNR($file, $photo, public_path('storage/petugas/'));

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Petugas');
            $request->session()->flash('type', 'success');

            return redirect()->route('admin.petugas.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::with('petugas')->findOrFail($id);

        $views = [
            'data' => $data
        ];

        return view('admin.petugas.show')->with($views);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::with('petugas')->findOrFail($id);
        $posyandu = Posyandu::get();

        $views = [
            'data' => $data,
            'posyandu' => $posyandu
        ];

        return view('admin.petugas.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * Request: name, no_hp, jenis_kelamin, tgl_lahir, alamat, foto, jabatan
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PetugasRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->update($request->except('jabatan', 'no_ktp'));

            $in_anggana = false;
            if (Str::substr($request->no_ktp ?? $request->nik, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $user->petugas->update([
                'jabatan' => $request->jabatan,
                'no_ktp'  => $request->no_ktp,
                'posyandu_id' => $request->posyandu_id,
                'is_in_anggana' => $in_anggana
            ]);

            if (isset($request->foto)) {
                if (!empty($user->foto)) {
                    \File::delete(public_path('storage/petugas/' . $user->foto));
                }

                $file = $request->foto;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $user->update(['foto' => $photo]);

                $this->SavePhotoNR($file, $photo, public_path('storage/petugas/'));
            }

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data petugas');
            $request->session()->flash('type', 'success');

            return redirect()->route('admin.petugas.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            if ($user->petugas->status_used) {
                $request->session()->flash('messages', 'Petugas tidak dapat dihapus karena sudah terdapat data');
                $request->session()->flash('type', 'danger');

                return redirect()->route()->back();
            }

            $user->petugas->delete();

            if (!empty($user->foto)) {
                \File::delete(public_path('storage/petugas/' . $user->foto));
            }

            $user->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Petugas');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }
}
