<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileRequest;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $data = auth()->user();
        if ($data->getRoleNames()->first() !== 'Admin') {
            $data = $data->load('profile');
        }

        $views = [
            'data' => $data
        ];

        return view('shared.profiles.profile')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profilePost(ProfileRequest $request)
    {
        DB::beginTransaction();
        try {
            if (!Hash::check($request->old_password, auth()->user()->password)) {
                $request->session()->flash('messages', 'Password Anda Salah');
                $request->session()->flash('type', 'danger');
                return redirect()->back();
            }

            $in_anggana = false;
            $authUser = auth()->user();
            $authUser->update($request->all());
            if (Str::substr($request->nik ?? $request->no_ktp, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $role = $authUser->getRoleNames()->first();
            if ($role === 'Orang_Tua') {
                $module = 'orang_tua';

                $authUser->orang_tua->update([
                    'no_kk'   => $request->no_kk,
                    'nik'     => $request->nik,
                    'is_in_anggana' => $in_anggana
                ]);
            } elseif ($role === 'Petugas') {
                $module = 'petugas';

                $authUser->petugas->update([
                    'jabatan' => $request->jabatan,
                    'no_ktp'  => $request->no_ktp,
                    'is_in_anggana' => $in_anggana
                    // 'posyandu_id' => $request->posyandu_id
                ]);
            } else {
                $module = 'admin';
            }


            if (isset($request->foto)) {
                if (!empty($authUser->foto)) {
                    \File::delete(public_path('storage/' . $module . '/' . $authUser->foto));
                }

                $file = $request->foto;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $authUser->update(['foto' => $photo]);

                $this->SavePhotoNR($file, $photo, public_path('storage/' . $module  . '/'));
            }

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Profile');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password(Request $request)
    {
        return view('shared.profiles.password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function passwordPost(ProfileRequest $request)
    {
        DB::beginTransaction();
        try {
            if (!Hash::check($request->old_password, auth()->user()->password)) {
                $request->session()->flash('messages', 'Password Anda Salah');
                $request->session()->flash('type', 'danger');
                return redirect()->back();
            }

            auth()->user()->update([
                'password'  => bcrypt($request->password),
            ]);

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Password');
            $request->session()->flash('type', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }
}
