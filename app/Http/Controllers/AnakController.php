<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Anak;
use App\Models\Petugas;
use App\Models\OrangTua;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\AnakRequest;

class AnakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $role = auth()->user()->getRoleNames()->first();
        switch ($role) {
            case 'Petugas':
                $anak = Anak::filter($request)->with('orang_tua')->orderBy('name', 'ASC')->paginate(10);
                $prefix = 'petugas.cari-anak.index';
                break;

            default:
                $anak = Anak::filter($request)->with('orang_tua')->whereHas('orang_tua', function ($q) {
                    $q->where('ortu_id', auth()->user()->orang_tua->id);
                })->orderBy('name', 'ASC')->paginate(10);
                $prefix = 'orangtua.anak.index';
                break;
        }

        $view = [
            'datas'  => $anak
        ];

        return view($prefix)->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ibu = OrangTua::whereHas('user', function ($q) {
            $q->where('jenis_kelamin', 'PR');
        })->with('user')->get()->pluck('user.name', 'id');

        $bapak = OrangTua::whereHas('user', function ($q) {
            $q->where('jenis_kelamin', 'LK');
        })->with('user')->get()->pluck('user.name', 'id');

        $views = [
            'bapak' => $bapak,
            'ibu'   => $ibu,
        ];

        return view('orangtua.anak.create')->with($views);
    }

    /**
     * Store a newly created resource in storage.
     *
     * Request: name, tgl_lahir, jenis_kelamin, alamat, bapak_id, ibu_id, foto
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnakRequest $request)
    {
        DB::beginTransaction();
        try {

            $file = $request->foto;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

            $generateId = explode('/', $request->tgl_lahir);
            $generateId = implode('', $generateId);
            $generateId = $generateId . rand(1111, 9999);
            $in_anggana = false;

            if (Str::substr($request->no_ktp ?? $request->nik, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $request->merge([
                'generateId'    => $generateId,
                'ortu_id'       => auth()->user()->orang_tua->id,
                'is_in_anggana' => $in_anggana
            ]);

            $anak = Anak::create($request->all());
            $anak->update(['foto'  => $photo]);

            $this->SavePhotoNR($file, $photo, public_path('storage/anak/'));

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menambahkan Data Anak');
            $request->session()->flash('type', 'success');

            if ($request->session()->get('anak')) {
                $redirect = 'orangtua.anak.data.index';
            } else {
                $redirect = 'orangtua.pilih_anak';
            }

            return redirect()->route($redirect);
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Anak::with('orang_tua')->findOrFail($id);

        $views = [
            'data' => $data
        ];

        return view('orangtua.anak.show')->with($views);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Anak::with('orang_tua')->findOrFail($id);

        $views = [
            'data' => $data,
        ];

        return view('orangtua.anak.edit')->with($views);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnakRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $anak = Anak::findOrFail($id);
            $in_anggana = false;

            if (Str::substr($request->no_ktp ?? $request->nik, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $request->merge([
                'is_in_anggana' => $in_anggana
            ]);

            $anak->update($request->all());

            if (isset($request->foto)) {
                if (!empty($anak->foto)) {
                    \File::delete(public_path('storage/anak/' . $anak->foto));
                }

                $file = $request->foto;
                $slug = Str::slug($file->getClientOriginalName(), '_');
                $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
                $anak->update(['foto' => $photo]);

                $this->SavePhotoNR($file, $photo, public_path('storage/anak/'));
            }

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Mengubah Data Anak');
            $request->session()->flash('type', 'success');

            return redirect()->route('orangtua.anak.data.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     *  Belom Selesai
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $anak = Anak::findOrFail($id);
            if ($anak->status_used) {
                $request->session()->flash('messages', 'Anak tidak dapat dihapus karena sudah terdapat data');
                $request->session()->flash('type', 'danger');

                return redirect()->back();
            }

            if (!empty($anak->foto)) {
                \File::delete(public_path('storage/anak/' . $anak->foto));
            }

            $anak->delete();

            DB::commit();

            $request->session()->flash('messages', 'Berhasil Menghapus Data Anak');
            $request->session()->flash('type', 'success');

            $sessionSelectedId = $request->session()->get('anak.id');

            if (auth()->user()->orang_tua->anak->count() > 0 && $id != $sessionSelectedId) {
                $redirect = redirect()->back();
            } else {
                $request->session()->forget('anak');
                $redirect = redirect()->route('orangtua.pilih_anak');
            }

            return $redirect;
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    public function ConfirmAnak(Request $request, $id)
    {
        $anak = Anak::findOrFail($id);
        $request->session()->put('anak', $anak->only('id', 'nik', 'name', 'generateId', 'tgl_lahir', 'jenis_kelamin', 'alamat', 'foto', 'umur', 'tgl_lahir_formatted', 'jenis_kelamin_formatted', 'umur_bulan_formatted', 'umur_bulan', 'is_in_anggana_formatted', 'bapak', 'ibu', 'kecamatan', 'kelurahan', 'rt'));
        $role = auth()->user()->getRoleNames()->first();

        switch ($role) {
            case 'Petugas':
                $prefix = 'petugas.anak.index';
                break;

            default:
                $prefix = 'orangtua.home';
                break;
        }

        return redirect()->route($prefix);
    }
}
