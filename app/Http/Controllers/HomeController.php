<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Models\OrangTua;
use App\Models\Anak;
use Illuminate\Support\Str;
use App\Http\Requests\OrangTuaRequest;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = auth()->user()->getRoleNames()->first();
        $role = strtolower(str_replace('_', '', $role));
        return redirect()->route($role . '.home');
    }

    public function AdminDashboard()
    {
        $petugas = User::role('Petugas')->count();
        $orang_tua = User::role('Orang_Tua')->count();

        $col = collect();
        for ($i=1; $i <= 12 ; $i++) { 
            $col->push($this->getAnakTimbangCount($i));
        }

        $views = [
            'P'      => $petugas,
            'OT'     => $orang_tua,
            'static' => $col
        ];

        return view('admin.home')->with($views);
    }

    public function PetugasDashboard()
    {
        $orang_tua = User::role('Orang_Tua')->count();
        $anak      = Anak::count();
        $static    = Anak::orderBy('created_at', 'DESC')->get();

        $views = [
            'OT'        => $orang_tua,
            'A'         => $anak,
            'static'    => $static
        ];

        return view('petugas.home')->with($views);
    }

    public function OrangTuaDashboard()
    {
        $petugas = User::role('Petugas')->count();
        $anak      = Anak::count();
        $static    = Anak::orderBy('created_at', 'DESC')->whereHas('orang_tua', function($q) {
            $q->where('ortu_id', auth()->user()->orang_tua->id);
        })->get();

        $views = [
            'P'      => $petugas,
            'A'      => $anak,
            'static' => $static
        ];

        return view('orangtua.home')->with($views);
    }

    private function getAnakTimbangCount($month)
    {
        $data = Anak::whereHas('penimbangan', function($q) use ($month){
            $q->whereMonth('tanggal', $month)->whereYear('tanggal', date('Y'));
        })->count();

        return $data;
    }

    public function RegisterOrangTua(OrangTuaRequest $request)
    {
        DB::beginTransaction();
        try {
            
            $in_anggana = false;
            $file = $request->foto;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();

            if (Str::substr($request->nik ?? $request->no_ktp, 0, 6) == 640204) {
                $in_anggana = true;
            }

            $request->merge([
                'password' => bcrypt($request->password),
            ]);
            
            $user = User::create($request->except('no_kk', 'nik'));
            $user->update(['foto' => $photo]);
            $user->assignRole('Orang_Tua');

            OrangTua::create([
                'no_kk'   => $request->no_kk,
                'nik'     => $request->nik, 
                'user_id' => $user->id,
                'kecamatan'   => $request->kecamatan,
                'kelurahan'   => $request->kelurahan,
                'rt'          => $request->rt,
                'is_in_anggana' => $in_anggana
            ]);
            
            $this->SavePhotoNR($file, $photo, public_path('storage/orang_tua/'));
            Auth::login($user);

            DB::commit();
            
            $request->session()->flash('messages', 'Berhasil Register');
            $request->session()->flash('type', 'success');

    
            return redirect()->route('orangtua.pilih_anak');
        } catch (\Exception $e) {
            DB::rollback();
        
            return $e->getMessage();
        }
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }
}
