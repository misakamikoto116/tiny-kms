<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JadwalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal'           => 'required|string|max:120',
            'jam'               => 'required|string',
            'posyandu_id'       => 'required|numeric',
            'kegiatan'          => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'tanggal'       => 'Tanggal',
            'jam'           => 'Jam',
            'posyandu_id'   => 'Tempat Kegiatan',
            'kegiatan'      => 'Kegiatan',
        ];
    }
}
