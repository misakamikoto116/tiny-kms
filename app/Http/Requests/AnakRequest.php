<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('PUT') || $this->isMethod('PATCH')) {
            $foto   = 'nullable';
        } else {
            $foto = 'required';
        }

        return [
            'nik'           => 'required|string',
            'name'          => 'required|string|max:120',
            'tgl_lahir'     => 'required',
            'jenis_kelamin' => 'required|string',
            'rt'            => 'required|string',
            'kecamatan'     => 'required|string',
            'kelurahan'     => 'required|string',
            'alamat'        => 'required',
            'bapak'         => 'nullable|string',
            'ibu'           => 'nullable|string',
            'foto'          => $foto . '|image|mimes:jpeg,jpg,png,gif,JPG,JPEG'
        ];
    }

    public function attributes()
    {
        return [
            'name'          => 'Nama',
            'nik'           => 'NIK',
            'tgl_lahir'     => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'kecamatan'     => 'Kecamatan',
            'kelurahan'     => 'Kelurahan',
            'rt'            => 'RT',
            'alamat'        => 'Alamat',
            'foto'          => 'Foto',
            'ibu'           => 'Ibu',
            'bapak'         => 'Bapak'
        ];
    }
}
