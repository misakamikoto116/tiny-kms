<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->has('name')) {
            $role = $this->user()->roles()->first()->name;
            switch ($role) {
                case 'Petugas':
                    $OrangTua = 'nullable';
                    $Petugas  = 'required';
                    $Posyandu = 'required|numeric';
                    break;

                case 'Orang_Tua':
                    $OrangTua = 'required|string';
                    $Petugas  = 'nullable';
                    $Posyandu = 'nullable';
                    break;

                default:
                    $OrangTua = 'nullable';
                    $Petugas  = 'nullable';
                    $Posyandu = 'nullable';
                    break;
            }

            return [
                'name'          => 'required|string|max:120',
                'username'      => 'required|unique:users,username,' . $this->user()->id,
                'email'         => 'required|unique:users,email,' . $this->user()->id,
                'no_ktp'        => $Petugas,
                'jabatan'       => $Petugas,
                // 'posyandu_id'   => $Posyandu,
                'no_kk'         => $OrangTua,
                'nik'           => $OrangTua,
                'no_hp'         => 'required|numeric',
                'jenis_kelamin' => 'required|string',
                'tgl_lahir'     => 'required',
                'rt'            => 'required|string',
                'kecamatan'     => 'required|string',
                'kelurahan'     => 'required|string',
                'alamat'        => 'required',
                'foto'          => 'nullable|image|mimes:jpeg,jpg,png,gif,JPG,JPEG',
                'old_password'  => 'required'
            ];
        } else {
            return [
                'password'              => 'required|min:5|confirmed',
                'password_confirmation' => 'required',
                'old_password'          => 'required'
            ];
        }
    }
}
