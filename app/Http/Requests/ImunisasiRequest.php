<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImunisasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jenis_imunisasi'  => 'required|string',
            'tanggal'          => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'jenis_imunisasi'   => 'Jenis Imunisasi',
            'tanggal'           => 'Tanggal',
        ];
    }
}
