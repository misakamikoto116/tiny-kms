<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class OrangTuaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('PUT') || $this->isMethod('PATCH')) {
            $pass = 'nullable';
            $foto   = 'nullable';
        } else {
            $pass = 'required|min:6|max:20';
            $foto = 'required';
        }

        return [
            'name'          => 'required|string|max:120',
            'username'      => 'required|unique:users,username,' . $this->orangtua,
            'email'         => 'required|unique:users,email,' . $this->orangtua,
            'password'      => $pass,
            'no_kk'         => 'required|string',
            'nik'           => 'required|string',
            'no_hp'         => 'required|numeric',
            'jenis_kelamin' => 'required|string',
            'tgl_lahir'     => 'required',
            'rt'            => 'required|string',
            'kecamatan'     => 'required|string',
            'kelurahan'     => 'required|string',
            'alamat'        => 'required',
            'foto'          => $foto . '|image|mimes:jpeg,jpg,png,gif,JPG,JPEG'
        ];
    }

    public function attributes()
    {
        return [
            'name'          => 'Nama',
            'username'      => 'Username',
            'email'         => 'Email',
            'password'      => 'Password',
            'no_hp'         => 'Nomor HP',
            'nik'           => 'NIK',
            'no_kk'         => 'Nomor KK',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tgl_lahir'     => 'Tanggal Lahir',
            'kecamatan'     => 'Kecamatan',
            'kelurahan'     => 'Kelurahan',
            'rt'            => 'RT',
            'alamat'        => 'Alamat',
            'foto'          => 'Foto'
        ];
    }
}
