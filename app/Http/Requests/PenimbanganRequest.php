<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenimbanganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal'           => 'required|string',
            'berat_badan'       => 'required',
            'tinggi_badan'      => 'required',
            'lingkar_kepala'    => 'required',
            'asi_eksklusif'     => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'tanggal'       => 'Tanggal',
            'jenis'         => 'Jenis',
        ];
    }
}
