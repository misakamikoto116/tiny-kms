<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PosyanduRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|max:120',
            'desa'          => 'required|string',
            'jumlah_kader'  => 'required|numeric',
            'ketua'         => 'required|string',
            'rt'            => 'required|string',
            'kecamatan'     => 'required|string',
            'alamat'        => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name'          => 'Nama',
            'desa'          => 'Desa',
            'jumlah_kader'  => 'Jumlah Kader',
            'kecamatan'     => 'Kecamatan',
            'rt'            => 'RT',
            'alamat'        => 'Alamat',
            'ketua'         => 'Ketua',
        ];
    }
}
