<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penimbangan extends Model
{
    protected $table = 'penimbangan';

    protected $fillable = [
        'anak_id',
        'posyandu_id',
        'petugas_id',
        'tanggal',
        'berat_badan',
        'tinggi_badan',
        'jml_umur',
        'lingkar_kepala',
        'keterangan',
        'asi_eksklusif',
        'color',
        'position'
    ];

    protected $dates = [
        'tanggal',
    ];

    protected $casts = [
        'asi_eksklusif' => 'boolean'
    ];

    protected $appends = ['umur', 'tanggal_formatted', 'keterangan_formatted', 'umur_bulan', 'umur_bulan_formatted'];

    public $timestamps = true;

    public function anak()
    {
        return $this->belongsTo(Anak::class, 'anak_id');
    }

    public function posyandu()
    {
        return $this->belongsTo(Posyandu::class, 'posyandu_id');
    }

    public function petugas()
    {
        return $this->belongsTo(Petugas::class, 'petugas_id');
    }

    public function getTanggalFormattedattribute()
    {
        return $this->tanggal->format('d F Y');
    }

    public function getUmurattribute()
    {
        $umur = $this->tanggal->diff($this->anak->tgl_lahir)->format('%y Tahun %m Bulan dan %d Hari');

        return $umur;
    }

    public function getUmurBulanattribute()
    {
        $umur = $this->tanggal->diffInMonths($this->anak->tgl_lahir);

        return $umur;
    }

    public function getUmurBulanFormattedattribute()
    {
        $umur = $this->tanggal->diffInMonths($this->anak->tgl_lahir);

        return $umur . ' Bulan';
    }

    public function getKeteranganFormattedattribute()
    {
        switch ($this->keterangan) {
            case '1':
                return 'Naik';
            case '2':
                return 'Turun';
            case '3':
                return 'Bulan Lalu Tidak Nimbang';
            default:
                return 'Baru';
        }
    }

    public function scopeFilter($query, $request)
    {
        $user = auth()->user();
        if ($request->has('q')) {
            $query->WhereHas('anak', function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->get('q') . '%');
            })
                ->orWhereHas('posyandu', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                })->OrwhereHas('petugas', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                });
        }

        return $query;
    }
}
