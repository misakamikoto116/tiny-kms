<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{
    protected $table = 'anak';

    protected $fillable = [
        'nik',
        'name',
        'tgl_lahir',
        'jenis_kelamin',
        'rt',
        'alamat',
        'kecamatan',
        'kelurahan',
        'ortu_id',
        'generateId',
        'foto',
        'bapak',
        'ibu',
        'is_in_anggana'
    ];

    protected $dates = [
        'tgl_lahir',
    ];

    public $timestamps = true;

    protected $appends = [
        'status_used',
        'umur',
        'tgl_lahir_formatted',
        'jenis_kelamin_formatted',
        'umur_bulan',
        'umur_bulan_formatted',
        'is_in_anggana_formatted',
        'baru',
        'naik',
        'turun',
        'tidak_timbang'
    ];

    public function orang_tua()
    {
        return $this->belongsTo(OrangTua::class, 'ortu_id');
    }

    public function vitamin_a()
    {
        return $this->hasMany(VitaminA::class, 'anak_id');
    }

    public function imunisasi()
    {
        return $this->hasMany(Imunisasi::class, 'anak_id');
    }

    public function penimbangan()
    {
        return $this->hasMany(Penimbangan::class, 'anak_id');
    }

    public function penimbangan_turun()
    {
        return $this->hasMany(Penimbangan::class, 'anak_id')->where('keterangan', '2');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%')
                ->orWhere('generateId', 'like', '%' . $request->get('q') . '%')
                ->OrWhereHas('orang_tua', function ($q) use ($request) {
                    $q->whereHas('orang_tua', function ($q) use ($request) {
                        $q->whereHas('user', function ($q) use ($request) {
                            $q->where('name', 'like', '%' . $request->get('q') . '%');
                        });
                    });
                });
        }

        return $query;
    }

    public function scopeLaki($query)
    {
        $query->where('jenis_kelamin', 'LK');
    }

    public function scopePerempuan($query)
    {
        $query->where('jenis_kelamin', 'PR');
    }

    public function getBaruattribute()
    {
        return $this->penimbangan()->where('keterangan', '0')->count();
    }

    public function getNaikattribute()
    {
        return $this->penimbangan()->where('keterangan', '1')->count();
    }

    public function getTurunattribute()
    {
        return $this->penimbangan()->where('keterangan', '2')->count();
    }

    public function getTidakTimbangattribute()
    {
        return $this->penimbangan()->where('keterangan', '3')->count();
    }

    public function getStatusUsedattribute()
    {
        $status = false;

        $vitamin_a = $this->vitamin_a->count();
        $imunisasi = $this->imunisasi->count();
        $penimbangan = $this->penimbangan->count();

        if ($vitamin_a > 0) {
            $status = true;
        } elseif ($imunisasi > 0) {
            $status = true;
        } elseif ($penimbangan > 0) {
            $status = true;
        }

        return $status;
    }

    public function getTglLahirFormattedattribute()
    {
        return $this->tgl_lahir->format('d F Y');
    }

    public function getUmurattribute()
    {
        $umur = $this->tgl_lahir->diff(Carbon::now())->format('%y Tahun %m Bulan dan %d Hari');

        return $umur;
    }

    public function getUmurBulanattribute()
    {
        $umur = $this->tgl_lahir->diffInMonths(Carbon::now());

        return $umur;
    }

    public function getUmurBulanFormattedattribute()
    {
        $umur = $this->tgl_lahir->diffInMonths(Carbon::now());

        return $umur . ' Bulan';
    }

    public function getJenisKelaminFormattedattribute()
    {
        if ($this->jenis_kelamin === 'LK') return 'Laki-laki';
        else return 'Perempuan';
    }

    public function getIsInAngganaFormattedattribute()
    {
        if ($this->is_in_anggana) {
            $stat = 'Warga Anggana';
        } else {
            $stat = 'Warga Luar Anggana';
        }

        return $stat;
    }
}
