<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imunisasi extends Model
{
    protected $table = 'imunisasi';

    protected $fillable = [
        'tanggal',
        'anak_id',
        'jenis_imunisasi',
        'posyandu_id',
        'petugas_id',
    ];

    protected $dates = [
        'tanggal',
    ];

    public $timestamps = true;

    protected $appends = ['umur', 'tanggal_formatted', 'umur_bulan', 'umur_bulan_formatted'];

    public function anak()
    {
        return $this->belongsTo(Anak::class, 'anak_id');
    }

    public function petugas()
    {
        return $this->belongsTo(Petugas::class, 'petugas_id');
    }

    public function posyandu()
    {
        return $this->belongsTo(Posyandu::class, 'posyandu_id');
    }

    public function getTanggalFormattedattribute()
    {
        return $this->tanggal->format('d F Y');
    }

    public function getUmurattribute()
    {
        $umur = $this->tanggal->diff($this->anak->tgl_lahir)->format('%y Tahun %m Bulan dan %d Hari');
  
        return $umur;
    }

    public function getUmurBulanattribute()
    {
        $umur = $this->tanggal->diffInMonths($this->anak->tgl_lahir);
  
        return $umur;
    }

    public function getUmurBulanFormattedattribute()
    {
        $umur = $this->tanggal->diffInMonths($this->anak->tgl_lahir);
  
        return $umur . ' Bulan';
    }

    public function scopeFilter($query, $request)
    {
        $user = auth()->user();
        
        if ($request->has('q')) {
            $query->where('kegiatan', 'like', '%' . $request->get('q') . '%')
                    ->OrWhereHas('anak', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                    })
                    ->OrWhereHas('petugas', function($q) use($request) {
                        $q->where('name', 'like', '%' . $request->get('q') . '%');
                    })
                   ->orWhereHas('posyandu', function($q) use($request) {
                    $q->whereHas('user', function($qe) use ($request) {
                        $qe->where('name', 'like', '%' . $request->get('q') . '%');
                    });
                   })->orWhere('jenis_imunisasi', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }
}
