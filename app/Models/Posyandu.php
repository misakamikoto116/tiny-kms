<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posyandu extends Model
{
    protected $table = 'posyandu';

    protected $fillable = [
        'code',
        'name',
        'jumlah_kader',
        'alamat',
        'rt',
        'kecamatan',
        'ketua',
        'desa',
    ];

    public $timestamps = true;

    protected $appends = ['status_used', 'jumlah_anak_l',  'jumlah_anak_p'];

    public function vitamin_a()
    {
        return $this->hasMany(VitaminA::class, 'posyandu_id');
    }

    public function imunisasi()
    {
        return $this->hasMany(Imunisasi::class, 'posyandu_id');
    }

    public function petugas()
    {
        return $this->hasMany(Petugas::class, 'posyandu_id');
    }

    public function penimbangan()
    {
        return $this->hasMany(Penimbangan::class, 'posyandu_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('code', 'like', '%' . $request->get('q') . '%')
                ->orWhere('name', 'like', '%' . $request->get('q') . '%')
                ->orWhere('ketua', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }

    public function getStatusUsedattribute()
    {
        $status = false;

        $vitamin_a = $this->vitamin_a->count();
        $imunisasi = $this->imunisasi->count();
        $penimbangan = $this->penimbangan->count();
        $petugas = $this->petugas->count();

        if ($vitamin_a > 0) {
            $status = true;
        } elseif ($imunisasi > 0) {
            $status = true;
        } elseif ($penimbangan > 0) {
            $status = true;
        } elseif ($petugas > 0) {
            $status = true;
        }

        return $status;
    }

    public function getJumlahAnakAttribute()
    {
        $penimbangan = $this->penimbangan->groupBy('anak_id')->count();

        return $penimbangan;
    }

    public function getJumlahAnakPAttribute()
    {
        $penimbangan = $this->penimbangan()->whereHas('anak', function ($q) {
            $q->where('jenis_kelamin', 'PR');
        })->groupBy('anak_id')->get()->count();

        return $penimbangan;
    }

    public function getJumlahAnakLAttribute()
    {
        $penimbangan = $this->penimbangan()->whereHas('anak', function ($q) {
            $q->where('jenis_kelamin', 'LK');
        })->groupBy('anak_id')->get()->count();

        return $penimbangan;
    }
}
