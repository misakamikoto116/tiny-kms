<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VitaminA extends Model
{
    protected $table = 'vitamin_a';

    protected $fillable = [
        'tanggal',
        'anak_id',
        'posyandu_id',
        'petugas_id',
        'umur',
        'jenis',
    ];

    protected $dates = [
        'tanggal',
    ];

    protected $appends = ['umur', 'tanggal_formatted', 'umur_bulan', 'umur_bulan_formatted', 'jenis_formatted'];

    public $timestamps = true;

    public function anak()
    {
        return $this->belongsTo(Anak::class, 'anak_id');
    }

    public function posyandu()
    {
        return $this->belongsTo(Posyandu::class, 'posyandu_id');
    }

    public function petugas()
    {
        return $this->belongsTo(Petugas::class, 'petugas_id');
    }

    public function getJenisFormattedAttribute() {
        $jenis = $this->jenis;
        if($jenis == 'biru') return 'Kapsul Biru 6-11 Bulan';
        else if($jenis == 'merah') return 'Kapsul Merah 12-59 Bulan';
    }

    public function getTanggalFormattedattribute()
    {
        return $this->tanggal->format('d F Y');
    }

    public function getUmurattribute()
    {
        $umur = $this->tanggal->diff($this->anak->tgl_lahir)->format('%y Tahun %m Bulan dan %d Hari');
  
        return $umur;
    }

    public function getUmurBulanattribute()
    {
        $umur = $this->tanggal->diffInMonths($this->anak->tgl_lahir);
  
        return $umur;
    }

    public function getUmurBulanFormattedattribute()
    {
        $umur = $this->tanggal->diffInMonths($this->anak->tgl_lahir);
  
        return $umur . ' Bulan';
    }

    public function scopeFilter($query, $request)
    {
        $user = auth()->user();
        if ($request->has('q')) {
            $query->where('jenis', 'like', '%' . $request->get('q') . '%')
                    >OrWhereHas('anak', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                    })
                   ->orWhereHas('posyandu', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                   })
                   ->orWhereHas('petugas', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                   });
        }

        if ($request->has('jenis')) {
            $query->where('jenis', $request->get('jenis'));
        }

        return $query;
    }

    public function scopeJenis($query, $jenis)
    {
        $query->where('jenis', $jenis);
    }
}
