<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zscore extends Model
{
    protected $fillable = [
        'bulan',
        'color',
        'position',
        'min',
        'max',
    ];

    public $timestamps = true;
}
