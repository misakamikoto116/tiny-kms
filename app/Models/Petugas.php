<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'petugas';

    protected $fillable = [
        'user_id',
        'no_ktp',
        'jabatan',
        'posyandu_id',
        'is_in_anggana'
    ];

    public $timestamps = true;

    protected $appends = ['status_used', 'is_in_anggana_formatted'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function vitamin_a()
    {
        return $this->hasMany(VitaminA::class, 'petugas_id');
    }

    public function penimbangan()
    {
        return $this->hasMany(Penimbangan::class, 'petugas_id');
    }

    public function posyandu()
    {
        return $this->belongsTo(Posyandu::class, 'posyandu_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('jabatan', 'like', '%' . $request->get('q') . '%')
                ->orWhereHas('user', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                })
                ->orWhereHas('posyandu', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                });
        }

        return $query;
    }

    public function getStatusUsedattribute()
    {
        $status = false;

        $vitamin_a = $this->vitamin_a->count();
        $penimbangan = $this->penimbangan->count();

        if ($vitamin_a > 0) {
            $status = true;
        } elseif ($penimbangan > 0) {
            $status = true;
        }

        return $status;
    }

    public static function kelurahans()
    {
        return [
            'Sungai meriam',
            'Anggana',
            'Sidomulyo',
            'Kutai lama',
            'Handil terusan',
            'Muara pantuan',
            'Sepatin',
            'Tani baru',
        ];
    }

    public static function kelurahan_choices()
    {
        return [
            'Sungai meriam' => 'Sungai meriam',
            'Anggana' => 'Anggana',
            'Sidomulyo' => 'Sidomulyo',
            'Kutai lama' => 'Kutai lama',
            'Handil terusan' => 'Handil terusan',
            'Muara pantuan' => 'Muara pantuan',
            'Sepatin' => 'Sepatin',
            'Tani baru' => 'Tani baru',
        ];
    }

    public function getIsInAngganaFormattedattribute()
    {
        if ($this->is_in_anggana) {
            $stat = 'Warga Anggana';
        } else {
            $stat = 'Warga Luar Anggana';
        }

        return $stat;
    }
}
