<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';

    protected $fillable = [
        'tanggal',
        'jam',
        'posyandu_id',
        'kegiatan'
    ];

    public $timestamps = true;

    protected $appends = ['jam_formatted'];

    public function posyandu()
    {
        return $this->belongsTo(Posyandu::class, 'posyandu_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('kegiatan', 'like', '%' . $request->get('q') . '%')
                   ->orWhereHas('posyandu', function($q) use($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                   });
        }

        return $query;
    }

    public function getJamFormattedattribute()
    {
        return Str::substr($this->jam, 0, 5);
    }
}
