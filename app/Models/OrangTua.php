<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrangTua extends Model
{
    protected $table = 'orang_tua';

    protected $fillable = [
        'nik',
        'no_kk',
        'user_id',
        'is_in_anggana'
    ];

    public $timestamps = true;

    protected $appends = ['jumlah_anak', 'is_in_anggana_formatted'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function anak()
    {
        return $this->hasMany(Anak::class, 'ortu_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('no_kk', 'like', '%' . $request->get('q') . '%')
                ->orWhereHas('user', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->get('q') . '%');
                });
        }

        return $query;
    }

    public function getStatusUsedattribute()
    {
        $status = false;

        $anak = $this->anak->count();

        if ($anak > 0) {
            $status = true;
        }

        return $status;
    }

    public function getJumlahAnakattribute()
    {
        return $this->anak->count();
    }

    public function getIsInAngganaFormattedattribute()
    {
        if ($this->is_in_anggana) {
            $stat = 'Warga Anggana';
        } else {
            $stat = 'Warga Luar Anggana';
        }

        return $stat;
    }
}
