<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Petugas;
use App\Models\OrangTua;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * NOTE: LK => Laki laki, PR => Perempuan
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'no_hp',
        'jenis_kelamin',
        'tgl_lahir',
        'alamat',
        'rt',
        'kecamatan',
        'kelurahan',
        'foto',
        'password',
    ];

    protected $appends = ['tgl_lahir_formatted'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        $role = auth()->user()->getRoleNames()->first();

        switch ($role) {
            case 'Petugas':
                $relation = $this->hasOne(Petugas::class, 'user_id');
                break;

            case 'Orang_Tua':
                $relation = $this->hasOne(OrangTua::class, 'user_id');
                break;

            default:
                $relation = null;
                break;
        }

        return $relation;
    }

    public function petugas()
    {
        return $this->hasOne(Petugas::class, 'user_id');
    }

    public function orang_tua()
    {
        return $this->hasOne(OrangTua::class, 'user_id');
    }

    public function getTglLahirFormattedattribute()
    {
        return Carbon::parse($this->tgl_lahir)->format('d F Y');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('name', 'like', '%' . $request->get('q') . '%')
                ->where('email', 'like', '%' . $request->get('q') . '%')
                ->where('no_hp', 'like', '%' . $request->get('q') . '%')
                ->where('username', 'like', '%' . $request->get('q') . '%');
        }

        return $query;
    }
}
