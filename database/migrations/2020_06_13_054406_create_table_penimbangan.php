<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePenimbangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penimbangan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('anak_id');
            $table->unsignedBigInteger('petugas_id');
            $table->unsignedBigInteger('posyandu_id');

            $table->foreign('anak_id')
                ->references('id')
                ->on('anak');

            $table->foreign('posyandu_id')
                ->references('id')
                ->on('posyandu');

            $table->foreign('petugas_id')
                ->references('id')
                ->on('petugas');

            $table->dateTime('tanggal');
            $table->double('berat_badan');
            $table->double('tinggi_badan');
            $table->integer('jml_umur')->nullable();
            $table->double('lingkar_kepala');
            $table->boolean('asi_eksklusif');
            $table->enum('keterangan', [0, 1, 2, 3])->comment('[0] Baru | [1] Naik | [2] Turun | [3] Bulan Lalu Tidak nimbang')->default(0);
            $table->enum('color', ['red', 'yellow', 'orange', 'green', 'undefined']);
            $table->enum('position', ['top', 'bottom', 'middle', 'undefined']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penimbangan');
    }
}
