<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Petugas;

class CreateTableAnak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anak', function (Blueprint $table) {
            $table->id();
            $table->string('nik');
            $table->string('generateId');
            $table->string('name');
            $table->date('tgl_lahir');
            $table->string('jenis_kelamin');
            $table->string('bapak');
            $table->string('ibu');
            $table->text('alamat');
            $table->string('rt');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->unsignedBigInteger('ortu_id');

            $table->foreign('ortu_id')
                ->references('id')
                ->on('orang_tua');
            $table->text('foto')->nullable();
            $table->boolean('is_in_anggana')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anak');
    }
}
