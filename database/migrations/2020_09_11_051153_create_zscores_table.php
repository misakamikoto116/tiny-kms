<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZscoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zscores', function (Blueprint $table) {
            $table->id();
            $table->integer('bulan');
            $table->enum('color', ['red', 'yellow', 'orange', 'green']);
            $table->enum('position', ['top', 'bottom', 'middle']);
            $table->double('min');
            $table->double('max');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zscores');
    }
}
