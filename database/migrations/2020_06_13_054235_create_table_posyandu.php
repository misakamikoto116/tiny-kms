<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Petugas;

class CreateTablePosyandu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posyandu', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('alamat');
            $table->string('rt');
            $table->string('kecamatan')->default('Anggana');
            $table->enum('desa', Petugas::kelurahans());
            $table->string('jumlah_kader');
            $table->string('ketua');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posyandu');
    }
}
