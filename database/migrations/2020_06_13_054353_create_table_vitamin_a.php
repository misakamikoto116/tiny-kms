<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVitaminA extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitamin_a', function (Blueprint $table) {
            $table->id();
            $table->dateTime('tanggal');
            $table->unsignedBigInteger('anak_id');
            $table->unsignedBigInteger('posyandu_id');
            $table->unsignedBigInteger('petugas_id');
            
            $table->foreign('anak_id')
                ->references('id')
                ->on('anak');
            
            $table->foreign('posyandu_id')
                ->references('id')
                ->on('posyandu');

            $table->foreign('petugas_id')
                ->references('id')
                ->on('petugas');

            $table->enum('jenis', ['biru', 'merah'])->comment('[Biru] 6-11 bulan | [Merah] 12 - 59 Bulan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitamin_a');
    }
}
