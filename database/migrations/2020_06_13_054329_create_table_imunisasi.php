<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableImunisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imunisasi', function (Blueprint $table) {
            $table->id();
            $table->dateTime('tanggal');
            $table->unsignedBigInteger('anak_id');
            $table->string('jenis_imunisasi');
            $table->unsignedBigInteger('posyandu_id');
            $table->unsignedBigInteger('petugas_id');


            $table->foreign('anak_id')
                ->references('id')
                ->on('anak');
            $table->foreign('posyandu_id')
                ->references('id')
                ->on('posyandu');
            $table->foreign('petugas_id')
                ->references('id')
                ->on('petugas');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imunisasi');
    }
}
