<?php

use App\User;
use App\Models\Anak;
use App\Models\Role;
use App\Models\Petugas;
use App\Models\OrangTua;
use Illuminate\Database\Seeder;
use App\Models\PivotOrangTuaAnak;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // set default roles
        $role_data = [
            ['name' => 'Admin', 'guard_name' => 'web'],
            ['name' => 'Petugas', 'guard_name' => 'web'],
            ['name' => 'Orang_Tua', 'guard_name' => 'web'],
        ];

        Role::insert($role_data);

        // Admin => ID = 1
        $admin_data =  [
            'name'          => 'Admin',
            'email'         => 'admin@gmail.com',
            'username'      => 'admin',
            'password'      => bcrypt('123123'),
            'no_hp'         => '080808080808',
            'jenis_kelamin' => 'LK',
            'tgl_lahir'     => '2000-07-06',
        ];

        $admin = User::create($admin_data);
        $admin->assignRole(Role::find(1));

        // Petugas => ID = 2
        $petugas_data =  [
            'name'          => 'Petugas',
            'email'         => 'petugas@gmail.com',
            'username'      => 'petugas',
            'password'      => bcrypt('123123'),
            'no_hp'         => '080808080808',
            'jenis_kelamin' => 'PR',
            'tgl_lahir'     => '2000-07-06',
            'alamat'        => 'Samarinda',
            'rt'            => 'RT 02',
            'kecamatan'     => 'Anggana',
            'kelurahan'     => 'Anggana',
        ];

        $petugas = User::create($petugas_data);
        $petugas->assignRole(Role::find(2));

        Petugas::create([
            'no_ktp'        => '12200020000',
            'user_id'       => $petugas->id,
            'jabatan'       => 'Kepala Petugas',
            'is_in_anggana' =>  true,
            'posyandu_id'   => 1
        ]);

        // Orang Tua => ID = 3
        $orang_tua_data =  [
            'name'          => 'Orang Tua',
            'email'         => 'orangtua@gmail.com',
            'username'      => 'orang_tua',
            'password'      => bcrypt('123123'),
            'no_hp'         => '080808080808',
            'jenis_kelamin' => 'LK',
            'tgl_lahir'     => '2000-07-06',
            'alamat'        => 'Samarinda',
            'rt'            => 'RT 12',
            'kecamatan'     => 'Sidomulyo',
            'kelurahan'     => 'Sidomulyo',
        ];

        $orang_tua = User::create($orang_tua_data);
        $orang_tua->assignRole(Role::find(3));

        OrangTua::create([
            'no_kk'         => '200012000012300032',
            'nik'           => '123399393939123332',
            'is_in_anggana' =>  false,
            'user_id'       => 3,
        ]);

        Anak::create([
            'generateId'    => '20090719' . rand(1111, 9999),
            'name'          => 'Joe',
            'tgl_lahir'     => '2020-07-19',
            'jenis_kelamin' => 'PR',
            'bapak'         => 'Jon',
            'ibu'           => 'Jane',
            'ortu_id'       => 1,
            'rt'            => 'RT 12',
            'kecamatan'     => 'Sidomulyo',
            'kelurahan'     => 'Sidomulyo',
            'alamat'        => 'Samarinda'
        ]);
    }
}
