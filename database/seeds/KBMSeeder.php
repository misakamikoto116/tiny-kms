<?php

use App\Models\KBM;
use Illuminate\Database\Seeder;

class KBMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['umur' => 0, 'kbm' => null],
            ['umur' => 1, 'kbm' => 800],
            ['umur' => 2, 'kbm' => 900],
            ['umur' => 3, 'kbm' => 800],
            ['umur' => 4, 'kbm' => 600],
            ['umur' => 5, 'kbm' => 500],
            ['umur' => 6, 'kbm' => 400],
            ['umur' => 7, 'kbm' => 300],
            ['umur' => 8, 'kbm' => 300],
            ['umur' => 9, 'kbm' => 300],
            ['umur' => 10, 'kbm' => 300],
            ['umur' => 11, 'kbm' => 200],
            ['umur' => 12, 'kbm' => 200],
            ['umur' => 13, 'kbm' => 200],
            ['umur' => 14, 'kbm' => 200],
            ['umur' => 15, 'kbm' => 200],
            ['umur' => 16, 'kbm' => 200],
            ['umur' => 17, 'kbm' => 200],
            ['umur' => 18, 'kbm' => 200],
            ['umur' => 19, 'kbm' => 200],
            ['umur' => 20, 'kbm' => 200],
            ['umur' => 21, 'kbm' => 200],
            ['umur' => 22, 'kbm' => 200],
            ['umur' => 23, 'kbm' => 200],
            ['umur' => 24, 'kbm' => 200],
            ['umur' => 25, 'kbm' => 200],
            ['umur' => 26, 'kbm' => 200],
            ['umur' => 27, 'kbm' => 200],
            ['umur' => 28, 'kbm' => 200],
            ['umur' => 29, 'kbm' => 200],
            ['umur' => 30, 'kbm' => 200],
            ['umur' => 31, 'kbm' => 200],
            ['umur' => 32, 'kbm' => 200],
            ['umur' => 33, 'kbm' => 200],
            ['umur' => 34, 'kbm' => 200],
            ['umur' => 35, 'kbm' => 200],
            ['umur' => 36, 'kbm' => 200],
            ['umur' => 37, 'kbm' => 200],
            ['umur' => 38, 'kbm' => 200],
            ['umur' => 39, 'kbm' => 200],
            ['umur' => 40, 'kbm' => 200],
            ['umur' => 41, 'kbm' => 200],
            ['umur' => 42, 'kbm' => 200],
            ['umur' => 43, 'kbm' => 200],
            ['umur' => 44, 'kbm' => 200],
            ['umur' => 45, 'kbm' => 200],
            ['umur' => 46, 'kbm' => 200],
            ['umur' => 47, 'kbm' => 200],
            ['umur' => 48, 'kbm' => 200],
            ['umur' => 49, 'kbm' => 200],
            ['umur' => 50, 'kbm' => 200],
            ['umur' => 51, 'kbm' => 200],
            ['umur' => 52, 'kbm' => 200],
            ['umur' => 53, 'kbm' => 200],
            ['umur' => 54, 'kbm' => 200],
            ['umur' => 55, 'kbm' => 200],
            ['umur' => 56, 'kbm' => 200],
            ['umur' => 57, 'kbm' => 200],
            ['umur' => 58, 'kbm' => 200],
            ['umur' => 59, 'kbm' => 200],
            ['umur' => 60, 'kbm' => 200],
        ];

        KBM::insert($data);
    }
}
