<?php

use Illuminate\Database\Seeder;
use App\Models\Posyandu;

class PosyanduSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Posyandu::create([
            'code'          => 'P001',
            'name'          => 'Posyandu Utama',
            'desa'          => 'Sungai meriam',
            'kecamatan'     => 'Anggana',
            'rt'            => '002',
            'jumlah_kader'  => 3,
            'ketua'         => 'Joel'
        ]);
    }
}
