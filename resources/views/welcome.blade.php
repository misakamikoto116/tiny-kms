<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Kartu Menuju Sehat</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
        <!-- Style -->
        <link href="{{ asset('template/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

        <!-- Script -->
        <script src="{{ asset('template/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('template/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>

        <style>
            .navbar {
                background: rgba(4, 147, 114, 0.5) !important;
            }

            .content {
                min-height: 100vh;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }

            .img {
                width: 150px;
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
            <a class="navbar-brand font-weight-bold" href="#">
                Kartu Menuju Sehat
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    @auth
                        <li class="nav-item active">
                            <a class="nav-link btn btn-sm btn-primary" href="{{ url('/home') }}">Home</a>
                        </li>
                    @else
                        <li class="nav-item mr-0 mr-md-2 mb-2 mb-md-0 active">
                            <a class="nav-link btn btn-sm btn-primary" href="{{ route('login') }}">Login</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link btn btn-sm btn-light text-dark" href="{{ route('register') }}">Daftar</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </nav>
        <div class="content text-center">
            <img src="{{ asset('template/images/icon/puskesmas-logo.png') }}" class="img" alt="Puskesmas" />
            <h2>Selamat datang di Aplikasi Kartu Menuju Sehat</h2>
        </div>
    </body>
</html>
