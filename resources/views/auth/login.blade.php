@extends('auth.layouts')

@section('content')
<div class="container">
    <div class="login-wrap">
        <div class="login-content">
            <div class="login-logo">
                    {{-- <img src="{{ asset('template/images/icon/logo.png') }}" alt="CoolAdmin"> --}}
                <h2>Kartu Menuju Sehat</h2>
            </div>
            <div class="login-form">
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Email atau Username</label>
                        <input class="au-input au-input--full form-control @error('username') is-invalid @enderror" type="text" name="username" placeholder="Email Atau Username">
                        @error('username')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="au-input au-input--full form-control @error('password') is-invalid @enderror" type="password" name="password" placeholder="Password">
                        @error('password')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="login-checkbox">
                        <label>
                            <input type="checkbox" name="remember">Remember Me
                        </label>
                        <label>
                            <a href="{{ route('password.request') }}">Lupa Password?</a>
                        </label>
                    </div>
                    <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                </form>
                <div class="register-link">
                    <p>
                        Belum punya akun?
                        <a  href="{{ route('register') }}">Register di sini</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
