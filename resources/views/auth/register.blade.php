@extends('auth.layouts')

@section('content')
<div class="container">
    <div class="login-wrap">
        <div class="login-content">
            <div class="login-logo">
                <h2>Kartu Menuju Sehat</h2>
                <small>Daftar orangtua</small>
            </div>
            <div class="login-form">
                <form action="{{ route('orangtua.register') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>No. KK</label>
                        <input
                            class="au-input au-input--full form-control @error('no_kk') is-invalid @enderror"
                            type="number"
                            name="no_kk"
                            placeholder="Masukkan no kartu keluarga"
                            value="{{ old('no_kk') }}"
                        >
                        @error('no_kk')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>NIK</label>
                        <input
                            id="nik"
                            class="au-input au-input--full form-control @error('nik') is-invalid @enderror"
                            type="number"
                            name="nik"
                            placeholder="Masukkan NIK"
                            value="{{ old('nik') }}"
                        >
                        <small class="help-block form-text text-info">Ket: <span id="ket_nik">-</span></small>
                        @error('nik')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input
                            class="au-input au-input--full form-control @error('name') is-invalid @enderror"
                            type="text"
                            name="name"
                            placeholder="Masukkan nama lengkap"
                            value="{{ old('name') }}"
                        >
                        @error('name')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input
                            class="au-input au-input--full form-control date @error('tgl_lahir') is-invalid @enderror"
                            type="text"
                            name="tgl_lahir"
                            placeholder="Masukkan tanggal lahir"
                            value="{{ old('tgl_lahir') }}"
                        >
                        @error('tgl_lahir')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select style="font-size: 0.9rem" name="jenis_kelamin" id="jenis_kelamin"
                            class="form-control select2-exception @error('jenis_kelamin') is-invalid @enderror">
                            <option disabled selected value="0">Pilih jenis kelamin</option>
                            <option value="LK" @if(old('jenis_kelamin') === 'LK') selected @endif>Laki-laki</option>
                            <option value="PR" @if(old('jenis_kelamin') === 'PR') selected @endif>Perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>No. HP</label>
                        <input
                            class="au-input au-input--full form-control @error('no_hp') is-invalid @enderror"
                            type="number"
                            name="no_hp"
                            placeholder="Masukkan no hp"
                            value="{{ old('no_hp') }}"
                        >
                        @error('no_hp')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <input
                            class="au-input au-input--full form-control @error('kecamatan') is-invalid @enderror"
                            type="text"
                            name="kecamatan"
                            placeholder="Masukkan kecamatan"
                            value="{{ old('kecamatan') }}"
                        >
                        @error('kecamatan')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Kelurahan/Desa</label>
                        <input
                            class="au-input au-input--full form-control @error('kelurahan') is-invalid @enderror"
                            type="text"
                            name="kelurahan"
                            placeholder="Masukkan kelurahan/desa"
                            value="{{ old('kelurahan') }}"
                        >
                        @error('kelurahan')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>RT</label>
                        <input
                            class="au-input au-input--full form-control @error('rt') is-invalid @enderror"
                            type="number"
                            name="rt"
                            placeholder="Masukkan rt"
                            value="{{ old('rt') }}"
                        >
                        @error('rt')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input
                            class="au-input au-input--full form-control @error('alamat') is-invalid @enderror"
                            type="text"
                            name="alamat"
                            placeholder="Masukkan alamat"
                            value="{{ old('alamat') }}"
                        >
                        @error('alamat')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input
                            class="au-input au-input--full form-control @error('username') is-invalid @enderror"
                            type="text"
                            name="username"
                            placeholder="Masukkan username"
                            value="{{ old('username') }}"
                        >
                        @error('username')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input
                            class="au-input au-input--full form-control @error('email') is-invalid @enderror"
                            type="text"
                            name="email"
                            placeholder="Masukkan email"
                            value="{{ old('email') }}"
                        >
                        @error('email')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input
                            class="au-input au-input--full form-control @error('password') is-invalid @enderror"
                            type="password"
                            name="password"
                            placeholder="Masukkan password"
                            value="{{ old('password') }}"
                        >
                        @error('password')
                            <small class="help-block form-text">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="row form-group">
                        <div class="col col-auto">
                          <img id="foto-preview" src="{{ asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
                        </div>
                        <div class="col">
                          <label for="file-input" class=" form-control-label">Foto</label>
                          <input name="foto" type="file" id="file-input" name="file-input"
                            value="{{ old('foto') }}"
                            class="form-control-file @error('foto') is-invalid @enderror">
                          @error('foto')
                            <div class="invalid-feedback">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                    </div>
                    <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
                </form>
                <div class="register-link">
                    <p>
                        Sudah punya akun?
                        <a href="{{ route('login') }}">Login</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })

      $('#nik').on('input', e => {
        const val = e.target.value;
        const id = val.substr(0, 6);
        const kodeAnggana = '640204';
        if(id === kodeAnggana) {
            $('#ket_nik').text('Warga Anggana')
        } 
        else if(id.length < 1) {
            $('#ket_nik').text('-')
        }
        else {
            $('#ket_nik').text('Bukan warga Anggana')
        }
      })
    </script>
@endpush