@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">Jadwal Kegiatan</h2>
        @role('Petugas')
          <a
            class="au-btn au-btn-icon au-btn--blue"
            href={{ url('/petugas/jadwal-kegiatan/create') }}
          >
            <i class="zmdi zmdi-plus"></i>
            Tambah jadwal kegiatan
          </a>
        @endrole
      </div>
    </div>
  </div>
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col col-12">
      <div class="table-responsive">
        <table class="table table-borderless table-data3">
          <thead>
            <tr>
              <th>tanggal</th>
              <th>jam</th>
              <th>tempat kegiatan</th>
              <th>kecamatan</th>
              <th>kelurahan/desa</th>
              <th>alamat</th>
              <th>kegiatan</th>
              @role('Petugas')
                <th>option</th>
              @endrole
            </tr>
          </thead>
          <tbody>
            @forelse ($datas as $dt)
              <tr>
                <td>{{ $dt->tanggal }}</td>
                <td>{{ $dt->jam_formatted }}</td>
                <td>{{ $dt->posyandu->name }}</td>
                <td>{{ $dt->posyandu->kecamatan }}</td>
                <td>{{ $dt->posyandu->desa }}</td>
                <td>{{ $dt->posyandu->alamat }}</td>
                <td>{{ $dt->kegiatan }}</td>
                @role('Petugas')
                  <td>
                    <a class="btn btn-sm btn-info d-inline-block" href={{ url('/petugas/jadwal-kegiatan/'. $dt->id .'/edit') }}>Edit</a>
                    <form class="d-inline-block" method="post" action={{ url('/petugas/jadwal-kegiatan/' . $dt->id) }} onSubmit="return confirm('Ingin menghapus?')">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                  </td>
                @endrole
              </tr>
            @empty
              <tr>
                <td colspan="100%" class="text-center p-5"><h5>Tidak ada data.</h5></td>
              </tr>
            @endforelse
          </tbody>
        </table>
        <div class="d-flex justify-content-end m-t-25">
          {{ $datas->appends(request()->all())->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection