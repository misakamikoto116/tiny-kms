@extends('layouts.app')

@section('content')
<form method="POST" action={{ route('petugas.jadwal-kegiatan.update', $data->id) }}>
  @csrf
  @method('PUT')
  <div class="card">
    <div class="card-header">
        <strong>Jadwal Kegiatan</strong>
        <small> Edit</small>
    </div>
    <div class="card-body card-block">
      <div class="row">
        <div class="col">
          <div class="form-group">
            <label for="tanggal" class="form-control-label">Tanggal</label>
            <input name="tanggal" type="text" id="tanggalss" placeholder="Masukkan tanggal"
              class="form-control @error('tanggal') is-invalid @enderror"
              value="{{ $data->tanggal }}">
            @error('tanggal')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label for="jam" class="form-control-label">Jam</label>
            <input name="jam" type="text" id="jam" placeholder="Pilih jam"
              class="form-control @error('jam') is-invalid @enderror"
              value="{{ $data->jam }}">
            @error('jam')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="posyandu" class="form-control-label">Tempat Kegiatan (posyandu)</label>
        <select name="posyandu_id" id="posyandu" class="form-control @error('posyandu_id') is-invalid @enderror">
          <option disabled selected value="0">Pilih tempat kegiatan</option>
          @foreach ($tempat_kegiatan as $posyandu)
            <option value="{{ $posyandu->id }}" @if($data->posyandu_id == $posyandu->id) selected @endif>{{ $posyandu->name }}</option>
          @endforeach
        </select>
        @error('posyandu_id')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="nama_kegiatan" class="form-control-label">Nama kegiatan</label>
        <input name="kegiatan" type="text" id="nama_kegiatan" placeholder="Masukkan nama kegiatan"
          class="form-control @error('kegiatan') is-invalid @enderror"
          value="{{ $data->kegiatan }}">
        @error('kegiatan')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>
    <div class="card-footer text-right">
      <a href={{ url('/petugas/jadwal-kegiatan') }} type="reset" class="btn btn-danger">
        <i class="fa fa-ban"></i> Cancel
      </a>
      <button type="submit" class="btn btn-primary">
        <i class="fa fa-send"></i> Submit
      </button>
    </div>
  </div>
</form>
@endsection

@push('script-addon')
  <script>
    $('#jam').datetimepicker({
      format: 'HH:mm',
    });
  </script>
@endpush