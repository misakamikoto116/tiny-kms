@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>IPV</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah virus polio yang dapat menyebabkan lumpuh layuh pada tungkai dan atau lengan
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <p class="ml-2">
      Anak yang belum pernah mendapat imunisasi polio
    </p>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <p class="ml-2">
      Riwayat reaksi alergi berat terhadap neomisin, streptomisin, polimiksin B
    </p>

    <h5 class="mt-2">Imunisasi Kejar/h5>
    <p class="ml-2">
      Dapat diberikan sesuai jadwal
    </p>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasai)</h5>
    <p class="ml-2">
      Alergi berat
    </p>

    <h5 class="mt-2">Informasi Tambahan</h5>
    <p class="ml-2">
      Sebaiknya paling sedikit mendapat 1 dosis IPV, yaitu bersamaan dengan pemberian OPV 3
    </p>

  </div>
</div>
@endsection