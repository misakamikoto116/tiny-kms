@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>PCV</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit pneumonia (radang/infeksi paru)
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum diberikan imunisasi PCV
      </li>
      <li>
        Kelompok berisiko: bayi tidak mendapat ASI, tinggal bersama perokok aktif, memiliki saudara yang dititipkan di TPA, tinggal di negara 4 musim
      </li>
      <li>
        Memiliki kondisi gangguan imunitas (HIV, defisiensi immunoglobulin, keganasan), menderita penyakti jantung dan paru kronik, gagal ginjal kronik, gangguan hati, dan pasien transplantasi organ.
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi PCV sebelumnya.
      </li>
      <li>
        Jadwal
      </li>
      <li>
        Bila diberikan pada usia 2-6 bulan, dapat diberikan sebanyak 3 dosis dengan interval 6-8 minggu, kemudian diberikan booster 1 dosis (usia 12-15 bulan).
      </li>
      <li>
        Bila diberikan pada usia 7-11 bulan, dapat diberikan sebanyak 2 dosis dengan interval 6-8 minggu, kemudian diberikan booster 1 dosis (usia 12-15 bulan).
      </li>
      <li>
        Bila diberikan pada usia 12-23 bulan, dapat diberikan sebanyak 2 dosis dengan interval 6-8 minggu, tanpa booster.
      </li>
      <li>
        Bila diberikan pada usia >24 bulan, dapat diberikan sebanyak 1 dosis saja.
      </li>
    </ul>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasai) </h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan di lokasi penyuntikan
      </li>
      <li>
        Rewel
      </li>
      <li>
        Diare
      </li>
      <li>
        Demam
      </li>
      <li>
        Gatal-gatal
      </li>
    </ul>
  </div>
</div>
@endsection