@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Rotavirus</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah diare akibat rotavirus
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua bayi berusia 2-8 bulan yang belum diberikan imunisasi rotavirus
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi rotavirus sebelumnya
      </li>
      <li>
        Jadwal
      </li>
      <li>
        Rotarix (diberikan 2 dosis): dosis pertama diberikan mulai usia 6-14 minggu dan untuk dosis selanjutnya diberikan dengan interval 4 minggu dan harus lengkap pada usia 24 minggu
      </li>
      <li>
        Rotateq (diberikan 3 dosis): dosis pertama diberikan antara usia 6-14 minggu dan pemberian berikutnya antara 4-10 minggu dan harus lengkap saat usia 32 minggu
      </li>
    </ul>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasai)</h5>
    <ul class="ml-4">
      <li>
        Demam
      </li>
      <li>
        BAB berdarah
      </li>
      <li>
        Muntah
      </li>
      <li>
        Diare
      </li>
      <li>
        Nyeri perut
      </li>
    </ul>

  </div>
</div>
@endsection