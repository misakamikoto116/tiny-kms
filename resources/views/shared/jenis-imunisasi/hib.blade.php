@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>HIB</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Infeksi HiB menyebabkan meningitis (infeksi/radang selaput otak) dan pneumonia (infeksi paru).
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum diberikan imunisasi HiB
      </li>
      <li>
        Indikasi kontra
      </li>
      <li>
        Riwayat alergi berat pada pemberian imunisasi HiB sebelumnya
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <ul class="ml-4">
      <li>
        Apabila vaksin pertama diberikan saat usia 7-11 bulan, maka dosis kedua diberikan 4 minggu setelahnya (12-14 bulan), dan dosis ketiga diberikan 8 minggu setelahnya.
      </li>
      <li>
        Apabila vaksin pertama diberikan saat usia 12-14 bulan, maka dosis kedua (terakhir) dapat diberikan 8 minggu setelahnya.
      </li>
      <li>
        Apabila vaksin diberikan saat usia 15 bulan/lebih pada anak yang belum tervaksinasi maka dapat diberikan sebanyak 1 dosis saja.
      </li>
    </ul>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasai) </h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan di lokasi penyuntikan
      </li>
      <li>
        Rewel
      </li>
      <li>
        Demam
      </li>
    </ul>

  </div>
</div>
@endsection