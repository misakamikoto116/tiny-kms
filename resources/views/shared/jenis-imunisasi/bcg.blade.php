@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>BCG</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Penyakit TBC (Tuberkolosis) yang berat
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Bayi usia <3 bulan yang belum mendapat imunisasi BCG
      </li>
      <li>
        Bayi dengan hasil tes tuberkulin (Mantoux) negatif
      </li>
      <li>
        Bayi yang memiliki kontak erat dengan penderita TB
      </li>
      <li>
        Indikasi Kontra
      </li>
      <li>
        Hasil tes Mantoux >5 mm
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, pengguna obat steroid dan imunosupresif, mendapatkan kemoterapi atau radiasi, penyakit keganasan yang mengenai sumsum tulang dan limfe, gizi buruk)
      </li>
      <li>
        Demam tinggi
      </li>
      <li>
        Infeksi kulit luas
      </li>
      <li>
        Riwayat infeksi TB
      </li>
      <li>
        Hamil
      </li>
      <li>
        Imunisasi Kejar
      </li>
      <li>
        Dapat dilakukan bila sudah dilakukan tes Mantoux terlebih dahulu.
      </li>
    </ul>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasi)</h5>
    <ul class="ml-4">
      <li>
        Bisul di tempat penyuntikan merupakan reaksi yang diharapkan muncul setelah pemberian vaksin. Muncul 3 minggu setelah penyuntikan dan sembuh dalam 2-3 bulan meninggalkan luka parut dengan diameter 4-8 mm.
      </li>
      <li>
        Bila penyuntikan terlalu dalam parut tertarik ke dalam sehingga penyuntikan tidak perlu diulang
      </li>
      <li>
        Bisul di ketiak atau leher biasanya dapat sembuh sendiri. Namun segera konsultasikan ke dokter anak anda untuk evaluasi apakah diperlukan tindakan pembedahan terhadap bisul atau tidak.
      </li>
    </ul>

    <h5 class="mt-2">Informasi Tambahan</h5>
    <ul class="ml-4">
      <li>
        Pemberian vaksin BCG dianjurkan sebelum usia 3 bulan, dengan usia optimal 2 bulan. Bila diberikan >3 bulan perlu dilakukan tes Mantoux
      </li>
    </ul>

  </div>
</div>
@endsection