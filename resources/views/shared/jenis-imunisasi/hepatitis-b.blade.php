@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Hepatitis B</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit hepatits B yang dapat menyebabkan kerusakan hati.
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Anak yang belum pernah mendapat imunisasi hepatitis B.
      </li>
      <li>
        Pasien hemodialisis (cuci darah).
      </li>
      <li>
        Kelompok berisiko: pengguna narkoba jarum suntik, homoseksual.
      </li>
      <li>
        Indikasi Kontra
      </li>
      <li>
        Riwayat alergi pada pemberian imunisasi Hepatitis B sebelumnya
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <p class="ml-2">
      Apabila sampai usia 5 tahun belum mendapatkan imunisasi Hepatitis B, maka dapat diberikan sebanyak 3 dosis
    </p>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasi)</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan di lokasi penyuntikan, rewel, alergi berat, lumpuh layu, dll
      </li>
      <li>
        Informasi Tambahan
      </li>
      <li>
        Paling baik diberikan dalam 12 jam setelah lahir
      </li>
      <li>
        Pemberian didahului oleh pemberian vitamin K 30 menit sebelumnya di paha yang berbeda
      </li>
      <li>
        Bayi dengan ibu penderita Hepatitis B, selain diberikan imunisasi hepatitis B juga diberikan imunoglobulin (HbIg) di paha yang berbeda
      </li>
      <li>
        Bila direncanakan pemberian imunisasi hepatitis B dengan kombinasi DTPw, maka jadwal pemberian pada usia 2,3,4 bulan. Apalbila dikombinasikan dengan DTPa, maka jadwal pemberian pada usia 2,4,6 bulan.
      </li>
      <li>
        Bila diberikan imunisasi hepatitis B monovalen (bukan kombinasi) maka jadwal pemberian adalah 0,1,6 bulan
      </li>
    </ul>

  </div>
</div>
@endsection