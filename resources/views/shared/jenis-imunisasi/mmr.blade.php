@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>MMR</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit gondongan, campak jerman, radang testis pada lelaki
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum mendapat imunisasi MMR dengan usia minimal 12 bulan
      </li>
      <li>
        Anak dengan penyakit kronis seperti kistik fibrosis, kelainan jantung bawaan, kelainan ginjal bawaan, gagal tumbuh, dan sindrom down
      </li>
      <li>
        Anak berusia > 1 tahun yang berada di tempat penitipan anak atau sudah mengikuti kelompok bermain
      </li>
      <li>
        Anak yang tinggal di lembaga cacat mental
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi MMR
      </li>
      <li>
        Sedang sakit berat
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, imunodefisiensi, pengguna obat steroid jangka panjang atau imunosupresif, mendapatkan kemoterapi atau radiasi)
      </li>
      <li>
        Alergi terhadap obat neomisin atau obat yang mengandung gelatin
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <p class="ml-2">
      Pada usia sekolah dan remaja, pelu diberikan 2 dosis dengan interval 4 minggu
    </p>

    <h5 class="mt-2">KIPI (kejadian ikutan pasca imunisasai)</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Lemas
      </li>
      <li>
        Alergi
      </li>
      <li>
        Informasi tambahan
      </li>
      <li>
        Pada daerah endemis dapat diberikan usia 9 bulan
      </li>
    </ul>

  </div>
</div>
@endsection