@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Varisela</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit cacar
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum mendapat imunisasi varisela dengan usia minimal 6 bulan
      </li>
      <li>
        Terpapar dengan penderita cacar dalam 72 jam dan belum pernah mendapat imunisasi cacar
      </li>
      <li>
        Penderita keganasan/kanker
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi varisela
      </li>
      <li>
        Demam tinggi
      </li>
      <li>
        Sedang sakit berat
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, imunodefisiensi, pengguna obat steroid jangka panjang atau imunosupresif, mendapatkan kemoterapi atau radiasi)
      </li>
      <li>
        Alergi terhadap obat neomisin atau obat yang mengandung gelatin
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <p class="ml-2">
      Apabila diberikan usia >12 tahun pelu diberikan 2 dosis dengan interval 4 minggu
    </p>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasai)</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Alergi
      </li>
      <li>
        Informasi tambahan
      </li>
      <li>
        Vaksin varisela diberikan setelah usia 12 bulan, terbaik pada usia sebelum masuk sekolah dasar
      </li>
    </ul>

  </div>
</div>
@endsection