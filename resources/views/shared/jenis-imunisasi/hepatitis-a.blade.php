@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Hepatitis A</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">Mencegah infeksi hepatitis A</p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum mendapat imunisasi hepatitis A atau yang tinggal di daerah endemis dengan usia minimal 2 tahun
      </li>
      <li>
        Penderita penyakit hati kronis
      </li>
      <li>
        Pasca paparan dengan penderita hepatitis A
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi hepatitis A
      </li>
      <li>
        Avaxim: Alergi terhadap obat neomisin atau obat yang mengandung gelatin
      </li>
      <li>
        Imunisasi kejar
      </li>
      <li>
        Dapat diberikan kapan saja dengan usia minimal 24 bulan
      </li>
      <li>
        KIPI (kejadian ikutan pasca imunisasai)
      </li>
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Informasi tambahan
      </li>
      <li>
        Immunoglobulin diberikan pada: Anak <2 tahun
      </li>
      <li>
        Pencegahan setelah terpapar: tidak lebih dari 2 minggu. Jika paparan >2 minggu berikan vaksin
      </li>
    </ul>
  </div>
</div>
@endsection