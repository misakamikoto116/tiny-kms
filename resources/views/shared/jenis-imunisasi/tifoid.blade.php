@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>TIFOID</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit tifoid
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum mendapat imunisasi tifoid dengan usia minimal 24 bulan
      </li>
      <li>
        Indikasi kontra
      </li>
      <li>
        Demam, penyakit akut dan kronik progresif, riwayat alergi pada pemberian imunisasi tifoid sebelumnya
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <p class="ml-2">
      Dapat diberikan kapan saja dengan usia minimal 24 bulan, diulang tiap 3 tahun
    </p>

    <h5 class="mt-2">KIPI (kejadian ikutan pasca imunisasai)</h5>
    <ul class="ml-4">
      <li>
        Demam
      </li>
      <li>
        Nyeri kepala
      </li>
      <li>
        Pusing
      </li>
      <li>
        Nyeri sendi
      </li>
      <li>
        Nyeri otot
      </li>
      <li>
        Mual
      </li>
      <li>
        Nyeri perut
      </li>
      <li>
        Alergi
      </li>
    </ul>

    <h5 class="mt-2">Inforrmasi Tambahan</h5>
    <p class="ml-2">
      Imunisasi ulangan tiap 3 tahun
    </p>

  </div>
</div>
@endsection