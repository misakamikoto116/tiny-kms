@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Polio</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah virus polio yang dapat menyebabkan lumpuh layuh pada tungkai dan atau lengan
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua bayi baru lahir
      </li>
      <li>
        Anak yang belum pernah mendapat imunisasi polio
      </li>
      <li>
        Indikasi Kontra
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, pengguna obat steroid jangka panjang dan imunosupresif, mendapatkan kemoterapi atau radiasi, penyakit keganasan yang mengenai sumsum tulang dan limfe)
      </li>
      <li>
        Kontak atau serumah dengan penderita gangguan imunitas
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <ul class="ml-4">
      <li>
        Pada saat lahir atau bayi dipulangkan harus diberi OPV-0. Selanjutnya untuk Polio 1, 2, 3 dan booster dapat diberikan OPV/IPV.
      </li>
    </ul>

    <h5 class="mt-2">KIPI</h5>
    <ul class="ml-4">
      <li>
        Penyakit polio yang timbul akibat vaksin OPV.
      </li>
      <li>
        Informasi Tambahan
      </li>
      <li>
        Apabila bayi lahir di rumah segera diberikan, sedangakn bila di sarana kesehatan dapat diberikan saat bayi pulang.
      </li>
      <li>
        Sebaiknya paling sedikit mendapat 1 dosis IPV, yaitu bersamaan dengan pemberian OPV 3
      </li>
    </ul>
  </div>
</div>
@endsection