@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Influenza</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit radang/infeksi jaringan otak (ensefalitis) yang ditularkan oleh nyamuk culex
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum mendapat imunisasi JE dengan usia minimal 12 bulan yang tinggal atau berpergian ke daerah endemis >1 bulan
      </li>
      <li>
        Indikasi kontra
      </li>
      <li>
        Riwayat alergi berat pada pemberian imunisasi JE sebelumnya
      </li>
      <li>
        Sedang sakit berat
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, imunodefisiensi, pengguna obat steroid jangka panjang atau imunosupresif, mendapatkan kemoterapi atau radiasi)
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <ul class="ml-4">
      <li>
        Dapat diberikan kapan saja dengan usia minimal 12 bulan, dan untuk perlindungan jangka panjang dapat diberikan booster 1-2 tahun berikutnya setelah pemberian imunisasi dosis terakhir
      </li>
    </ul>

    <h5 class="mt-2">KIPI (kejadian ikutan Pasca Imunisasai)</h5>
    <p class="ml-2">
      Nyeri/bengkak/kemerahan tempat penyuntikan pemberian yaitu usia 6 bulan, diulang tiap tahun
    </p>

    <h5 class="mt-2">Informasi Tambahan</h5>
    <p class="ml-2">
      Vaksin influenza diberikan pada usia minimal 6 bulan, diulang setiap tahun. Untuk imunisasi pertama kali bila berusia <9 tahun diberikan 2 kali dengan interval 4 minggu
    </p>

  </div>
</div>
@endsection