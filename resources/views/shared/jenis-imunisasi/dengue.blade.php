@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>DENGUE</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah penyakit demam berdarah akibat nyamuk Aedes aegypti
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <p class="ml-2">
      Semua anak yang belum mendapat imunisasi dengue yang tinggal di daerah endemis
    </p>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi dengue
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, imunodefisiensi, pengguna obat steroid jangka panjang atau imunosupresif, mendapatkan kemoterapi atau radiasi)
      </li>
      <li>
        Perhatian khusus: ibu hamil
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <p class="ml-2">
      Dapat diberikan kapan saja dengan usia pemberian yaitu usia 9-16 tahun
    </p>

    <h5 class="mt-2">KIPI (kejadian ikutan pasca imunisasai)</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Nyeri otot
      </li>
      <li>
        Nyeri kepala
      </li>
      <li>
        Alergi
      </li>
    </ul>

  </div>
</div>
@endsection