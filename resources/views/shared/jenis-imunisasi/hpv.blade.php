@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>HPV</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah terjadinya penyakit kutil kelamin, kanker leher rahim, anus dan kelamin
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <p class="ml-2">
      Semua anak yang belum mendapat imunisasi HPV mulai usia 10 tahun dan belum/tidak terinfeksi HPV
    </p>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi HPV
      </li>
      <li>
        Perhatian khusus: ibu hamil
      </li>
      <li>
        Imunisasi kejar
      </li>
      <li>
        Dapat diberikan kapan saja dengan usia pemberian mulai 10 tahun
      </li>
    </ul>

    <h5 class="mt-2">KIPI (kejadian ikutan pasca imunisasai)</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Nyeri kepala
      </li>
      <li>
        Alergi
      </li>
    </ul>

    <h5 class="mt-2">Informasi Tambahan</h5>
    <ul class="ml-4">
      <li>
        Bivalen: untuk kanker serviks (buat wanita saja)
      </li>
      <li>
        Quadrivalen: untuk kutil kelamin dan kanker serviks (buat wanita dan pria)
      </li>
      <li>
        Untuk pemberian pada anak usia 10-13 tahun cukup 2 dosis (respon antibodi setara 3 dosis)
      </li>
    </ul>

  </div>
</div>
@endsection