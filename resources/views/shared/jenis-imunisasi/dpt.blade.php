@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>DPT</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      Mencegah infeksi difteri yang menyebabkan penyumbatan jalan nafas, infeksi pertusis yang menyebabkan batuk rejan (batuk 100 hari), dan infeksi tetanus yang menyebabkan kaku dan kejang otot
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin mati</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum diberikan imunisasi DTP minimal usia 6 minggu
      </li>
      <li>
        Terpapar dengan individu terinfeksi difteri dan pertusis
      </li>
      <li>
        Terdapat luka terbuka yang kotor
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian vaksin DTP
      </li>
      <li>
        Penurunan kesadaran/kejang sesudah pemberian vaksin pertusis sebelumnya
      </li>
      <li>
        Perhatian khusus
      </li>
      <li>
        Demam tinggi
      </li>
      <li>
        Kejang demam dalam 3 hari setelah imunisasi
      </li>
      <li>
        Kondisi lemas-lunglai dalam 48 jam setelah imunisasi
      </li>
      <li>
        Anak menangis terus menerus selama 3 jam setelah imunisasi
      </li>
    </ul>

    <h5 class="mt-2">Imunisasi Kejar</h5>
    <p class="ml-2">Belum pernah</p>
    <ul class="ml-4">
      <li>
        <12 bulan: imunisasi diberikan sesuai imunisasi dasar baik dosis atau interval
      </li>
      <li>
        Usia >7 tahun
      </li>
      <li>
        Td ke-1 dan 2 diberikan dengan jarak 1-2 bulan
      </li>
      <li>
        Td ke-3 diberikan dengan jarak 6-12 bulan
      </li>
    </ul>

    <h5 class="mt-2">Terlambat Imunisasi</h5>
    <ul class="ml-4">
      <li>
        Jika DTP ke-4 diberikan saat usia <4 tahun, DTP ke-5 diberikan dengan jarak 6 bulan
      </li>
      <li>
        Jika DTP ke-4 diberikan saat usia >4 tahun, tidak perlu diberikan DTP ke-5
      </li>
    </ul>

    <h5 class="mt-2">Vaksin palsu</h5>
    <ul class="ml-4">
      <li>
        Usia < 1 tahun : imunisasi diberikan 3 kali dengan interval 1 bulan
      </li>
    </ul>

    <h5 class="mt-2">Usia 1-7 tahun : DTP</h5>
    <ul class="ml-4">
      <li>
        Dosis I : hari penyuntikan
      </li>
      <li>
        Dosis II : 2 bulan setelah dosis I
      </li>
      <li>
        Dosis III : 6 bulan setelah dosisi II
      </li>
    </ul>

    <h5 class="mt-2">Usia > 7 tahun : Td, Tdap</h5>
    <ul class="ml-4">
      <li>
        Dosis I : hari penyuntikan
      </li>
      <li>
        Dosis II : 2 bulan setelah dosis I
      </li>
      <li>
        Dosis III : 6 bulan setelah dosis II
      </li>
      <li>
        Dosis IV dan V diberikan dengan interval 12 bulan dari pemberian terakhir
      </li>
    </ul>

    <h5 class="mt-2">KIPI (Kejadian Ikutan Pasca Imunisasi)</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Gelisah
      </li>
      <li>
        Vaksin yang mengandung vaksin pertusis: lemas lunglai, penurunan kesadaran, kejang, alergi berat
      </li>
    </ul>
    
    <h5 class="mt-2">Informasi Tambahan</h5>
    <ul class="ml-4">
      <li>
        Vaksin DTP dapat diberikan paling cepat pada usia 6 minggu.
      </li>
      <li>
        Dapat diberikan vaksi DTPw atau DTPa atau kombinasi dengan vaksin lain
      </li>
      <li>
        Apabila diberikan vaksin DTPa makan interval mengikuti rekomendasi vaksin tersebut yaitu usia 2,4,6 bulan
      </li>
      <li>
        Untuk anak usia >7 tahun dapat diberikan Td atau Tdap
      </li>
      <li>
        DTP6 dapat diberikan Td/Tdap pada saat anak berusia 10-12 tahun dengan booster Td setiap 10 tahun
      </li>
    </ul>

  </div>
</div>
@endsection