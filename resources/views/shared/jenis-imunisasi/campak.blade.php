@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
      <h3 class="card-title d-flex justify-content-between">
        <span>Campak</span>
        <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">Kembali</a>
      </h3>
    <h5 class="mt-2">Mencegah</h5>
    <p class="ml-2">
      mingguCAMPAKMencegah penyakit campak yang dapat mengakibatkan komplikasi radang paru, radang otak dan kebutaan
    </p>

    <h5 class="mt-2">Jenis</h5>
    <p class="ml-2">Vaksin hidup</p>

    <h5 class="mt-2">Indikasi</h5>
    <ul class="ml-4">
      <li>
        Semua anak yang belum mendapat imunisasi campak dengan usia minimal 6 bulan
      </li>
      <li>
        Terpapar dengan penderita campak dalam 72 jam dan belum pernah mendapat imunisasi campak
      </li>
    </ul>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Riwayat alergi berat pada pemberian imunisasi campak sebelumnya
      </li>
      <li>
        Sedang sakit berat
      </li>
      <li>
        Kondisi dengan gangguan imunitas (HIV, imunodefisiensi, pengguna obat steroid jangka panjang atau imunosupresif, mendapatkan kemoterapi atau radiasi)
      </li>
      <li>
        Alergi terhadap obat neomisin atau obat yang mengandung gelatin
      </li>
    </ul>

    <h5 class="mt-2">Imunisai Kejar</h5>
    <p class="ml-2">
      Pada usia sekolah dan remaja, berikan 2 kali dgn jarak pemberian minimal 4 minggu
    </p>

    <h5 class="mt-2">Indikasi Kontra</h5>
    <ul class="ml-4">
      <li>
        Nyeri/bengkak/kemerahan tempat penyuntikan
      </li>
      <li>
        Demam
      </li>
      <li>
        Alergi
      </li>
      <li>
        Penurunan kesadaran
      </li>
      <li>
        Kejang
      </li>
      <li>
        Informasi tambahan 
      </li>
      <li>
        Vaksin campak ke-2 tidak perlu diberikan pada usia 18 bulan bila MMR sudah diberikan pada usia 12 bulan
      </li>
      <li>
        Vaksin campak ke-3 tidak perlu diberikan bila MMR kedua sudah diberikan
      </li>
    </ul>

  </div>
</div>
@endsection