<form method="POST" action={{ route('profiles.post.profile') }} enctype="multipart/form-data">
  @csrf
  @method('PUT')
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="card">
    <div class="card-header">
        <strong>Profile</strong>
        <small> Edit</small>
    </div>
    <div class="card-body card-block">
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="no_ktp" class="form-control-label">No. KTP</label>
          <input name="no_ktp" type="number" id="no_ktp" placeholder="Masukkan no. ktp"
            value="{{ $data->profile->no_ktp }}"
            class="form-control @error('no_ktp') is-invalid @enderror">
          <small class="help-block form-text text-info">Ket: <span id="ket_no_ktp">{{ $data->profile->is_in_anggana_formatted }}</span></small>
          @error('no_ktp')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="no_hp" class="form-control-label">No. HP</label>
          <input name="no_hp" type="number" id="no_hp" placeholder="Masukkan no. hp"
            value="{{ $data->no_hp }}"
            class="form-control @error('no_hp') is-invalid @enderror">
          @error('no_hp')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
          <input name="name" type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
            value="{{ $data->name }}"
            class="form-control @error('name') is-invalid @enderror">
          @error('name')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="jabatan" class="form-control-label">Jabatan</label>
          <input name="jabatan" type="text" id="jabatan" placeholder="Masukkan jabatan"
            value="{{ $data->profile->jabatan }}"
            class="form-control @error('jabatan') is-invalid @enderror">
          @error('jabatan')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
          <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
            value="{{ $data->tgl_lahir }}"
            class="form-control date @error('tgl_lahir') is-invalid @enderror">
          @error('tgl_lahir')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
          <select name="jenis_kelamin" id="jenis_kelamin"
            class="form-control @error('jenis_kelamin') is-invalid @enderror">
              <option disabled value="0">Pilih jenis kelamin<option>
              <option value="LK" @if($data->jenis_kelamin === 'LK') selected @endif>Laki-laki</option>
              <option value="PR" @if($data->jenis_kelamin === 'PR') selected @endif>Perempuan</option>
          </select>
          @error('jenis_kelamin')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="kecamatan" class="form-control-label">Kecamatan</label>
          <input name="kecamatan" type="text" id="kecamatan" placeholder="Masukkan kecamatan"
            value="{{ $data->kecamatan }}"
            class="form-control @error('kecamatan') is-invalid @enderror">
          @error('kecamatan')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="kelurahan" class="form-control-label">Kelurahan/Desa</label>
          <input name="kelurahan" type="text" id="kelurahan" placeholder="Masukkan kelurahan"
            value="{{ $data->kelurahan }}"
            class="form-control @error('kelurahan') is-invalid @enderror">
          @error('kelurahan')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="rt" class="form-control-label">RT</label>
          <input name="rt" type="text" id="rt" placeholder="Masukkan rt"
            value="{{ $data->rt }}"
            class="form-control @error('rt') is-invalid @enderror">
          @error('rt')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="alamat" class="form-control-label">Alamat</label>
          <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
            value="{{ $data->alamat }}"
            class="form-control @error('alamat') is-invalid @enderror">
          @error('alamat')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="username" class="form-control-label">Username</label>
          <input name="username" type="text" id="username" placeholder="Masukkan username"
            value="{{ $data->username }}"
            class="form-control @error('username') is-invalid @enderror">
          @error('username')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="email" class="form-control-label">Email</label>
          <input name="email" type="email" id="email" placeholder="Masukkan email"
            value="{{ $data->email }}"
            class="form-control @error('email') is-invalid @enderror">
          @error('email')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col">
          <label for="old_password" class="form-control-label">Password Sekarang</label>
          <input name="old_password" type="password" id="old_password" placeholder="Masukkan password lama"
            class="form-control @error('old_password') is-invalid @enderror">
          @error('old_password')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-auto">
          <img id="foto-preview" src="{{ !is_null($data->foto) ? url('images/petugas/' . $data->foto) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
        </div>
        <div class="col">
          <label for="file-input" class=" form-control-label">Foto</label>
          <input name="foto" type="file" id="file-input" name="file-input"
            class="form-control-file @error('foto') is-invalid @enderror">
          @error('foto')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-primary">
        <i class="fa fa-send"></i> Submit
      </button>
    </div>
  </div>
</form>

@push('script-addon')
    <script>
      $('#no_ktp').on('input', e => {
        const val = e.target.value;
        const id = val.substr(0, 6);
        const kodeAnggana = '640204';
        if(id === kodeAnggana) {
            $('#ket_no_ktp').text('Warga Anggana')
        } 
        else if(id.length < 1) {
            $('#ket_no_ktp').text('-')
        }
        else {
            $('#ket_no_ktp').text('Bukan warga Anggana')
        }
      })
    </script>
@endpush