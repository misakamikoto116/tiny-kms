@extends('layouts.app')

@section('content')
  @role('Admin')
    @include('shared.profiles.profile-admin')
  @elserole('Petugas')
    @include('shared.profiles.profile-petugas')
  @elserole('Orang_Tua')
    @include('shared.profiles.profile-orangtua')
  @endrole
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })
    </script>
@endpush