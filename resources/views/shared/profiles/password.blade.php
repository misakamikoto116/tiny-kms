@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('profiles.post.password') }} enctype="multipart/form-data">
    @csrf
    @method('PUT')
    @if ($msg = Session::get('messages'))
      <div class="row">
        <div class="col col-12">
          <div class="alert alert-block alert-{{ Session::get('type') }}">
            <p>{{ $msg }}</p>
          </div>      
        </div>
      </div>
    @endif
    <div class="card">
      <div class="card-header">
          <strong>Password</strong>
          <small> Edit</small>
      </div>
      <div class="card-body card-block">
        <div class="row form-group">
          <div class="col">
            <label for="old_password" class="form-control-label">Password Lama</label>
            <input name="old_password" type="password" id="old_password" placeholder="Masukkan password lama"
              class="form-control @error('old_password') is-invalid @enderror">
            @error('old_password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col">
            <label for="password" class="form-control-label">Password Baru</label>
            <input name="password" type="password" id="password" placeholder="Masukkan password baru"
              class="form-control @error('password') is-invalid @enderror">
            @error('password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col">
            <label for="password_confirmation" class="form-control-label">Password Baru (konfirmasi)</label>
            <input name="password_confirmation" type="password" id="password_confirmation" placeholder="Masukkan konfirmasi password baru"
              class="form-control @error('password_confirmation') is-invalid @enderror">
            @error('password_confirmation')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </div>
  </form>
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })
    </script>
@endpush