<form method="POST" action={{ route('profiles.post.profile') }} enctype="multipart/form-data">
  @csrf
  @method('PUT')
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="card">
    <div class="card-header">
        <strong>Profile</strong>
        <small> Edit</small>
    </div>
    <div class="card-body card-block">
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="no_hp" class="form-control-label">No. HP</label>
          <input name="no_hp" type="number" id="no_hp" placeholder="Masukkan no. hp"
            value="{{ $data->no_hp }}"
            class="form-control @error('no_hp') is-invalid @enderror">
          @error('no_hp')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
          <input name="name" type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
            value="{{ $data->name }}"
            class="form-control @error('name') is-invalid @enderror">
          @error('name')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
          <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
            value="{{ $data->tgl_lahir }}"
            class="form-control date @error('tgl_lahir') is-invalid @enderror">
          @error('tgl_lahir')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
          <select name="jenis_kelamin" id="jenis_kelamin"
            class="form-control @error('jenis_kelamin') is-invalid @enderror">
              <option disabled value="0">Pilih jenis kelamin<option>
              <option value="LK" @if($data->jenis_kelamin === 'LK') selected @endif>Laki-laki</option>
              <option value="PR" @if($data->jenis_kelamin === 'PR') selected @endif>Perempuan</option>
          </select>
          @error('jenis_kelamin')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12">
          <label for="username" class="form-control-label">Username</label>
          <input name="username" type="text" id="username" placeholder="Masukkan username"
            value="{{ $data->username }}"
            class="form-control @error('username') is-invalid @enderror">
          @error('username')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-12 col-md-6">
          <label for="email" class="form-control-label">Email</label>
          <input name="email" type="email" id="email" placeholder="Masukkan email"
            value="{{ $data->email }}"
            class="form-control @error('email') is-invalid @enderror">
          @error('email')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="col col-12 col-md-6">
          <label for="alamat" class="form-control-label">Alamat</label>
          <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
            value="{{ $data->alamat }}"
            class="form-control @error('alamat') is-invalid @enderror">
          @error('alamat')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col">
          <label for="old_password" class="form-control-label">Password Sekarang</label>
          <input name="old_password" type="password" id="old_password" placeholder="Masukkan password lama"
            class="form-control @error('old_password') is-invalid @enderror">
          @error('old_password')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="row form-group">
        <div class="col col-auto">
          <img id="foto-preview" src="{{ !is_null($data->foto) ? url('images/admin/' . $data->foto) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
        </div>
        <div class="col">
          <label for="file-input" class=" form-control-label">Foto</label>
          <input name="foto" type="file" id="file-input" name="file-input"
            class="form-control-file @error('foto') is-invalid @enderror">
          @error('foto')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
    </div>
    <div class="card-footer text-right">
      <a href={{ url('/admin/petugas') }} type="reset" class="btn btn-danger">
        <i class="fa fa-ban"></i> Cancel
      </a>
      <button type="submit" class="btn btn-primary">
        <i class="fa fa-send"></i> Submit
      </button>
    </div>
  </div>
</form>