@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('petugas.anak.vitamin.store') }}>
    @csrf
    @method('POST')
    <div class="container-fluid">
      <div class="row m-b-25">
        <div class="col-md-12">
          <div class="overview-wrap">
            <h2 class="title-1">
              Data Vitamin A
              <small class="text-muted">{{ Session::get('anak.name') }} / {{ Session::get('anak.umur_bulan_formatted') }} / {{ Session::get('anak.jenis_kelamin_formatted') }}</small>
            </h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
                <strong>Vitamin A</strong>
                <small> Tambah</small>
            </div>
            <div class="card-body card-block">
              <div class="form-group">
                <label for="tanggal" class="form-control-label">Tanggal</label>
                <input name="tanggal" type="text" id="tanggal" placeholder="Masukkan tanggal"
                  class="form-control @error('tanggal') is-invalid @enderror"
                  value={{ old('tanggal') }}>
                @error('tanggal')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
              <div class="form-group">
                <label for="jenis" class="form-control-label">Jenis Vitamin A</label>
                <select name="jenis" id="jenis" class="form-control @error('jenis') is-invalid @enderror">
                  @php
                    $umur = Session::get('anak.umur_bulan');
                  @endphp
                  <option disabled selected value="0">Pilih jenis vitamin a</option>
                  <option value="biru"
                    @if(old('jenis') === 'biru') selected @endif
                    @if($umur < 6 || $umur > 11) disabled @endif
                  >
                    Kapsul Biru 6-11 Bulan
                    @if($umur < 6 || $umur > 11) (belum cukup umur / kelebihan umur) @endif
                  </option>
                  <option value="merah"
                    @if(old('jenis') === 'merah') selected @endif
                    @if($umur < 12 || $umur > 59) disabled @endif
                  >
                    Kapsul Merah 12-59 Bulan 
                    @if($umur < 12 || $umur > 59) (belum cukup umur / kelebihan umur) @endif
                  </option>
                </select>
                @error('jenis')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
            <div class="card-footer text-right">
              <a href={{ url('/petugas/anak/vitamin') }} type="reset" class="btn btn-danger">
                <i class="fa fa-ban"></i> Cancel
              </a>
              <button type="submit" class="btn btn-primary">
                <i class="fa fa-send"></i> Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@push('script-addon')
  <script>
    $('#tanggal').datetimepicker({
      format: 'YYYY/MM/DD',
      defaultDate: moment()
    });
  </script>
@endpush