@extends('layouts.app')

@section('content')
<form method="POST" action={{ route('petugas.anak.imunisasi.store') }}>
  @csrf
  <div class="container-fluid">
    <div class="row m-b-25">
      <div class="col-md-12">
        <div class="overview-wrap">
          <h2 class="title-1">
            Data Imunisasi
            <small class="text-muted">{{ Session::get('anak.name') }} / {{ Session::get('anak.umur_bulan_formatted') }} / {{ Session::get('anak.jenis_kelamin_formatted') }}</small>
          </h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-header">
              <strong>Imunisasi</strong>
              <small> Tambah</small>
          </div>
          <div class="card-body card-block">
            <div class="form-group">
              <label for="tanggal" class="form-control-label">Tanggal</label>
              <input name="tanggal" type="text" id="tanggal" placeholder="Masukkan tanggal"
                class="form-control @error('tanggal') is-invalid @enderror"
                value={{ old('tanggal') }}>
              @error('tanggal')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="jenis_imunisasi" class="form-control-label">Jenis Imunisasi</label>
              <input name="jenis_imunisasi" type="text" id="jenis_imunisasi" placeholder="Masukkan jenis imunisasi"
                class="form-control @error('jenis_imunisasi') is-invalid @enderror"
                value="{{ old('jenis_imunisasi') }}">
              @error('jenis_imunisasi')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
              @enderror
            </div>
          </div>
          <div class="card-footer text-right">
            <a href={{ url('/petugas/anak/imunisasi') }} type="reset" class="btn btn-danger">
              <i class="fa fa-ban"></i> Cancel
            </a>
            <button type="submit" class="btn btn-primary">
              <i class="fa fa-send"></i> Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push('script-addon')
  <script>
    $('#tanggal').datetimepicker({
      format: 'YYYY/MM/DD',
      defaultDate: moment()
    });
  </script>
@endpush