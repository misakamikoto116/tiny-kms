<h2 class="title-2 text-center m-b-10">Imunisasi Kementerian Kesehatan</h2>
<div class="table-responsive m-b-40">
  <table class="table table-borderless table-data3">
    <thead>
      <tr>
        <th>umur</th>
        <th>jenis imunisasi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="font-weight-bold">Baru lahir</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis B-0</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">1 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/bcg') }}">BCG</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-1</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">2 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-1</a><br>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis B-1</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-1</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">3 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-2</a><br>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis B-2</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">4 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-3</a><br>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis B-3</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-3</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">9 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/campak') }}">Campak-1</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">18 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/campak') }}">Campak-2</a><br>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-4</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-4</a><br>
        </td>
      </tr>
    </tbody>
  </table>
</div>