<h2 class="title-2 text-center m-b-10">Imunisasi IDAI</h2>
<div class="table-responsive m-b-40">
  <table class="table table-borderless table-data3">
    <thead>
      <tr>
        <th>umur</th>
        <th>jenis imunisasi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="font-weight-bold">Baru lahir</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis B-1</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-0</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">1 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/bcg') }}">BCG</a>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">2 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis B-2</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-1</a><br>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-1</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-1</a><br>
          <a href="{{ url('/jenis-imunisasi/pcv') }}">Pcv-1</a><br>
          <a href="{{ url('/jenis-imunisasi/rotavirus') }}">Rotavirus-1</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">3 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis-3</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-2</a><br>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-2</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">4 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hepatitis-b') }}">Hepatitis-4</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-3</a><br>
          <a href="{{ url('/jenis-imunisasi/ipv') }}">Ipv</a><br>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-3</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-3</a><br>
          <a href="{{ url('/jenis-imunisasi/pcv') }}">Pcv-2</a><br>
          <a href="{{ url('/jenis-imunisasi/rotavirus') }}">Rotavirus-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">6 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/pcv') }}">Pcv-3</a><br>
          <a href="{{ url('/jenis-imunisasi/rotavirus') }}">Rotavirus-2</a><br>
          <a href="{{ url('/jenis-imunisasi/influenza') }}">Influenza</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">9 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/campak') }}">Campak-1</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">12 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/pcv') }}">Pcv-4</a><br>
          <a href="{{ url('/jenis-imunisasi/varisela') }}">Varisela</a><br>
          <a href="{{ url('/jenis-imunisasi/japanese-encephalitis') }}">Japanese Encephalitis</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">18 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/campak') }}">Campak-2</a><br>
          <a href="{{ url('/jenis-imunisasi/polio') }}">Polio-4</a><br>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-4</a><br>
          <a href="{{ url('/jenis-imunisasi/hib') }}">Hib-4</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">24 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/japanese-encephalitis') }}">Japanese Encephalitis</a><br>
          <a href="{{ url('/jenis-imunisasi/tifoid') }}">Tifoid-1</a><br>
          <a href="{{ url('/jenis-imunisasi/hepatitis-a') }}">Hepatitis A-1</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">30 bulan</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hepatitis-a') }}">Hepatitis A-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">5 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-5</a><br>
          <a href="{{ url('/jenis-imunisasi/mmr') }}">Mmr-2</a><br>
          <a href="{{ url('/jenis-imunisasi/tifoid') }}">Tifoid-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">6 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/campak') }}">Campak-3</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">8 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/tifoid') }}">Tifoid-3</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">9 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dengue') }}">Dengue-1</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">9.5 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dengue') }}">Dengue-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">10 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dengue') }}">Dengue-3</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">10.5 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/hpv') }}">Hpv-2</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">11 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/tifoid') }}">Tifoid-4</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">14 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/tifoid') }}">Tifoid-5</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">17 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/tifoid') }}">Tifoid-6</a><br>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">18 tahun</td>
        <td>
          <a href="{{ url('/jenis-imunisasi/dpt') }}">DPT-7</a><br>
        </td>
      </tr>
    </tbody>
  </table>
</div>