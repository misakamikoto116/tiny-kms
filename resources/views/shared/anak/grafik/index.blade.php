@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row m-b-25">
        <div class="col-md-12">
            <div class="overview-wrap">
                <h2 class="title-1">
                    Grafik KMS
                    <small class="text-muted">{{ Session::get('anak.name') }} /
                        {{ Session::get('anak.umur_bulan_formatted') }} /
                        {{ Session::get('anak.jenis_kelamin_formatted') }}</small>
                </h2>
                @role('Petugas')
                <div>
                    <a class="au-btn au-btn-icon au-btn--blue2 mr-2" href={{ url('/petugas/anak') }}>
                        <i class="zmdi zmdi-arrow-left"></i>
                        Kembali
                    </a>
                </div>
                @endrole
            </div>
        </div>
    </div>
    @if ($msg = Session::get('messages'))
    <div class="row">
        <div class="col col-12">
            <div class="alert alert-block alert-{{ Session::get('type') }}">
                <p>{{ $msg }}</p>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
      <div class="col col-12">
        <div class="au-card m-b-30">
            <div class="au-card-inner">
                <canvas id="sales-chart"></canvas>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card p-4">
                <div class="row">
                  <div class="col">
                    <h4>Warna Gizi</h4>
                  </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-xs-6 col-lg-3 d-flex flex-row align-items-center mb-2 mb-md-0">
                        <span class="d-block rounded mr-3"
                            style="width: 50px; height: 50px; background-color: #00ff19"></span>
                        <p>Normal</p>
                    </div>
                    <div class="col col-xs-6 col-lg-3 d-flex flex-row align-items-center mb-2 mb-md-0">
                        <span class="d-block rounded mr-3"
                            style="width: 50px; height: 50px; background-color: #ffa550"></span>
                        <p>Baik / Kurang Baik</p>
                    </div>
                    <div class="col col-xs-6 col-lg-3 d-flex flex-row align-items-center mb-2 mb-md-0">
                        <span class="d-block rounded mr-3"
                            style="width: 50px; height: 50px; background-color: #fff600"></span>
                        <p>Kelebihan / Kekurangan</p>
                    </div>
                    <div class="col col-xs-6 col-lg-3 d-flex flex-row align-items-center mb-2 mb-md-0">
                        <span class="d-block rounded mr-3"
                            style="width: 50px; height: 50px; background-color: #f73333"></span>
                        <p>Terlalu Kelebihan / Buruk</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @forelse ($cards as $cd)
        <div class="col col-12 col-md-6">
            <div class="card p-4">
                <div class="row">
                    <div class="col">
                        <h5>
                            Bulan ke-{{ $cd['umur_bulan'] }}&nbsp;&nbsp;
                            <small class="text-muted font-weight-normal">{{ $cd['tanggal'] }}</small>
                        </h5>
                        <p>
                            Berat {{ $cd['berat_badan'] }}kg&nbsp;
                            <small class="text-muted">
                              (kbm: {{ $cd['kbm'] }}gr)&nbsp;
                              ({{ $cd['persentage'] }}%)
                            </small>
                        </p>
                        <p>
                            <span class="{{ $cd['keterangan'] == 'Naik' ? '' : 'line-through' }}">Naik</span>
                            <span class="mx-1">/</span>
                            <span class="{{ $cd['keterangan'] == 'Turun' ? '' : 'line-through' }}">Turun</span>
                        </p>
                        <p>
                            Asi eksklusif:&nbsp;
                            <span class="font-weight-bold">
                                {{ $cd['asi_eksklusif'] ? 'Ya' : 'Tidak' }}
                            </span>
                        </p>
                    </div>
                    <div class="col col-auto">
                        @php
                          $newColor;
                          $status;
                          switch ($cd['color']) {
                            case 'green':
                              $newColor = '#00ff19';
                              $status = 'Gizi normal';
                              break;
                            case 'orange':
                              $newColor = '#ffa550';
                              $status = 'Gizi';
                              if($cd['position'] == 'top') {
                                $status .= ' baik';
                              } else {
                                $status .= ' kurang baik';
                              }
                              break;
                            case 'yellow':
                              $newColor = '#fff600';
                              $status = 'Gizi';
                              if($cd['position'] == 'top') {
                                $status .= ' kelebihan';
                              } else {
                                $status .= ' kekurangan';
                              }
                              break;
                            case 'red':
                              $newColor = '#f73333';
                              $status = 'Gizi';
                              if($cd['position'] == 'top') {
                                $status .= ' terlalu kelebihan';
                              } else {
                                $status .= ' buruk';
                              }
                            break;
                            default:
                              $newColor = '#ffffff';
                              $status = 'Undefined';
                            break;
                        }
                        @endphp
                        <span class="d-block rounded"
                            style="width: 50px; height: 50px; background-color: {{ $newColor }}" data-toggle="tooltip"
                            data-placement="left" title="{{ $status }}"></span>
                    </div>
                </div>
            </div>
        </div>
        @empty
          <div class="col">
            <h3 class="text-center pt-4">Tidak ada data.</h3>
          </div>
        @endforelse
    </div>
</div>
@endsection

@push('script-addon')
    <script>
      var ctx = document.getElementById("sales-chart");
      var labels = {!! json_encode($months) !!};
      var data = {!! json_encode($weights) !!};
      var colors = {!! json_encode($colors) !!};
      colors = colors.map(color => {
        switch (color) {
          case 'green':
            return '#00ff19';
            break;
          case 'orange':
            return '#ffa550';
            break;
          case 'yellow':
            return '#fff600';
            break;
          case 'red':
            return '#f73333';
          break;
          default:
            return '#000000';
          break;
        }
      })
      if (ctx) {
        ctx.height = 100;
        var myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: labels,
            type: 'line',
            defaultFontFamily: 'Poppins',
            datasets: [{
              label: "Berat badan: ",
              data: data,
              backgroundColor: 'transparent',
              borderColor: 'rgba(0, 0, 0, 0.5)',
              borderWidth: 0.5,
              pointStyle: 'circle',
              pointRadius: 5,
              pointBorderColor: 'black',
              pointBackgroundColor: colors,
            }]
          },
          options: {
            responsive: true,
            tooltips: {
              mode: 'index',
              titleFontSize: 12,
              titleFontColor: '#000',
              bodyFontColor: '#000',
              backgroundColor: '#e5e5e5',
              titleFontFamily: 'Poppins',
              bodyFontFamily: 'Poppins',
              cornerRadius: 3,
              intersect: false,
              callbacks: {
                label: function(tooltipItem, data) {
                  var label = data.datasets[tooltipItem.datasetIndex].label || '';
                  label += tooltipItem.yLabel + ' kg';
                  return label;
                }
              }
            },
            legend: {
              display: false,
              labels: {
                usePointStyle: true,
                fontFamily: 'Poppins',
              },
            },
            scales: {
              xAxes: [{
                display: true,
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                scaleLabel: {
                  display: false,
                  labelString: 'Month'
                },
                ticks: {
                  fontFamily: "Poppins",
                  callback: function(value, index, values) {
                    return 'Bulan ke-' + value;
                  }
                }
              }],
              yAxes: [{
                display: true,
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Berat badan',
                  fontFamily: "Poppins"

                },
                ticks: {
                  suggestedMin: 0,
                  fontFamily: "Poppins",
                  callback: function(value, index, values) {
                    return value + ' kg';
                  }
                }
              }]
            },
            title: {
              display: false,
              text: 'Normal Legend'
            }
          }
        });
      }
    </script>
@endpush