@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row m-b-25">
      <div class="col-md-12">
        <div class="overview-wrap">
          <h2 class="title-1">
            Data Penimbangan
            <small class="text-muted">{{ Session::get('anak.name') }} / {{ Session::get('anak.umur_bulan_formatted') }} / {{ Session::get('anak.jenis_kelamin_formatted') }}</small>
          </h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col col-lg-6 col-12">
        <div class="card">
          <div class="card-header">
              <strong>Penimbangan</strong>
              <small> Bulan lalu</small>
          </div>
          <div class="card-body card-block">
            <div class="form-group">
              <label for="tanggal" class="form-control-label">Tanggal</label>
              <input type="text" id="tanggal" placeholder="Tanggal"
                class="form-control date" readonly
                value={{ $last_month_penimbangan->tanggal ?? '' }}>
            </div>
            <div class="form-group">
              <label for="berat_badan" class="form-control-label">Berat badan</label>
              <div class="input-group">
                <input type="number" step="0.01" id="berat_badan" placeholder="Berat badan"
                  class="form-control" readonly
                  value={{ $last_month_penimbangan->berat_badan ?? '' }}>
                <div class="input-group-append">
                  <span class="input-group-text">kg</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="keterangan" class="form-control-label">Keterangan</label>
              <input type="text" id="keterangan" placeholder="Keterangan"
                class="form-control" readonly
                value="{{ $last_month_penimbangan->keterangan_formatted ?? '' }}">
            </div>
            <div class="form-group">
              <label for="tinggi_badan" class="form-control-label">Tinggi badan</label>
              <div class="input-group">
                <input type="number" step="0.01" id="tinggi_badan" placeholder="Tinggi badan"
                  class="form-control" readonly
                  value={{ $last_month_penimbangan->tinggi_badan ?? '' }}>
                <div class="input-group-append">
                  <span class="input-group-text">cm</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="lingkar_kepala" class="form-control-label">Lingkar kepala</label>
              <div class="input-group">
                <input type="number" step="0.01" id="lingkar_kepala" placeholder="Lingkar kepala"
                  class="form-control" readonly
                  value={{ $last_month_penimbangan->lingkar_kepala ?? '' }}>
                <div class="input-group-append">
                  <span class="input-group-text">cm</span>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col col-12">
                <label for="" class="form-control-label mr-1">Asi Eksklusif:</label>
                <strong>
                  @isset($last_month_penimbangan->asi_eksklusif)
                    {{ $last_month_penimbangan->asi_eksklusif ? 'Ya' : 'Tidak' }}
                  @endisset
                </strong>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col col-lg-6 col-12">
        <form method="POST" action={{ route('petugas.anak.penimbangan.store') }}>
          @csrf
          <div class="card">
            <div class="card-header">
                <strong>Penimbangan</strong>
                <small> Bulan ini</small>
            </div>
            <div class="card-body card-block">
              <div class="form-group">
                <label for="tanggal" class="form-control-label">Tanggal</label>
                <input name="tanggal" type="text" id="tanggal_now" placeholder="Masukkan tanggal"
                  class="form-control @error('tanggal') is-invalid @enderror"
                  value="{{ old('tanggal') }}">
                @error('tanggal')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
              <div class="form-group">
                <label for="berat_badan" class="form-control-label">Berat badan</label>
                <div class="input-group">
                  <input name="berat_badan" type="number" step="0.01" id="berat_badan" placeholder="Masukkan berat badan"
                    class="form-control @error('berat_badan') is-invalid @enderror"
                    value={{ old('berat_badan') }}>
                  <div class="input-group-append">
                    <span class="input-group-text">kg</span>
                  </div>
                  @error('berat_badan')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>
              {{-- <div class="form-group">
                <label for="keterangan" class="form-control-label">Keterangan</label>
                <input type="text" id="keterangan" class="form-control" readonly value="Naik (N) / Turun (T)">
              </div> --}}
              <div class="form-group">
                <label for="tinggi_badan" class="form-control-label">Tinggi badan</label>
                <div class="input-group">
                  <input name="tinggi_badan" type="number" step="0.01" id="tinggi_badan" placeholder="Masukkan tinggi badan"
                    class="form-control @error('tinggi_badan') is-invalid @enderror"
                    value="{{ old('tinggi_badan') }}">
                  <div class="input-group-append">
                    <span class="input-group-text">cm</span>
                  </div>
                  @error('tinggi_badan')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>
              <div class="form-group">
                <label for="lingkar_kepala" class="form-control-label">Lingkar kepala</label>
                <div class="input-group">
                  <input name="lingkar_kepala" type="number" step="0.01" id="lingkar_kepala" placeholder="Masukkan lingkar kepala"
                    class="form-control @error('lingkar_kepala') is-invalid @enderror"
                    value="{{ old('lingkar_kepala') }}">
                  <div class="input-group-append">
                    <span class="input-group-text">cm</span>
                  </div>
                  @error('lingkar_kepala')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <div class="col col-12">
                  <label class="form-control-label">Asi Eksklusif</label>
                </div>
                <div class="col col-12">
                  <div class="form-check form-check-inline">
                    <input name="asi_eksklusif" type="radio" id="asi_yes"
                      class="form-check-input @error('asi_eksklusif') is-invalid @enderror"
                      value="1"
                      @if(old('asi_eksklusif') == "1") checked @endif
                    >
                    <label class="form-check-label" for="asi_yes">Ya</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input name="asi_eksklusif" type="radio" id="asi_no"
                      class="form-check-input @error('asi_eksklusif') is-invalid @enderror"
                      value="0"
                      @if(old('asi_eksklusif') == "0") checked @endif
                    >
                    <label class="form-check-label" for="asi_no">Tidak</label>
                    @error('asi_eksklusif')
                      <div class="invalid-feedback ml-2 mt-0">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer text-right">
              <a href={{ url('/petugas/anak/penimbangan') }} type="reset" class="btn btn-danger">
                <i class="fa fa-ban"></i> Cancel
              </a>
              <button type="submit" class="btn btn-primary">
                <i class="fa fa-send"></i> Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script-addon')
  <script>
    $('#tanggal_now').datetimepicker({
      format: 'YYYY/MM/DD',
      defaultDate: moment()
    });
  </script>
@endpush