@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">
          Data Penimbangan
          <small class="text-muted">{{ Session::get('anak.name') }} / {{ Session::get('anak.umur_bulan_formatted') }} / {{ Session::get('anak.jenis_kelamin_formatted') }}</small>
        </h2>
        @role('Petugas')
          <div>
            <a
              class="au-btn au-btn-icon au-btn--blue2 mr-2"
              href={{ url('/petugas/anak') }}
            >
              <i class="zmdi zmdi-arrow-left"></i>
              Kembali
            </a>
            <a
              class="au-btn au-btn-icon au-btn--blue"
              href={{ url('/petugas/anak/penimbangan/create') }}
            >
              <i class="zmdi zmdi-plus"></i>
              Tambah penimbangan
            </a>
          </div>
        @endrole
      </div>
    </div>
  </div>
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col col-12">
      <div class="table-responsive">
        <table class="table table-borderless table-data3">
          <thead>
            <tr>
              <th>tanggal</th>
              <th>umur</th>
              <th>berat badan</th>
              <th>tinggi badan</th>
              <th>lingkar kepala</th>
              <th>keterangan</th>
              <th>petugas</th>
              <th>posyandu</th>
              @role('Petugas')
                <th>option</th>
              @endrole
            </tr>
          </thead>
          <tbody>
            @forelse ($datas as $dt)
              <tr>
                <td>{{ $dt->tanggal_formatted }}</td>
                <td>{{ $dt->umur_bulan_formatted }}</td>
                <td>{{ $dt->berat_badan }} kg</td>
                <td>{{ $dt->tinggi_badan }} cm</td>
                <td>{{ $dt->lingkar_kepala }} cm</td>
                <td>{{ $dt->keterangan_formatted }}</td>
                <td>{{ $dt->petugas->user->name }}</td>
                <td>{{ $dt->posyandu->name }}</td>
                @role('Petugas')
                  @if ($dt->posyandu->id == Auth::user()->petugas->posyandu->id)
                    <td>
                      <a class="btn btn-sm btn-info d-inline-block" href={{ url('/petugas/anak/penimbangan/'. $dt->id .'/edit') }}>Edit</a>
                      <form class="d-inline-block" method="post" action={{ url('/petugas/anak/penimbangan/' . $dt->id) }} onSubmit="return confirm('Ingin menghapus?')">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                      </form>
                    </td>
                  @else
                    <td></td>
                  @endif
                @endrole
              </tr>
            @empty
              <tr>
                <td colspan="100%" class="text-center p-5"><h5>Tidak ada data.</h5></td>
              </tr>
            @endforelse
          </tbody>
        </table>
        <div class="d-flex justify-content-end m-t-25">
          {{ $datas->appends(request()->all())->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection