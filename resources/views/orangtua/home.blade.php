@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="overview-wrap">
                <h2 class="title-1">Dashboard</h2>
            </div>
        </div>
    </div>
    <div class="row m-t-25">
        <div class="col-sm-12 col-lg-6 mb-4 mb-lg-0">
            <div class="au-card">
                <div class="au-card-inner">
                    <h3>Selamat datang di halaman orangtua!</h3>
                    <span id="dateNow"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="overview-item overview-item--c1">
                <div class="overview__inner">
                    <div class="overview-box clearfix">
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                        <div class="text">
                            <h2>{{ $P }}</h2>
                            <span>petugas</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="overview-item overview-item--c2">
                <div class="overview__inner">
                    <div class="overview-box clearfix">
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                        <div class="text">
                            <h2>{{ $A }}</h2>
                            <span>anak</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-25">
        <div class="col">
            <div class="table-responsive">
                <table class="table table-borderless table-data3">
                  <thead>
                    <tr>
                      <th>id anak</th>
                      <th>nama</th>
                      <th class="text-center">naik (N)</th>
                      <th class="text-center">turun (T)</th>
                      <th class="text-center">baru (B)</th>
                      <th class="text-center">bulan lalu tidak nimbang (O)</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($static as $dt)
                      <tr>
                        <td>{{ $dt->generateId }}</td>
                        <td>{{ $dt->name }}</td>
                        <td class="text-center">{{ $dt->naik }}</td>
                        <td class="text-center">{{ $dt->turun }}</td>
                        <td class="text-center">{{ $dt->baru }}</td>
                        <td class="text-center">{{ $dt->tidak_timbang }}</td>  
                      </tr>
                    @empty
                      <tr>
                        <td colspan="100%" class="text-center p-5"><h5>Tidak ada data.</h5></td>
                      </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script-addon')
    <script>
        const dateNow = moment().format('dddd, DD MMMM YYYY');
        $('#dateNow').text(dateNow);
    </script>
@endpush