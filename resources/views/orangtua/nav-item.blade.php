<div class="card border-0 d-flex justify-content-center align-items-center py-4 py-md-0">
  {{-- <div class="gbr bg-secondary m-b-15" style="width: 100px; height: 100px;"></div> --}}
  <img class="m-b-15 rounded" style="width: 150px; height: 150px;" src="{{ !is_null(Session::get('anak.foto')) ? url('images/anak/' . Session::get('anak.foto')) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
  <h2 class="title-5">{{ Session::get('anak.name') ?? 'Belum pilih'}}</h2>
  <a href="{{ url('/orangtua/pilih-anak') }}">Pilih anak</a>
</div>
<li class="{{ request()->segment(2) == 'home' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/home') }}">
    <i class="fas fa-tachometer-alt tex-center"></i>
    Dashboard
  </a>
</li>
<li class="{{ request()->segment(2) == 'anak' && request()->segment(3) == 'data' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/anak') }}">
    <i class="fas fa-child text-center"></i>
    Data Anak
  </a>
</li>
<li class="{{ request()->segment(3) == 'penimbangan' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/anak/penimbangan') }}">
    <i class="fas fa-weight"></i>
    Data Penimbangan
  </a>
</li>
<li class="{{ request()->segment(3) == 'imunisasi' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/anak/imunisasi') }}">
    <i class="fas fa-syringe"></i>
    Data Imunisasi
  </a>
</li>
<li class="{{ request()->segment(3) == 'vitamin' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/anak/vitamin') }}">
    <i class="fas fa-pills"></i>
    Data Vitamin
  </a>
</li>
<li class="{{ request()->segment(3) == 'grafik' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/anak/grafik') }}">
    <i class="fas fa-chart-pie"></i>
    Grafik KMS
  </a>
</li>
<li class="{{ request()->segment(2) == 'jadwal-kegiatan' ? 'active' : '' }}">
  <a href="{{ url('/orangtua/jadwal-kegiatan') }}">
    <i class="fas fa-clipboard-list"></i>
    Jadwal Kegiatan
  </a>
</li>