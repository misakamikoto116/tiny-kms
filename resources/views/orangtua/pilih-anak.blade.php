@extends('auth.layouts')

@section('content')
<div class="container">
  <div class="login-wrap">
    <div class="login-content">
      <h2 class="m-b-25">Pilih anak</h2>
      <div class="list-group m-b-10">
        @foreach($anak as $id => $name)
          <form action="{{ route('orangtua.confirm.anak', $id) }}" method="post">
            @csrf
            <button class="list-group-item list-group-item-action">{{ $name }}</button>
          </form>
        @endforeach
        <a href="{{ url('/orangtua/tambah-anak') }}" class="list-group-item list-group-item-action active">
          Tambah anak
        </a>
        <a href="{{ url('/profile') }}" class="btn btn-info btn-block mt-3">
          Ke Profile
        </a>
      </div>
    </div>
  </div>
</div>
@endsection