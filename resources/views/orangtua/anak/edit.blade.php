@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('orangtua.anak.data.update', $data->id) }} enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card">
      <div class="card-header">
          <strong>Anak</strong>
          <small> Edit</small>
      </div>
      <div class="card-body card-block">
        <div class="row form-group">
          {{-- <div class="col col-12 col-md-6">
            <label for="identitas_anak" class="form-control-label">Identitas Anak</label>
            <input name="identitas_anak" value="{{ old('identitas_anak') }}"
              type="text" id="identitas_anak" placeholder="Masukkan identitas anak"
              class="form-control @error('identitas_anak') is-invalid @enderror">
            @error('identitas_anak')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div> --}}
          <div class="col col-12 col-md-6">
            <label for="nik" class="form-control-label">NIK Anak</label>
            <input name="nik" type="number" id="nik" placeholder="Masukkan NIK"
              value="{{ $data->nik }}"
              class="form-control @error('nik') is-invalid @enderror">
            <small class="help-block form-text text-info">Ket: <span id="ket_nik">{{ $data->is_in_anggana_formatted }}</span></small>
            @error('nik')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
            <input name="name" value="{{ $data->name }}"
              type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
              class="form-control @error('name') is-invalid @enderror">
            @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
            <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
              value="{{ $data->tgl_lahir }}"
              class="form-control date @error('tgl_lahir') is-invalid @enderror">
            @error('tgl_lahir')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
            <select name="jenis_kelamin" id="jenis_kelamin"
              class="form-control @error('jenis_kelamin') is-invalid @enderror">
                <option disabled selected value="0">Pilih jenis kelamin</option>
                <option value="LK" @if($data->jenis_kelamin === 'LK') selected @endif>Laki-laki</option>
                <option value="PR" @if($data->jenis_kelamin === 'PR') selected @endif>Perempuan</option>
            </select>
            @error('jenis_kelamin')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="kecamatan" class="form-control-label">Kecamatan</label>
            <input name="kecamatan" type="text" id="kecamatan" placeholder="Masukkan kecamatan"
              value="{{ $data->kecamatan }}"
              class="form-control @error('kecamatan') is-invalid @enderror">
            @error('kecamatan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="kelurahan" class="form-control-label">Kelurahan/Desa</label>
            <input name="kelurahan" type="text" id="kelurahan" placeholder="Masukkan kelurahan"
              value="{{ $data->kelurahan }}"
              class="form-control @error('kelurahan') is-invalid @enderror">
            @error('kelurahan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="rt" class="form-control-label">RT</label>
            <input name="rt" type="text" id="rt" placeholder="Masukkan rt"
              value="{{ $data->rt }}"
              class="form-control @error('rt') is-invalid @enderror">
            @error('rt')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="alamat" class="form-control-label">Alamat</label>
            <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
              value="{{ $data->alamat }}"
              class="form-control @error('alamat') is-invalid @enderror">
            @error('alamat')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="bapak" class="form-control-label">Ayah</label>
            <input name="bapak" type="text" id="bapak" placeholder="Masukkan nama bapak"
              value="{{ $data->bapak }}"
              class="form-control @error('bapak') is-invalid @enderror">
            @error('bapak')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="ibu" class="form-control-label">Ibu</label>
            <input name="ibu" type="text" id="ibu" placeholder="Masukkan nama ibu"
              value="{{ $data->ibu }}"
              class="form-control @error('ibu') is-invalid @enderror">
            @error('ibu')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-auto">
            <img id="foto-preview" src="{{ !is_null($data->foto) ? url('images/anak/' . $data->foto) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
          </div>
          <div class="col">
            <label for="file-input" class=" form-control-label">Foto</label>
            <input name="foto" type="file" id="file-input" name="file-input"
              value="{{ old('foto') }}"
              class="form-control-file @error('foto') is-invalid @enderror">
            @error('foto')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href={{ url('/orangtua/anak') }} type="reset" class="btn btn-danger">
          <i class="fa fa-ban"></i> Cancel
        </a>
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </div>
  </form>
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })

      $('#nik').on('input', e => {
        const val = e.target.value;
        const id = val.substr(0, 6);
        const kodeAnggana = '640204';
        if(id === kodeAnggana) {
            $('#ket_nik').text('Warga Anggana')
        } 
        else if(id.length < 1) {
            $('#ket_nik').text('-')
        }
        else {
            $('#ket_nik').text('Bukan warga Anggana')
        }
      })
    </script>
@endpush