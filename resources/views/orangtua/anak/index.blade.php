@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">Data Anak</h2>
        <a
          class="au-btn au-btn-icon au-btn--blue"
          href={{ url('/orangtua/anak/data/create') }}
        >
          <i class="zmdi zmdi-plus"></i>
          Tambah anak
        </a>
      </div>
    </div>
  </div>
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    @forelse ($datas as $dt)
      <div class="col col-12 col-md-6 mb-4">
        <div class="au-card">
          <div class="au-card-inner">
            <div class="row d-flex justify-content-center justify-content-lg-start">
              <div class="col col-auto">
                <img id="foto-preview" src="{{ !is_null($dt->foto) ? url('images/anak/' . $dt->foto) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
              </div>
              <div class="col col-12 col-md-auto text-center text-lg-left">
                <h3 class="mt-3 mb-2 text-capitalize">{{ $dt->name }}</h3>
                <p>{{ $dt->jenis_kelamin_formatted }}</p>
                <p>{{ $dt->tgl_lahir_formatted }}</p>
                <p>{{ $dt->umur_bulan_formatted }}</p>
                <div class="mt-3 d-flex flex-row justify-content-center justify-content-lg-start">
                  <a href="{{ url('/orangtua/anak/data/'. $dt->id .'/edit') }}" class="btn btn-primary mr-2 mb-2">
                    Edit
                  </a>
                  <form method="post" action={{ url('/orangtua/anak/data/' . $dt->id) }} onSubmit="return confirm('Ingin menghapus?')">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @empty
        
    @endforelse
  </div>
</div>
@endsection