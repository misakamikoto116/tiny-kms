@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">Petugas</h2>
      </div>
    </div>
  </div>
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col col-12 col-md-6 m-b-25">
      <form class="form-header" method="GET" action="{{ url('/admin/petugas') }}">
        <input class="au-input au-input--sm" type="text" name="q" placeholder="Search..." value="{{ request()->get('q') }}" />
        <button class="au-btn--submit" type="submit">
            <i class="zmdi zmdi-search"></i>
        </button>
      </form>
    </div>
    <div class="col col-12 col-md-6 m-b-25 d-flex justify-content-center justify-content-md-end">
      <a
        class="au-btn au-btn-icon au-btn--blue"
        href={{ url('/admin/petugas/create') }}
      >
        <i class="zmdi zmdi-plus"></i>
        Tambah petugas
      </a>
    </div>
  </div>
  <div class="row">
    <div class="col col-12">
      <div class="table-responsive">
        <table class="table table-borderless table-data3">
          <thead>
            <tr>
              <th>no. KTP</th>
              <th>nama</th>
              <th>jabatan</th>
              <th>no. HP</th>
              <th>alamat</th>
              <th>posyandu</th>
              <th>option</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($datas as $dt)
              <tr>
                <td>{{ $dt->petugas->no_ktp }}</td>
                <td>{{ $dt->name }}</td>
                <td>{{ $dt->petugas->jabatan }}</td>
                <td>{{ $dt->no_hp }}</td>
                <td>{{ $dt->alamat }}</td>
                <td>{{ optional($dt->petugas->posyandu)->name }}</td>
                <td>
                  <a class="btn btn-sm btn-info d-inline-block" href={{ route('admin.petugas.edit', $dt->id) }}>Edit</a>
                  <form class="d-inline-block" method="post" action={{ url('/admin/petugas/' . $dt->id) }} onSubmit="return confirm('Ingin menghapus?')">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="100%" class="text-center p-5"><h5>Tidak ada data.</h5></td>
              </tr>
            @endforelse
          </tbody>
        </table>
        <div class="d-flex justify-content-end m-t-25">
          {{ $datas->appends(request()->all())->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection