@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('admin.petugas.store') }} enctype="multipart/form-data">
    @csrf
    @method('POST')
    <div class="card">
      <div class="card-header">
          <strong>Petugas</strong>
          <small> Tambah</small>
      </div>
      <div class="card-body card-block">
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="no_ktp" class="form-control-label">No. KTP</label>
            <input name="no_ktp" type="number" id="no_ktp" placeholder="Masukkan no. ktp"
              value="{{ old('no_ktp') }}"
              class="form-control @error('no_ktp') is-invalid @enderror">
            <small class="help-block form-text text-info">Ket: <span id="ket_no_ktp">-</span></small>
            @error('no_ktp')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="no_hp" class="form-control-label">No. HP</label>
            <input name="no_hp" type="number" id="no_hp" placeholder="Masukkan no. hp"
              value="{{ old('no_hp') }}"
              class="form-control @error('no_hp') is-invalid @enderror">
            @error('no_hp')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
            <input name="name" type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
              value="{{ old('name') }}"
              class="form-control @error('name') is-invalid @enderror">
            @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="jabatan" class="form-control-label">Jabatan</label>
            <input name="jabatan" type="text" id="jabatan" placeholder="Masukkan jabatan"
              value="{{ old('jabatan') }}"
              class="form-control @error('jabatan') is-invalid @enderror">
            @error('jabatan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
            <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
              value="{{ old('tgl_lahir') }}"
              class="form-control date @error('tgl_lahir') is-invalid @enderror">
            @error('tgl_lahir')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
            <select name="jenis_kelamin" id="jenis_kelamin"
              class="form-control @error('jenis_kelamin') is-invalid @enderror">
                <option disabled selected value="0">Pilih jenis kelamin</option>
                <option value="LK" @if(old('jenis_kelamin') === 'LK') selected @endif>Laki-laki</option>
                <option value="PR" @if(old('jenis_kelamin') === 'PR') selected @endif>Perempuan</option>
            </select>
            @error('jenis_kelamin')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="username" class="form-control-label">Username</label>
            <input name="username" type="text" id="username" placeholder="Masukkan username"
              value="{{ old('username') }}"
              class="form-control @error('username') is-invalid @enderror">
            @error('username')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="password" class="form-control-label">Password</label>
            <input name="password" type="text" id="password" placeholder="Masukkan password"
              value="{{ old('password') }}"
              class="form-control @error('password') is-invalid @enderror">
            @error('password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="email" class="form-control-label">Email</label>
            <input name="email" type="email" id="email" placeholder="Masukkan email"
              value="{{ old('email') }}"
              class="form-control @error('email') is-invalid @enderror">
            @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="posyandu" class="form-control-label">Posyandu</label>
            <select name="posyandu_id" id="posyandu"
              class="form-control @error('posyandu_id') is-invalid @enderror">
                <option disabled selected value="0">Pilih posyandu</option>
                @foreach ($posyandu as $p)
                  <option value="{{ $p->id }}" @if(old('posyandu_id') == $p->id) selected @endif>{{ $p->name }}</option>
                @endforeach
            </select>
            @error('posyandu')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="kecamatan" class="form-control-label">Kecamatan</label>
            <input name="kecamatan" type="text" id="kecamatan" placeholder="Masukkan kecamatan"
              value="{{ old('kecamatan') }}"
              class="form-control @error('kecamatan') is-invalid @enderror">
            @error('kecamatan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="kelurahan" class="form-control-label">Kelurahan/Desa</label>
            <input name="kelurahan" type="text" id="kelurahan" placeholder="Masukkan kelurahan"
              value="{{ old('kelurahan') }}"
              class="form-control @error('kelurahan') is-invalid @enderror">
            @error('kelurahan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="rt" class="form-control-label">RT</label>
            <input name="rt" type="text" id="rt" placeholder="Masukkan rt"
              value="{{ old('rt') }}"
              class="form-control @error('rt') is-invalid @enderror">
            @error('rt')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="alamat" class="form-control-label">Alamat</label>
            <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
              value="{{ old('alamat') }}"
              class="form-control @error('alamat') is-invalid @enderror">
            @error('alamat')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-auto">
            <img id="foto-preview" src="{{ asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
          </div>
          <div class="col">
            <label for="file-input" class=" form-control-label">Foto</label>
            <input name="foto" type="file" id="file-input" name="file-input"
              value="{{ old('foto') }}"
              class="form-control-file @error('foto') is-invalid @enderror">
            @error('foto')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href={{ url('/admin/petugas') }} type="reset" class="btn btn-danger">
          <i class="fa fa-ban"></i> Cancel
        </a>
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </div>
  </form>
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })

      $('#no_ktp').on('input', e => {
        const val = e.target.value;
        const id = val.substr(0, 6);
        const kodeAnggana = '640204';
        if(id === kodeAnggana) {
            $('#ket_no_ktp').text('Warga Anggana')
        } 
        else if(id.length < 1) {
            $('#ket_no_ktp').text('-')
        }
        else {
            $('#ket_no_ktp').text('Bukan warga Anggana')
        }
      })
    </script>
@endpush