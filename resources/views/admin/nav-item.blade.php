<li class="{{ request()->segment(2) == 'home' ? 'active' : '' }}">
  <a href="{{ url('/admin/home') }}">
    <i class="fas fa-tachometer-alt"></i>
    Dashboard
  </a>
</li>
<li class="{{ request()->segment(2) == 'posyandu' ? 'active' : '' }}">
  <a href="{{ url('/admin/posyandu') }}">
    <i class="fas fa-hospital"></i>
    Data Posyandu
  </a>
</li>
<li class="{{ request()->segment(2) == 'petugas' ? 'active' : '' }}">
  <a href="{{ url('/admin/petugas') }}">
    <i class="fas fa-address-book"></i>
    Data Petugas
  </a>
</li>
<li class="{{ request()->segment(2) == 'orangtua' ? 'active' : '' }}">
  <a href="{{ url('/admin/orangtua') }}">
    <i class="fas fa-address-book"></i>
    Data Orangtua
  </a>
</li>
<li class="{{ request()->segment(2) == 'posyandu-rekap' ? 'active' : '' }}">
  <a href="{{ url('/admin/posyandu-rekap') }}">
    <i class="fas fa-clipboard-list"></i>
    Rekap Posyandu
  </a>
</li>