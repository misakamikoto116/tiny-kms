@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('admin.orangtua.store') }} enctype="multipart/form-data">
    @csrf
    @method('POST')
    <div class="card">
      <div class="card-header">
          <strong>Orangtua</strong>
          <small> Tambah</small>
      </div>
      <div class="card-body card-block">
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="no_kk" class="form-control-label">No. KK</label>
            <input name="no_kk" type="number" id="no_kk" placeholder="Masukkan no kartu keluarga"
              value="{{ old('no_kk') }}"
              class="form-control @error('no_kk') is-invalid @enderror">
            @error('no_kk')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="nik" class="form-control-label">NIK</label>
            <input name="nik" type="number" id="nik" placeholder="Masukkan NIK"
              value="{{ old('nik') }}"
              class="form-control @error('nik') is-invalid @enderror">
            @error('nik')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
            <input name="name" type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
              value="{{ old('name') }}"
              class="form-control @error('name') is-invalid @enderror">
            @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
            <select name="jenis_kelamin" id="jenis_kelamin"
              class="form-control @error('jenis_kelamin') is-invalid @enderror">
                <option disabled selected value="0">Pilih jenis kelamin</option>
                <option value="LK" @if(old('jenis_kelamin') === 'LK') selected @endif>Laki-laki</option>
                <option value="PR" @if(old('jenis_kelamin') === 'PR') selected @endif>Perempuan</option>
            </select>
            @error('jenis_kelamin')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
            <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
              value="{{ old('tgl_lahir') }}"
              class="form-control date @error('tgl_lahir') is-invalid @enderror">
            @error('tgl_lahir')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="no_hp" class="form-control-label">No. HP</label>
            <input name="no_hp" type="number" id="no_hp" placeholder="Masukkan no. hp"
              value="{{ old('no_hp') }}"
              class="form-control @error('no_hp') is-invalid @enderror">
            @error('no_hp')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="alamat" class="form-control-label">Alamat</label>
            <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
              value="{{ old('alamat') }}"
              class="form-control @error('alamat') is-invalid @enderror">
            @error('alamat')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="email" class="form-control-label">Email</label>
            <input name="email" type="email" id="email" placeholder="Masukkan email"
              value="{{ old('email') }}"
              class="form-control @error('email') is-invalid @enderror">
            @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="username" class="form-control-label">Username</label>
            <input name="username" type="text" id="username" placeholder="Masukkan username"
              value="{{ old('username') }}"
              class="form-control @error('username') is-invalid @enderror">
            @error('username')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="password" class="form-control-label">Password</label>
            <input name="password" type="text" id="password" placeholder="Masukkan password"
              value="{{ old('password') }}"
              class="form-control @error('password') is-invalid @enderror">
            @error('password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-auto">
            <img id="foto-preview" src="{{ asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
          </div>
          <div class="col">
            <label for="file-input" class=" form-control-label">Foto</label>
            <input name="foto" type="file" id="file-input" name="file-input"
              value="{{ old('foto') }}"
              class="form-control-file @error('foto') is-invalid @enderror">
            @error('foto')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href={{ url('/admin/orangtua') }} type="reset" class="btn btn-danger">
          <i class="fa fa-ban"></i> Cancel
        </a>
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </div>
  </form>
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })
    </script>
@endpush