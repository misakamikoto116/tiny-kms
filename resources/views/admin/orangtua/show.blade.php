@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('admin.orangtua.update', $data->id) }} enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card">
      <div class="card-header">
          <strong>Orangtua</strong>
          <small> Detail</small>
      </div>
      <div class="card-body card-block">
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="no_kk" class="form-control-label">No. KK</label>
            <input name="no_kk" type="number" id="no_kk" placeholder="Masukkan no kartu keluarga"
              value="{{ $data->orang_tua->no_kk }}"
              class="form-control @error('no_kk') is-invalid @enderror"
              readonly>
            @error('no_kk')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="nik" class="form-control-label">NIK</label>
            <input name="nik" type="number" id="nik" placeholder="Masukkan NIK"
              value="{{ $data->orang_tua->nik }}"
              class="form-control @error('nik') is-invalid @enderror"
              readonly>
            @error('nik')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
            <input name="name" type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
              value="{{ $data->name }}"
              class="form-control @error('name') is-invalid @enderror"
              readonly>
            @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
            <select name="jenis_kelamin" id="jenis_kelamin"
              class="form-control @error('jenis_kelamin') is-invalid @enderror"
              disabled>
                <option disabled selected value="0">Pilih jenis kelamin</option>
                <option value="LK" @if($data->jenis_kelamin === 'LK') selected @endif>Laki-laki</option>
                <option value="PR" @if($data->jenis_kelamin === 'PR') selected @endif>Perempuan</option>
            </select>
            @error('jenis_kelamin')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
            <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
              value="{{ $data->tgl_lahir }}"
              class="form-control date @error('tgl_lahir') is-invalid @enderror"
              readonly>
            @error('tgl_lahir')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="no_hp" class="form-control-label">No. HP</label>
            <input name="no_hp" type="number" id="no_hp" placeholder="Masukkan no. hp"
              value="{{ $data->no_hp }}"
              class="form-control @error('no_hp') is-invalid @enderror"
              readonly>
            @error('no_hp')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="username" class="form-control-label">Username</label>
            <input name="username" type="text" id="username" placeholder="Masukkan username"
              value="{{ $data->username }}"
              class="form-control @error('username') is-invalid @enderror"
              readonly>
            @error('username')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="email" class="form-control-label">Email</label>
            <input name="email" type="email" id="email" placeholder="Masukkan email"
              value="{{ $data->email }}"
              class="form-control @error('email') is-invalid @enderror"
              readonly>
            @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="kecamatan" class="form-control-label">Kecamatan</label>
            <input name="kecamatan" type="text" id="kecamatan" placeholder="Masukkan kecamatan"
              value="{{ $data->kecamatan }}"
              class="form-control @error('kecamatan') is-invalid @enderror"
              readonly>
            @error('kecamatan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="kelurahan" class="form-control-label">Kelurahan/Desa</label>
            <input name="kelurahan" type="text" id="kelurahan" placeholder="Masukkan kelurahan"
              value="{{ $data->kelurahan }}"
              class="form-control @error('kelurahan') is-invalid @enderror"
              readonly>
            @error('kelurahan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="rt" class="form-control-label">RT</label>
            <input name="rt" type="text" id="rt" placeholder="Masukkan rt"
              value="{{ $data->rt }}"
              class="form-control @error('rt') is-invalid @enderror"
              readonly>
            @error('rt')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="alamat" class="form-control-label">Alamat</label>
            <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
              value="{{ $data->alamat }}"
              class="form-control @error('alamat') is-invalid @enderror"
              readonly>
            @error('alamat')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-auto">
            <img id="foto-preview" src="{{ !is_null($data->foto) ? url('images/orang_tua/' . $data->foto) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href={{ url('/admin/orangtua') }} type="reset" class="btn btn-danger">
          <i class="fa fa-ban"></i> Back
        </a>
      </div>
    </div>
  </form>
@endsection

@push('script-addon')
    <script>
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#foto-preview').attr('src', e.target.result);
      }
        
      function readURL(input) {
        if (input.files && input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

      $('#file-input').change(function () {
        readURL(this)
      })
    </script>
@endpush