@extends('layouts.app')

@section('content')
  <form method="POST" action={{ route('admin.posyandu.store') }}>
    @csrf
    @method('POST')
    <div class="card">
      <div class="card-header">
          <strong>Posyandu</strong>
          <small> Tambah</small>
      </div>
      <div class="card-body card-block">
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="kode_posyandu" class="form-control-label">Kode Posyandu</label>
            <input name="code" value="{{ $kode_posyandu }}" disabled
              type="text" id="kode_posyandu" placeholder="Masukkan kode posyandu"
              class="form-control @error('code') is-invalid @enderror">
            @error('code')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="nama_posyandu" class="form-control-label">Nama Posyandu</label>
            <input name="name" value="{{ old('name') }}"
              type="text" id="nama_posyandu" placeholder="Masukkan nama posyandu"
              class="form-control @error('name') is-invalid @enderror">
            @error('name')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="ketua_posyandu" class="form-control-label">Ketua Posyandu</label>
            <input name="ketua" value="{{ old('ketua') }}"
              type="text" id="ketua_posyandu" placeholder="Masukkan ketua posyandu"
              class="form-control @error('ketua') is-invalid @enderror">
            @error('ketua')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="jumlah_kader" class="form-control-label">Jumlah Kader</label>
            <input name="jumlah_kader" value="{{ old('jumlah_kader') }}"
              type="number" id="jumlah_kader" placeholder="Masukkan jumlah kader"
              class="form-control @error('jumlah_kader') is-invalid @enderror">
            @error('jumlah_kader')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="kecamatan" class="form-control-label">Kecamatan</label>
            <input type="text" name="kecamatan" value="Anggana"
              type="text"
              id="kecamatan"
              class="form-control"
              readonly>
            @error('kecamatan')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="desa" class="form-control-label">Kelurahan/Desa</label>
            <select name="desa" id="desa"
              class="form-control @error('desa') is-invalid @enderror">
                <option disabled selected value="0">Pilih kelurahan/desa</option>
                @foreach ($kelurahan_choices as $kelurahan)
                  <option value="{{ $kelurahan }}" @if(old('desa') == $kelurahan) selected @endif>{{ $kelurahan }}</option>
                @endforeach
            </select>
            @error('desa')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
        <div class="row form-group">
          <div class="col col-12 col-md-6">
            <label for="alamat" class="form-control-label">Alamat</label>
            <input name="alamat" type="text" value="{{ old('alamat') }}"
              id="alamat" placeholder="Masukkan alamat"
              class="form-control @error('alamat') is-invalid @enderror">
            @error('alamat')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="col col-12 col-md-6">
            <label for="rt" class="form-control-label">RT</label>
            <input name="rt" type="text" value="{{ old('rt') }}"
              id="rt" placeholder="Masukkan rt"
              class="form-control @error('rt') is-invalid @enderror">
            @error('rt')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
            @enderror
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href={{ url('/admin/posyandu') }} type="reset" class="btn btn-danger">
          <i class="fa fa-ban"></i> Cancel
        </a>
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </div>
  </form>
@endsection