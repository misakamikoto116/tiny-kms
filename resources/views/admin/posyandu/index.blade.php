@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">Posyandu</h2>
      </div>
    </div>
  </div>
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col col-12 col-md-6 m-b-25">
      <form class="form-header" method="GET" action="{{ url('/admin/posyandu') }}">
        <input class="au-input au-input--sm" type="text" name="q" placeholder="Search..." value="{{ request()->get('q') }}" />
        <button class="au-btn--submit" type="submit">
            <i class="zmdi zmdi-search"></i>
        </button>
      </form>
    </div>
    <div class="col col-12 col-md-6 m-b-25 d-flex justify-content-center justify-content-md-end">
      <a
          class="au-btn au-btn-icon au-btn--blue"
          href={{ url('/admin/posyandu/create') }}
        >
          <i class="zmdi zmdi-plus"></i>
          Tambah posyandu
      </a>
    </div>
  </div>
  <div class="row">
    <div class="col col-12">
      <div class="table-responsive">
        <table class="table table-borderless table-data3">
          <thead>
            <tr>
              <th>kode</th>
              <th>nama</th>
              <th>desa</th>
              <th>jumlah kader</th>
              <th>ketua</th>
              <th>option</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($datas as $dt)
              <tr>
                <td>{{ $dt->code }}</td>
                <td>{{ $dt->name }}</td>
                <td>{{ $dt->desa }}</td>
                <td>{{ $dt->jumlah_kader }}</td>
                <td>{{ $dt->ketua }}</td>
                <td>
                  <a class="btn btn-sm btn-info d-inline-block" href={{ url('/admin/posyandu/'. $dt->id .'/edit') }}>Edit</a>
                  <form class="d-inline-block" method="post" action={{ url('/admin/posyandu/' . $dt->id) }} onSubmit="return confirm('Ingin menghapus?')">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="100%" class="text-center p-5"><h5>Tidak ada data.</h5></td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <div class="d-flex justify-content-end m-t-25">
        {{ $datas->appends(request()->all())->links() }}
      </div>
    </div>
  </div>
</div>
@endsection