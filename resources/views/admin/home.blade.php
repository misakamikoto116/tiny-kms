@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="overview-wrap">
                <h2 class="title-1">Dashboard</h2>
            </div>
        </div>
    </div>
    <div class="row m-t-25">
        <div class="col-sm-12 col-lg-6 mb-4 mb-lg-0">
            <div class="au-card">
                <div class="au-card-inner">
                    <h3>Selamat datang di halaman admin!</h3>
                    <span id="dateNow"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="overview-item overview-item--c1">
                <div class="overview__inner">
                    <div class="overview-box clearfix">
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                        <div class="text">
                            <h2>{{ $OT }}</h2>
                            <span>orangtua</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="overview-item overview-item--c2">
                <div class="overview__inner">
                    <div class="overview-box clearfix">
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                        <div class="text">
                            <h2>{{ $P }}</h2>
                            <span>petugas</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-25">
        <div class="col">
            <div class="au-card recent-report">
                <div class="au-card-inner">
                    {{-- {{ dd($static) }} --}}
                    <h3 class="title-2 mb-5">Orangtua melakukan penimbangan</h3>
                    <div class="recent-report__chart">
                        <canvas id="chart-home"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script-addon')
    <script>
        const dateNow = moment().format('dddd, DD MMMM YYYY');
        $('#dateNow').text(dateNow);

        // Chart Home
        const brandProduct = 'rgba(0,181,233,0.8)';
        const brandService = 'rgba(0,173,95,0.8)';

        var elements = 10;
        var labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var data = {{ $static }};

        var ctx = document.getElementById("chart-home");
        if (ctx) {
            ctx.height = 250;
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            label: 'Orangtua melakukan penimbangan',
                            backgroundColor: brandService,
                            borderColor: 'transparent',
                            pointHoverBackgroundColor: '#fff',
                            borderWidth: 0,
                            data: data
                        }
                    ]
                },
                options: {
                    maintainAspectRatio: true,
                    legend: {
                        display: false
                    },
                    responsive: true,
                    scales: {
                    xAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: '#f2f2f2'
                        },
                        ticks: {
                            fontFamily: "Poppins",
                            fontSize: 12
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            stepSize: 1,
                            // max: 150,
                            fontFamily: "Poppins",
                            fontSize: 12
                        },
                        gridLines: {
                            display: true,
                            color: '#f2f2f2'
                        }
                    }]
                },
                elements: {
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3
                    }
                }
            }
        });
        }
    </script>
@endpush