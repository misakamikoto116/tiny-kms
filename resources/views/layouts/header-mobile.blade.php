<header class="header-mobile d-block d-lg-none">
  <div class="header-mobile__bar">
      <div class="container-fluid">
          <div class="header-mobile-inner">
              <a class="logo" href="{{ url('/home') }}">
                <div class="row d-flex align-items-center">
                    <div class="col col-auto">
                        <img src="{{ asset('template/images/icon/puskesmas-logo.png') }}" style="width: 50px;" alt="Puskesmas" />
                    </div>
                    <div class="col">
                        <h4>Kartu Menuju Sehat</h4>
                    </div>
                </div>
              </a>
              <button class="hamburger hamburger--slider" type="button">
                  <span class="hamburger-box">
                      <span class="hamburger-inner"></span>
                  </span>
              </button>
          </div>
      </div>
  </div>
  <nav class="navbar-mobile">
      <div class="container-fluid">
          <ul class="navbar-mobile__list list-unstyled">
            @role('Admin')
                @include('admin.nav-item')
            @elserole('Petugas')
                @include('petugas.nav-item')
            @elserole('Orang_Tua')
                @include('orangtua.nav-item')
            @endrole
          </ul>
      </div>
  </nav>
</header>