@php 
    $role = strtolower(Auth::user()->getRoleNames()->first());
    $img = Auth::user()->foto;
    $imgUrl = url('images/' . $role . '/' . Auth::user()->foto);
@endphp

<header class="header-desktop">
  <div class="section__content section__content--p30">
      <div class="container-fluid">
          <div class="header-wrap d-flex justify-content-end">
              <div class="header-button justify-content-center">
                  <div class="account-wrap d-flex flex-row align-items-center">
                      @if ($role !== 'admin')
                        <span class="mr-4" data-toggle="tooltip"
                            data-placement="bottom"
                            title="{{ Auth::user()->profile->is_in_anggana_formatted }}">
                            <i class="fas fa-id-card"></i>
                        </span>
                      @endif
                      <div class="account-item clearfix js-item-menu">
                          <div class="image">
                              <img src="{{ !is_null($img) ? $imgUrl : asset('/img/placeholder.jpg') }}" alt="avatar" />
                          </div>
                          <div class="content">
                              <a class="js-acc-btn d-flex flex-row" href="#">
                                  <div>
                                    {{ Auth::user()->name ?? 'Nama Lengkap' }}
                                    @auth
                                        <small class="text-muted text-lowercase"> 
                                            {{ str_replace('_', ' ', Auth::user()->getRoleNames()->first()) }}
                                        </small>
                                    @endauth
                                  </div>
                              </a>
                          </div>
                          <div class="account-dropdown js-dropdown">
                              <div class="info clearfix">
                                  <div class="image">
                                      <a href="#">
                                        <img src="{{ !is_null($img) ? $imgUrl : asset('/img/placeholder.jpg') }}" alt="avatar" />
                                      </a>
                                  </div>
                                  <div class="content">
                                      <h5 class="name">
                                        {{ Auth::user()->name ?? 'Nama Lengkap' }}
                                      </h5>
                                      <span class="email">
                                        {{ Auth::user()->email ?? 'Email' }}
                                      </span>
                                  </div>
                              </div>
                              <div class="account-dropdown__body">
                                  <div class="account-dropdown__item">
                                      <a href="{{ url('/profile') }}">
                                          <i class="zmdi zmdi-account"></i>Profile</a>
                                  </div>
                                  <div class="account-dropdown__item">
                                    <a href="{{ url('/password') }}">
                                        <i class="zmdi zmdi-account"></i>Ganti Password</a>
                                  </div>
                              </div>
                              <div class="account-dropdown__footer">
                                  {{-- <a href="#">
                                      <i class="zmdi zmdi-power"></i>Logout</a> --}}
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="zmdi zmdi-power"></i>Logout
                                    </a>
                                    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</header>