<aside class="menu-sidebar d-none d-lg-block">
  <div class="logo">
		<a href="{{ url('/home') }}">
			<div class="row d-flex align-items-center">
				<div class="col col-auto">
					<img src="{{ asset('template/images/icon/puskesmas-logo.png') }}" style="width: 50px;" alt="Puskesmas" />
				</div>
				<div class="col">
					<h4>Kartu Menuju Sehat</h4>
				</div>
			</div>
		</a>
  </div>
  <div class="menu-sidebar__content js-scrollbar1">
		<nav class="navbar-sidebar">
			<ul class="list-unstyled navbar__list">
				@role('Admin')
					@include('admin.nav-item')
				@elserole('Petugas')
					@include('petugas.nav-item')
				@elserole('Orang_Tua')
					@include('orangtua.nav-item')
				@endrole
			</ul>
		</nav>
  </div>
</aside>