<li class="{{ request()->segment(2) == 'home' ? 'active' : '' }}">
  <a href="{{ url('/petugas/home') }}">
    <i class="fas fa-tachometer-alt"></i>
    Dashboard
  </a>
</li>
<li class="{{ request()->segment(2) == 'cari-anak' ? 'active' : '' }}">
  <a href="{{ url('/petugas/cari-anak') }}">
    <i class="fas fa-chart-pie"></i>
    Input data KMS
  </a>
</li>
<li class="{{ request()->segment(2) == 'jadwal-kegiatan' ? 'active' : '' }}">
  <a href="{{ url('/petugas/jadwal-kegiatan') }}">
    <i class="fas fa-clipboard-list"></i>
    Jadwal Kegiatan
  </a>
</li>
<li class="has-sub {{ request()->segment(4) == 'report' ? 'active' : '' }}">
  <a class="js-arrow" href="#">
      <i class="fas fa-clipboard-list"></i>
      Laporan KMS
  </a>
  <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
    <li class="{{ request()->segment(3) == 'penimbangan' && request()->segment(4) == 'report' ? 'active' : '' }}">
      <a href="{{ url('/petugas/anak/penimbangan/report') }}">
        Penimbangan
      </a>
    </li>
    <li class="{{ request()->segment(3) == 'vitamin' && request()->segment(4) == 'report' ? 'active' : '' }}">
      <a href="{{ url('/petugas/anak/vitamin/report') }}">
        Vitamin A
      </a>
    </li>
    <li class="{{ request()->segment(2) == 'posyandu-report' ? 'active' : '' }}">
      <a href="{{ url('/petugas/posyandu-report') }}">
        Bulanan Posyandu
      </a>
    </li>
  </ul>
</li>
<li>
  <a href="https://dashboard.tawk.to/" target="_blank">
    <i class="fas fa-comments"></i>
    Chat Dashboard
  </a>
</li>