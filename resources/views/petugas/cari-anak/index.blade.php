@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">Cari anak</h2>
      </div>
    </div>
  </div>
  @if ($msg = Session::get('messages'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-block alert-{{ Session::get('type') }}">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col col-12 col-md-6 m-b-25">
      <form class="form-header" method="GET" action="{{ url('/petugas/cari-anak') }}">
        <input class="au-input au-input--sm" type="text" name="q" placeholder="Search..." value="{{ request()->get('q') }}" />
        <button class="au-btn--submit" type="submit">
            <i class="zmdi zmdi-search"></i>
        </button>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col col-12">
      <div class="table-responsive">
        <table class="table table-borderless table-striped table-earning">
          <thead>
            <tr>
              <th>No. Identitas</th>
              <th>nama anak</th>
              <th>bapak</th>
              <th>ibu</th>
              <th>option</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($datas as $dt)
              <tr class="tr_click">
                <td>{{ $dt->generateId }}</td>
                <td>{{ $dt->name }}</td>
                <td>{{ $dt->bapak?? '-' }}</td>
                <td>{{ $dt->ibu?? '-' }}</td>
                <td>
                  <form action="{{ route('petugas.confirm.anak', $dt->id) }}" method="post">
                    @csrf
                    @method('POST')
                    <button class="btn btn-sm btn-primary">Pilih</button>
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="100%" class="text-center p-5"><h5>Tidak ada data.</h5></td>
              </tr>
            @endforelse
          </tbody>
        </table>
        <div class="d-flex justify-content-end m-t-25">
          {{ $datas->appends(request()->all())->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script-addon')
    <script>
      $('.tr_click').click(function() {
        $(this).find('form').submit();
      })
    </script>
@endpush