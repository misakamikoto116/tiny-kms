@extends('layouts.app')

@section('content')
@if ($msg = Session::get('messages'))
<div class="row">
  <div class="col col-12">
    <div class="alert alert-block alert-{{ Session::get('type') }}">
      <p>{{ $msg }}</p>
    </div>      
  </div>
</div>
@endif
<div class="card">
  <div class="card-body">
    <h3 class="card-title d-flex flex-column">
      Laporan Vitamin A
      <small class="text-muted">{{ Auth::user()->petugas->posyandu->name }}</small>
    </h3>
    <form method="GET" action="{{ route('petugas.anak.vitamin.print') }}">
      <div class="form-group">
        <label for="jenis" class="form-control-label">Jenis Vitamin A</label>
        <select name="jenis" id="jenis" class="form-control">
          <option disabled selected value="0">Pilih jenis vitamin a</option>
          <option value="biru" @if(old('jenis') === 'biru') selected @endif>Kapsul Biru 6-11 Bulan</option>
          <option value="merah" @if(old('jenis') === 'merah') selected @endif>Kapsul Merah 12-59 Bulan</option>
        </select>
      </div>
      <div class="form-group">
        <label for="date" class="form-control-label">Tanggal</label>
        <input name="date" type="text" id="date" placeholder="Masukkan tanggal"
          value="{{ old('date') }}"
          class="form-control date-laporan"
          required
        >
      </div>
      <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </form>
  </div>
</div>
@endsection
@push('script-addon')
  <script>
    $('.date-laporan').datetimepicker({
      viewMode: 'years',
      format: 'YYYY/MM'
    });
  </script>
@endpush