@extends('layouts.app')

@section('content')
@if ($msg = Session::get('messages'))
<div class="row">
  <div class="col col-12">
    <div class="alert alert-block alert-{{ Session::get('type') }}">
      <p>{{ $msg }}</p>
    </div>      
  </div>
</div>
@endif
<div class="card">
  <div class="card-body">
    <h3 class="card-title d-flex flex-column">
      Laporan Posyandu
      <small class="text-muted">{{ Auth::user()->petugas->posyandu->name }}</small>
    </h3>
    <form method="GET" action="{{ route('petugas.posyandu.print') }}" target="_blank">
      <div class="form-group">
        <label for="tanggal" class="form-control-label">Tanggal</label>
        <input name="tanggal" type="text" id="tanggal" placeholder="Masukkan tanggal"
          value="{{ old('tanggal') }}"
          class="form-control date-laporan"
          required
        >
      </div>
      <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </form>
  </div>
</div>
@endsection
@push('script-addon')
  <script>
    $('.date-laporan').datetimepicker({
      viewMode: 'years',
      format: 'YYYY/MM'
    });
  </script>
@endpush