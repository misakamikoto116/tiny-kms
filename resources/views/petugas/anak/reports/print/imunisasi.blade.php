<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/papper.css') }}">
    <link href="{{ asset('template/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css">
    <style>
        .label-print{
            font-size: 100%;
            font-weight: lighter;
        }

        hr {
            margin-top: 20px;
            margin-bottom: 20px;
            border: 1px;
            border-top: 4px solid black;
        }
        
        .table-custom th {
            border: 1px solid black;
            font-weight: bold;
            background:#f3d89c;
            text-align: center;
        }


        .table-custom td, .table-custom th {
            border: 1px solid black;
            text-align: center;
        }
        
        .table-custom {
            border: 1px solid;
            margin-top: 8px;
            width: 100%;
        }
        

    </style>
    <title></title>
</head>
<body onload="window.print();">
    <section class="A4 sheet padding-0mm1">
        <article>
          Monggo
        </article>
    </section>
</body>
</html>