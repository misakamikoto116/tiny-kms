<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ asset('css/papper.css') }}">
	<link href="{{ asset('template/css/theme.css') }}" rel="stylesheet" media="all">

	<title>Laporan Vitamin A</title>

	<style>
		html, body {
			font-size: 14px;
		}

		.header {
			border-bottom: 3px solid black;
			display: flex;
			flex-direction: row;
			padding-bottom: 20px;
			margin-bottom: 20px;
		}

		.logo {
			width: 100px;
			height: 100px;
		}

		.header-info {
			display: flex;
			flex: 1;
			flex-direction: column;
			justify-content: center;
			align-items: center;
      margin: 0 20px;
		}

		.heading {
			font-size: 1.6em;
			margin: 2px 0;
      text-align: center;
		}

		.content-heading {
			font-size: 1.6em;
		}

		.content-info {
			align-self: flex-start;
		}

		.content-info p {
			margin: 5px 0;
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			margin-top: 20px;
		}

		.table th, .table td{
			border: 2px solid black;
			padding: 5px;
		}

		.text-center {
			text-align: center;
		}

    .font-weight-bold {
      font-weight: bold;
    }
    
    .footer {
      width: 100%;
      margin-top: 40px;
      display: flex;
      justify-content: flex-end;
      position: relative;
    }

    .ttd-text {
      margin-bottom: 70px;
    }

    @media print {
      .table th, .table td, .footer {
        page-break-inside: avoid;
      }
    }
		
	</style>
</head>
<body onload="window.print();" class="A4.landscape">
	<section class="sheet padding-10mm">
		<article>
			<div class="header">
				<img class="logo" src="{{ asset('template/images/icon/kukar-logo.png') }}" alt="Logo Kukar">
				<div class="header-info"">
					<h1 class="heading">PEMBERIAN KAPSUL VITAMIN A</h1>
					<h1 class="heading">UMUR {{ request()->get('jenis') === 'biru' ? '6-11 BULAN' : '12-59 BULAN' }}</h1>
				</div>
				{{-- <img class="logo" src="{{ asset('template/images/icon/puskesmas-logo.png') }}" alt="Logo Kukar"> --}}
			</div>
			
			<div class="content">			
				<div class="content-info">
					<p>Posyandu: {{ Auth::user()->petugas->posyandu->name }}</p>
					<p>Jenis: Kapsul {{ ucfirst(request()->get('jenis')) }}</p>
					<p>Tanggal: {{ $tanggal }}</p>
				</div>

				<table class="table">
					<thead>
						<tr>
							<th rowspan="2">No. Identitas Anak</th>
							<th rowspan="2">Nama Anak</th>
							<th colspan="2">Orangtua</th>
							<th colspan="2">Tanggal Lahir / Umur</th>
							<th rowspan="2">Jenis Kelamin</th>
							<th rowspan="2">Alamat</th>
						</tr>
						<tr>
							<th>Bapak</th>
							<th>Ibu</th>
							<th>Tanggal Lahir</th>
							<th>Umur</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($datas as $dt)
							<tr>
								<td class="text-center">{{ $dt->anak->generateId }}</td>
								<td>{{ $dt->anak->name }}</td>
								<td>{{ $dt->anak->bapak ?? '-' }}</td>
								<td>{{ $dt->anak->ibu ?? '-' }}</td>
								<td class="text-center">{{ $dt->anak->tgl_lahir_formatted }}</td>
								<td class="text-center">{{ $dt->anak->umur_bulan_formatted }}</td>
								<td>{{ $dt->anak->jenis_kelamin_formatted }}</td>
								<td>{{ $dt->anak->alamat }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>

				<div class="footer">
          <div class="ttd">
            <p class="ttd-text">
              Sungai Mariam,<br>
              Ketua Kader Posyandu
            </p>
            <p>(....................................)</p>
          </div>
        </div>
			</div>
		</article>
	</section>
</body>
</html>