<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ asset('css/papper.css') }}">
	<link href="{{ asset('template/css/theme.css') }}" rel="stylesheet" media="all">

	<title>Laporan Posyandu</title>

	<style>
		html, body {
			font-size: 14px;
		}

		.header {
			border-bottom: 3px solid black;
			display: flex;
			flex-direction: row;
			padding-bottom: 20px;
			margin-bottom: 20px;
		}

		.logo {
			width: 100px;
			height: 100px;
		}

		.header-info {
			display: flex;
			flex: 1;
			flex-direction: column;
			justify-content: center;
			align-items: center;
      margin: 0 20px;
		}

		.heading {
			font-size: 1.6em;
			margin: 2px 0;
      text-align: center;
		}

		.content-heading {
			font-size: 1.6em;
		}

		.content-info {
			align-self: flex-start;
		}

		.content-info p {
			margin: 5px 0;
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			margin-top: 20px;
		}

		.table th, .table td{
			border: 2px solid black;
			padding: 5px;
		}

		.text-center {
			text-align: center;
		}

    .font-weight-bold {
      font-weight: bold;
    }
    
    .footer {
      width: 100%;
      margin-top: 40px;
      display: flex;
      justify-content: flex-end;
      position: relative;
    }

    .ttd-text {
      margin-bottom: 70px;
    }

    @media print {
      .table th, .table td, .footer {
        page-break-inside: avoid;
      }
    }
		
	</style>
</head>
<body onload="window.print();" class="A4.potrait">
	<section class="sheet padding-10mm">
		<article>
      <div class="header">
				<img class="logo" src="{{ asset('template/images/icon/kukar-logo.png') }}" alt="Logo Kukar">
				<div class="header-info">
					<h1 class="heading">LAPORAN BULANAN POSYANDU</h1>
          <h1 class="heading">WILAYAH KERJA PUSKESMAS SUNGAI MARIAM</h1>
				</div>
				{{-- <img class="logo" src="{{ asset('template/images/icon/puskesmas-logo.png') }}" alt="Logo Kukar"> --}}
			</div>
			
			<div class="content">				
				<div class="content-info">
          <p><span class="font-weight-bold">Tanggal:</span> {{ $tanggal }}</p>
          <p><span class="font-weight-bold">Nama Posyandu:</span> {{ Auth::user()->petugas->posyandu->name }}</p>
          <p><span class="font-weight-bold">Desa:</span> {{ Auth::user()->petugas->posyandu->desa }}</p>
          <p><span class="font-weight-bold">Jumlah Kader Aktif:</span> {{ Auth::user()->petugas->posyandu->jumlah_kader }}</p>
				</div>

				<table class="table">
					<thead>
						<tr>
              <th rowspan="2">NO</th>
              <th rowspan="2">KEGIATAN PENIMBANGAN BALITA</th>
              <th colspan="2">JENIS KELAMIN</th>
              <th rowspan="2">JUMLAH</th>
            </tr>
            <tr>
              <th>L</th>
              <th>P</th>
            </tr>
					</thead>
					<tbody>
						<tr>
              @php $no = 1 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah bayi ( 6-11 bln ) yang dapat VIT.A dosis tinggi ( 100.000 UI )
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 2 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah anak ( 1-5 thn ) yang dapat VIT.A dosis tinggi ( 200.000 UI )
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 3 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah seluruh bayi ( 0-12 bln) yang ada di wilayah posyandu
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 4 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah seluruh anak ( 1-5 thn ) yabg ada di wilayah posyandu
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 5 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah BALITA ( 0-5 thn ) yang ada di wilayah posyandu (S) 
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              {{-- @php $no = 6 @endphp --}}
              <td class="text-center">6</td>
              <td>
                Jumlah bayi ( 0-23 bln ) yang memiliki KMS (K)
              </td>
              <td class="text-center">
                {{-- {{ $datas->get($no)['L'] }} --}}
              </td>
              <td class="text-center">
                {{-- {{ $datas->get($no)['P'] }} --}}
              </td>
              <td class="text-center">
                {{-- {{ $datas->get($no)['J'] }} --}}
              </td>
            </tr>
            <tr>
              {{-- @php $no = 7 @endphp --}}
              <td class="text-center">7</td>
              <td>
                Jumlah anak ( 2-5 thn) yang memiliki KMS (K)
              </td>
              <td class="text-center">
                {{-- {{ $datas->get($no)['L'] }} --}}
              </td>
              <td class="text-center">
                {{-- {{ $datas->get($no)['P'] }} --}}
              </td>
              <td class="text-center">
                {{-- {{ $datas->get($no)['J'] }} --}}
              </td>
            </tr>
            <tr>
              @php $no = 8 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah bayi ( 0-23 bln ) yang ditimbang bulan ini (D)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 9 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah anak ( 2-5 thn ) yang ditimbang bulan ini (D)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 10 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah bayi ( 0-23 bln ) yang naik timbangannya bulan ini (N)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 11 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah anak ( 2-5 thn ) yang naik timbangannya bulan ini (N)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 12 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah BALITA yang tidak naik/tetap/turun BB nya bulan ini (T)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 13 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah BALITA yang ditimbang bulan ini tapi tidak hadir bulan lalu (O)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 14 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah bayi ( 0-23 bln ) yang pertama kali hadir bulan ini (B)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 15 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah anak ( 2-5 thn ) yang pertama kali hadir bulan ini (B)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 16 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah bayi ( 6-12 bln ) dengan BB di Bawah Garis Merah (BGM)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 17 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah anak ( 1-5 thn ) dengan BB di Bawah Garis Merah (BGM)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 18 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah BALITA yang BB nya tidak naik 2 kali (2T)
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 19 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah BALITA yang ditimbang bulan ini berumur 36 bulan
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
            <tr>
              @php $no = 20 @endphp
              <td class="text-center">{{ $no }}</td>
              <td>
                Jumlah BALITA yang mencapai umur 36 bulan dengan BB 11.5 Kg atau lebih
              </td>
              <td class="text-center">
                {{ $datas->get($no)['L'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['P'] }}
              </td>
              <td class="text-center">
                {{ $datas->get($no)['J'] }}
              </td>
            </tr>
					</tbody>
        </table>
        
        <div class="footer">
          <div class="ttd">
            <p class="ttd-text">
              Sungai Mariam,<br>
              Ketua Kader Posyandu
            </p>
            <p>(....................................)</p>
          </div>
        </div>
			</div>
		</article>
  </section>
</body>
</html>