@extends('layouts.app')

@section('content')
@if ($msg = Session::get('messages'))
<div class="row">
  <div class="col col-12">
    <div class="alert alert-block alert-{{ Session::get('type') }}">
      <p>{{ $msg }}</p>
    </div>      
  </div>
</div>
@endif
<div class="card">
  <div class="card-body">
    <h3 class="card-title d-flex flex-column">
      Laporan Imunisasi
      <small class="text-muted">{{ Auth::user()->petugas->posyandu->name }}</small>
    </h3>
    <form method="GET" action="{{ route('petugas.anak.imunisasi.print') }}">
      @csrf
      <div class="form-group">
        <label for="start_date" class="form-control-label">Tanggal Awal</label>
        <input name="start_date" type="text" id="start_date" placeholder="Masukkan tanggal awal"
          value="{{ old('start_date') }}"
          class="form-control date"
          required
        >
      </div>
      <div class="form-group">
        <label for="end_date" class="form-control-label">Tanggal Akhir</label>
        <input name="end_date" type="text" id="end_date" placeholder="Masukkan tanggal akhir"
          value="{{ old('end_date') }}"
          class="form-control date"
          required
        >
      </div>
      <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-send"></i> Submit
        </button>
      </div>
    </form>
  </div>
</div>
@endsection