@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">Data Anak</h2>
      </div>
    </div>
  </div>
  @if ($msg = Session::get('message'))
    <div class="row">
      <div class="col col-12">
        <div class="alert alert-info alert-block">
          <p>{{ $msg }}</p>
        </div>      
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col">
      <div class="au-card">
        <div class="au-card-inner">
          <div class="row">
            <div class="col col-lg-auto">
              <img class="m-b-15 rounded" style="width: 200px; height: 200px;" src="{{ !is_null(Session::get('anak.foto')) ? url('images/anak/' . Session::get('anak.foto')) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
            </div>
            <div class="col">
              <h3 class="mb-2">{{ Session::get('anak.name') }}</h3>
              <p>
                {{ Session::get('anak.jenis_kelamin_formatted') }}
              </p>
              <p>
                {{ Session::get('anak.tgl_lahir_formatted') }}
              </p>
              <p>
                {{ Session::get('anak.umur_bulan_formatted') }}
              </p>
              <a href="{{ url('petugas/anak/detail') }}" class="btn-link mt-3" role="button">
                Lihat detail
              </a>
            </div>
            <div class="col d-flex flex-column">
              <a href="{{ url('petugas/anak/penimbangan') }}" class="btn btn-primary mb-3">
                Data penimbangan
              </a>
              <a href="{{ url('petugas/anak/imunisasi') }}" class="btn btn-primary mb-3">
                Data imunisasi
              </a>
              <a href="{{ url('petugas/anak/vitamin') }}" class="btn btn-primary mb-3">
                Data vitamin
              </a>
              <a href="{{ url('petugas/anak/grafik') }}" class="btn btn-primary mb-3">
                Lihat grafik KMS
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection