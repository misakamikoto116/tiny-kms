@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row m-b-25">
    <div class="col-md-12">
      <div class="overview-wrap">
        <h2 class="title-1">
          Data Anak
          <small class="text-muted">{{ Session::get('anak.name') }} / {{ Session::get('anak.umur_bulan_formatted') }} / {{ Session::get('anak.jenis_kelamin_formatted') }}</small>
        </h2>
        <div>
          <a
            class="au-btn au-btn-icon au-btn--blue2 mr-2"
            href={{ url('/petugas/anak') }}
          >
            <i class="zmdi zmdi-arrow-left"></i>
            Kembali
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
            <strong>Anak</strong>
            <small> Detail</small>
        </div>
        <div class="card-body card-block">
          <div class="row form-group">
            <div class="col col-12 col-md-6">
              <label for="nik" class="form-control-label">NIK Anak</label>
              <input name="nik" type="number" id="nik" placeholder="Masukkan NIK"
                value="{{ Session::get('anak.nik') }}"
                class="form-control"
                disabled>
              <small class="help-block form-text text-info">Ket: <span id="ket_nik">{{ Session::get('anak.is_in_anggana_formatted') }}</span></small>
            </div>
            <div class="col col-12 col-md-6">
              <label for="nama_lengkap" class="form-control-label">Nama Lengkap</label>
              <input name="name" value="{{ Session::get('anak.name') }}"
                type="text" id="nama_lengkap" placeholder="Masukkan nama lengkap"
                class="form-control"
                disabled>
            </div>
          </div>
          <div class="row form-group">
            <div class="col col-12 col-md-6">
              <label for="tanggal_lahir" class="form-control-label">Tanggal Lahir</label>
              <input name="tgl_lahir" type="text" id="tanggal_lahir" placeholder="Masukkan tanggal lahir"
                value="{{ Session::get('anak.tgl_lahir_formatted') }}"
                class="form-control"
                disabled>
            </div>
            <div class="col col-12 col-md-6">
              <label for="jenis_kelamin" class="form-control-label">Jenis Kelamin</label>
              <input name="tgl_lahir" type="text" id="jenis_kelamin" placeholder="Masukkan jenis kelamin"
                value="{{ Session::get('anak.jenis_kelamin_formatted') }}"
                class="form-control"
                disabled>
            </div>
          </div>
          <div class="row form-group">
            <div class="col col-12 col-md-6">
              <label for="kecamatan" class="form-control-label">Kecamatan</label>
              <input name="kecamatan" type="text" id="kecamatan" placeholder="Masukkan kecamatan"
                value="{{ Session::get('anak.kecamatan') }}"
                class="form-control"
                disabled>
            </div>
            <div class="col col-12 col-md-6">
              <label for="kelurahan" class="form-control-label">Kelurahan/Desa</label>
              <input name="kelurahan" type="text" id="kelurahan" placeholder="Masukkan kelurahan"
                value="{{ Session::get('anak.kelurahan') }}"
                class="form-control"
                disabled>
            </div>
          </div>
          <div class="row form-group">
            <div class="col col-12 col-md-6">
              <label for="rt" class="form-control-label">RT</label>
              <input name="rt" type="text" id="rt" placeholder="Masukkan rt"
                value="{{ Session::get('anak.rt') }}"
                class="form-control"
                disabled>
            </div>
            <div class="col col-12 col-md-6">
              <label for="alamat" class="form-control-label">Alamat</label>
              <input name="alamat" type="text" id="alamat" placeholder="Masukkan alamat"
                value="{{ Session::get('anak.alamat')}}"
                class="form-control"
                disabled>
            </div>
          </div>
          <div class="row form-group">
            <div class="col col-12 col-md-6">
              <label for="bapak" class="form-control-label">Ayah</label>
              <input name="bapak" type="text" id="bapak" placeholder="Masukkan nama bapak"
                value="{{ Session::get('anak.bapak') }}"
                class="form-control"
                disabled>
            </div>
            <div class="col col-12 col-md-6">
              <label for="ibu" class="form-control-label">Ibu</label>
              <input name="ibu" type="text" id="ibu" placeholder="Masukkan nama ibu"
                value="{{ Session::get('anak.ibu') }}"
                class="form-control"
                disabled>
            </div>
          </div>
          <div class="row form-group">
            <div class="col col-auto">
              <img id="foto-preview" src="{{ !is_null(Session::get('anak.foto')) ? url('images/anak/' . Session::get('anak.foto')) : asset('/img/placeholder.jpg') }}" alt="foto preview" class="foto-preview">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection